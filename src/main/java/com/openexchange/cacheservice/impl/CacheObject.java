/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import com.openexchange.cacheservice.api.ICacheObject;

import java.io.Serial;

/**
 * {@link CacheObject}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class CacheObject implements ICacheObject {

    /**
     * serialVersionUID
     */
    @Serial
    private static final long serialVersionUID = 1203862060096934396L;

    /**
     * Initializes a new {@link CacheObject}.
     */
    @SuppressWarnings("unused")
    private CacheObject() {
        // no empty Ctor
    }

    /**
     * Initializes a new {@link CacheObject}.
     *
     * @param groupId
     * @param keyId
     * @param fileId
     */
    public CacheObject(String groupId, String keyId, String fileId) {
        super();

        m_groupId = groupId;
        m_keyId = keyId;
        m_fileId = fileId;

        if (CacheUtils.isInvalid(m_fileId)) {
            LOG.error("CS Invalid fileId is not allowed, set fileId to unique and not empty value");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CacheObject" + " [" + ((null != m_objectStoreData) ? m_objectStoreData.toString() : "ObjectStoreData [null]") + " , groupId: " +
               m_groupId + " , keyId: " + m_keyId + " , fileId: " + m_fileId + ']';
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IFileItem#getGroupId()
     */
    @Override
    public String getGroupId() {
        return m_groupId;
    }

    /**
     * Gets the keyId
     *
     * @return The m_keyId
     */
    @Override
    public String getKeyId() {
        return m_keyId;
    }

    /**
     * Gets the fileId
     *
     * @return The m_fileId
     */
    @Override
    public String getFileId() {
        return m_fileId;
    }

    // - Package internal API  -------------------------------------------------

    /**
     * @return
     */
    protected String getKey() {
        return m_groupId + '-' + m_keyId + '-' + m_fileId;
    }

    /**
     * @return
     */
    protected boolean isValid() {
        return CacheUtils.isValid(m_groupId) && CacheUtils.isValid(m_keyId) && CacheUtils.isValid(m_fileId);
    }

    /**
     * @return
     */
    protected boolean isInvalid() {
        return CacheUtils.isInvalid(m_groupId) || CacheUtils.isInvalid(m_keyId) || CacheUtils.isInvalid(m_fileId);
    }

    /**
     * Gets the storeId
     *
     * @return The m_storeId
     */
    protected ObjectStoreData getOjectStoreData() {
        return m_objectStoreData;
    }

    /**
     * Gets the storeId
     *
     * @return The m_storeId
     */
    protected void setObjectStoreData(final ObjectStoreData objectStoreData) {
        m_objectStoreData = objectStoreData;
    }

    // - Members ---------------------------------------------------------------

    protected String m_groupId = null;

    protected String m_keyId = null;

    protected String m_fileId = null;

    protected ObjectStoreData m_objectStoreData = null;
}
