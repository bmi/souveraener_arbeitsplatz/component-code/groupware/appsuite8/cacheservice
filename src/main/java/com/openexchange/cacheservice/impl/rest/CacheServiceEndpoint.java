/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl.rest;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.core.Response.StatusType;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheConfig;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.GroupConfig;
import com.openexchange.cacheservice.api.ICache;
import com.openexchange.cacheservice.api.ICacheObject;
import com.openexchange.cacheservice.api.IReadAccess;
import com.openexchange.cacheservice.api.IWriteAccess;
import com.openexchange.cacheservice.api.IdLocker;
import com.openexchange.cacheservice.api.MutableWrapper;
import com.openexchange.cacheservice.impl.CacheMetrics;
import com.openexchange.cacheservice.impl.CacheMetrics.RequestType;
import com.openexchange.cacheservice.impl.CacheUtils;

/**
 * {@link CacheServiceEndpoint}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@Component
@Path("/cache")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CacheServiceEndpoint {

    /**
     * StatusLocked
     */
    final private static StatusType STATUS_LOCKED = new StatusType() {

        /**
         * STATUS_REASON_LOCKED
         */
        final private static String STATUS_REASON_LOCKED = "Locked";

        @Override
        public int getStatusCode() {
            return 423;
        }

        @Override
        public Family getFamily() {
            return Family.CLIENT_ERROR;
        }

        @Override
        public String getReasonPhrase() {
            return STATUS_REASON_LOCKED;
        }
    };

    /**
     * @param cache
     * @param cacheMetrics
     */
    @Autowired
    public CacheServiceEndpoint(@NonNull final ICache cache, @NonNull CacheMetrics cacheMetrics) {
        super();

        m_cache = cache;
        m_cacheMetrics = cacheMetrics;
    }

    // - Request handler -------------------------------------------------------

    /**
     * @param metrics
     * @return
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @PermitAll
    public String root(@QueryParam("metrics") @DefaultValue("false") String metrics) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("CS Server received #/ request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final String ret = implCreateCacheServiceServerHtmlPage(m_running.get() ? (m_cache.isValid() ? CACHE_SERVER_OK : CACHE_SERVER_UNAVAILABLE) : CACHE_SERVER_GONE, StringUtils.equalsIgnoreCase(metrics, "true") ? m_cacheMetrics : null);

        m_cacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    /**
     * @param metrics
     * @return
     */
    @GET
    @Path("/status")
    @PermitAll
    public Response status(@QueryParam("metrics") @DefaultValue("false") String metrics) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("CS Server received #/status request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final Response ret = implGetStatus("true".equalsIgnoreCase(metrics));

        m_cacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    /**
     * @return
     */
    @GET
    @Path("/health")
    @PermitAll
    public Response health() {
        if (LOG.isTraceEnabled()) {
            LOG.trace("CS Server received #/health request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final Response ret = implGetStatus(false);

        m_cacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    /**
     * @param groupId
     * @param groupConfigStr
     * @return
     */
    @POST
    @Path("/registerGroup/{group}")
    @PermitAll
    public Response registerGroup(
        @PathParam("group") String groupId,
        String groupConfigStr) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("CS Server received #/registerGroup request: {}", groupId);
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final String group = implGetValidatedGroup(groupId);
        final boolean parametersValid = (null != group);

        try {
            if (!m_running.get()) {
                return implCreateJSONResponse(Status.GONE);
            }

            final JSONObject jsonGroupConfig = StringUtils.isNotBlank(groupConfigStr) ? new JSONObject(groupConfigStr) : null;

            if (parametersValid && (null != jsonGroupConfig)) {
                final CacheConfig cacheConfig = m_cache.getCacheConfig();
                final GroupConfig.Builder groupConfigConfigBuilder = GroupConfig.builder().
                    withMaxKeyCount(cacheConfig.getDefaultCacheMaxKeyCount()).
                    withMaxGroupSize(cacheConfig.getDefaultCacheMaxSize()).
                    withKeyTimeoutMillis(cacheConfig.getDefaultCacheKeyTimeoutMillis()).
                    withCleanupPeriodMillis(cacheConfig.getDefaultCacheCleanupPeriodMillisp());

                final GroupConfig groupConfig = groupConfigConfigBuilder.withGroupId(group).withJSON(jsonGroupConfig).build();

                if (IdLocker.tryLock(group)) {
                    try {
                        final GroupConfig updatedGroupConfig = m_cache.registerGroup(groupConfig);

                        if (null != updatedGroupConfig) {
                            // register group at metrics instance
                            m_cacheMetrics.registerGroup(group);
                            LOG.trace("CS Server registered group: {}", group);

                            return Response.ok().type(MediaType.APPLICATION_JSON).entity(updatedGroupConfig.toJSON()).build();
                        }

                        return implCreateJSONResponse(Status.INTERNAL_SERVER_ERROR);
                    } catch (CacheException e) {
                        return implCreateJSONExceptionResponse(e);
                    } finally {
                        IdLocker.unlock(group);
                    }
                }

                return implCreateJSONResponse(STATUS_LOCKED);
            }

            return implCreateJSONResponse(Status.BAD_REQUEST);
        } finally {
            if (parametersValid) {
                m_cacheMetrics.incrementRequestCount(group, RequestType.REGISTERGROUP, System.currentTimeMillis() - requestStartTimeMillis);
            }
        }
    }

    /**
     * @param groupId
     * @param metrics
     *
     * @return
     */
    @GET
    @Path("/getGroupInfo/{group}")
    @PermitAll
    public Response getGroupInfo(
        @PathParam("group") String groupId,
        @QueryParam("metrics") @DefaultValue("false") String metrics) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("CS Server received #/getGroupInfo request: {}, metrics: {}", groupId, metrics);
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final boolean allGroups = (StringUtils.isBlank(groupId) || StringUtils.equalsIgnoreCase(groupId.trim(), STR_ALL));
        final String group = allGroups ? null : implGetValidatedGroup(groupId);

        try {
            if (!m_running.get()) {
                return implCreateJSONResponse(Status.GONE);
            }

            final Set<String> groupSetToUse = new HashSet<>();

            try {
                if (allGroups) {
                    final String[] allGroupArray = m_cache.getGroups();

                    if (ArrayUtils.isNotEmpty(allGroupArray)) {
                        groupSetToUse.addAll(Arrays.asList(allGroupArray));
                    }
                } else {
                    groupSetToUse.add(group);
                }

                final JSONArray jsonGroupArray = new JSONArray(groupSetToUse.size());
                final AtomicInteger curPos = new AtomicInteger(-1);

                groupSetToUse.forEach((curGroupName) -> {
                    final GroupConfig curGroupConfig = m_cache.getGroupConfig(curGroupName);

                    if (null != curGroupConfig) {
                        try {
                            final JSONObject jsonGroupInfo = curGroupConfig.toJSON();

                            if (StringUtils.equalsIgnoreCase(metrics, STR_TRUE)) {
                                // add cache info if requested
                                jsonGroupInfo.put(JSON_KEY_CACHE, new JSONObject().
                                    put(JSON_KEY_CACHE_KEYCOUNT, m_cache.getKeyCount(curGroupName)).
                                    put(JSON_KEY_CACHE_MAX_KEYAGE_MILLIS, m_cache.getMaxKeyAgeMillis(curGroupName)).
                                    put(JSON_KEY_CACHE_GROUPLENGTH, m_cache.getGroupSize(curGroupName)));
                            }

                            jsonGroupArray.put(curPos.incrementAndGet(), jsonGroupInfo);
                        } catch (Exception e) {
                            CacheUtils.logExcp(e);
                        }
                    }
                });

                return Response.ok().type(MediaType.APPLICATION_JSON).entity(new JSONObject().put(JSON_KEY_GROUPS, jsonGroupArray).toString()).build();
            } catch (Exception e) {
                return implCreateJSONExceptionResponse(e);
            }
        } finally {
            final long requestDurationMillis = System.currentTimeMillis() - requestStartTimeMillis;

            if (allGroups) {
                m_cacheMetrics.incrementAdminRequestCount(RequestType.GETGROUPINFO, requestDurationMillis);
            } else if (null != group) {
                m_cacheMetrics.incrementRequestCount(group, RequestType.GETGROUPINFO, requestDurationMillis);
            }
        }
    }

    /**
     * @param groupId
     * @param keyId
     * @param multiPart
     *
     * @return
     */
    @POST
    @Path("/addObjects/{group}/{key}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response addObjects(
        @PathParam("group") String groupId,
        @PathParam("key") String keyId,
        FormDataMultiPart multiPart) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("CS Server received #/addObjects request: {} / {}", groupId, keyId);
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final String group = implGetValidatedGroup(groupId);
        final String key = implGetValidatedId(keyId);
        boolean parametersValid = (null != group) && (null != key) && (null != multiPart);

        try {
            if (!m_running.get()) {
                return implCreateJSONResponse(Status.GONE);
            }

            if (parametersValid) {
                final MutableWrapper<Boolean> done = new MutableWrapper<>(Boolean.TRUE);

                for (final BodyPart curBodyPart : multiPart.getBodyParts()) {
                    final ContentDisposition curContentDisposition = curBodyPart.getContentDisposition();
                    final Map<String, String> curPartParameters = curContentDisposition.getParameters();
                    final String id = implGetValidatedId((null != curPartParameters) ? curPartParameters.get("name") : null);

                    // immediately quit loop in case an invalid id parameter is detected
                    if (null == id) {
                        parametersValid = false;
                        break;
                    }

                    try (final InputStream inputStm = curBodyPart.getEntityAs(InputStream.class)) {
                        if (null != inputStm) {
                            try (final IWriteAccess writeAcc = m_cache.getWriteAccess(group, key, id);
                                 final FileOutputStream outputStm = new FileOutputStream(writeAcc.getOutputFile())) {

                                IOUtils.copy(inputStm, outputStm);
                                outputStm.flush();
                            }
                        }
                    } catch (final Exception e) {
                        done.set(Boolean.FALSE);
                        LOG.error("CS Server error in #/addObjects", e);
                    }
                }

                if (parametersValid) {
                    return implCreateJSONResponse(done.get() ? Status.OK : Status.INTERNAL_SERVER_ERROR);
                }
            }

            return implCreateJSONResponse(Status.BAD_REQUEST);
        } finally {
            if (parametersValid) {
                m_cacheMetrics.incrementRequestCount(group, RequestType.ADDOBJECTS, System.currentTimeMillis() - requestStartTimeMillis);
            }
        }
    }

    /**
     * @param groupId
     * @param keyId
     * @param objectIds
     * @param removeKeyOnError
     * @param removeFileOnError
     *
     * @return
     */
    @GET
    @Path("/hasObjects/{group}/{key}")
    @PermitAll
    public Response hasObjects(
        @PathParam("group") String groupId,
        @PathParam("key") String keyId,
        @QueryParam("objects") Set<String> objectIds,
        @QueryParam("removeKeyOnError") @DefaultValue("false") boolean removeKeyOnError,
        @QueryParam("removeFileOnError") @DefaultValue("false") boolean removeFileOnError) {

        if (LOG.isTraceEnabled()) {
            final StringBuilder objectIdsStrBuilder = new StringBuilder(256);

            if (null != objectIds) {
                objectIds.forEach((curObjectId) -> objectIdsStrBuilder.append(curObjectId).append(", "));
            }

            LOG.trace("CS Server received #/hasObjects request: {} / {} / [ {} ]", groupId, keyId, objectIdsStrBuilder.substring(0, objectIdsStrBuilder.length() - 2));
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final String group = implGetValidatedGroup(groupId);
        final String key = implGetValidatedId(keyId);
        final Set<String> ids = implGetValidatedIds(objectIds);
        final boolean parametersValid = (null != group) && (null != key) && (null != ids);

        try {
            if (!m_running.get()) {
                return implCreateJSONResponse(Status.GONE);
            }

            if (parametersValid) {
                try {
                    if (m_cache.isValid()) {
                        final ICacheObject[] objectsArray = m_cache.get(group, key);
                        final boolean consistencyCheck = removeFileOnError || removeKeyOnError;

                        if (ArrayUtils.isNotEmpty(objectsArray)) {
                            int foundIdCount = 0;

                            for (final String curId : ids) {
                                for (final ICacheObject curCacheObject : objectsArray) {
                                    if (curCacheObject.getFileId().equals(curId)) {
                                        if (consistencyCheck && !implCheckConsistency(curCacheObject, removeFileOnError, removeKeyOnError)) {
                                            // we can leave here in case removeKeyOnError is set to true
                                            // since the whole subgroup with given key has just been removed
                                            if (removeKeyOnError) {
                                                return implCreateJSONResponse(Status.NOT_FOUND);
                                            }
                                        } else {
                                            ++foundIdCount;
                                            break;
                                        }
                                    }
                                }
                            }

                            return implCreateJSONResponse((ids.size() == foundIdCount) ? Status.OK : Status.NOT_FOUND);
                        }
                    }

                    return implCreateJSONResponse(Status.NOT_FOUND);
                } catch (Exception e) {
                    return implCreateJSONExceptionResponse(e);
                }
            }

            return implCreateJSONResponse(Status.BAD_REQUEST);
        } finally {
            if (parametersValid) {
                m_cacheMetrics.incrementRequestCount(group, RequestType.HASOBJECTS, System.currentTimeMillis() - requestStartTimeMillis);
            }
        }
    }

    /**
     * @param groupId
     * @param keyId
     * @param objectIds
     * @param removeKeyOnError
     * @param removeFileOnError
     *
     * @return
     */
    @GET
    @Path("/getObjects/{group}/{key}")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response getObjects(
        @PathParam("group") String groupId,
        @PathParam("key") String keyId,
        @QueryParam("objects") Set<String> objectIds,
        @QueryParam("removeKeyOnError") @DefaultValue("false") boolean removeKeyOnError,
        @QueryParam("removeFileOnError") @DefaultValue("false") boolean removeFileOnError) {

        if (LOG.isTraceEnabled()) {
            final StringBuilder objectIdsStrBuilder = new StringBuilder(256);

            if (null != objectIds) {
                objectIds.forEach((curObjectId) -> objectIdsStrBuilder.append(curObjectId).append(", "));
            }

            LOG.trace("CS Server received #/getObjects request: {} / {} / [ {} ]", groupId, keyId, objectIdsStrBuilder.substring(0, Math.max(objectIdsStrBuilder.length(), 2) - 2));
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final String group = implGetValidatedGroup(groupId);
        final String key = implGetValidatedId(keyId);
        final Set<String> ids = implGetValidatedIds(objectIds);
        final boolean parametersValid = (null != group) && (null != key) && (null != ids);
        final MutableWrapper<Exception> lastExceptionWrapper = new MutableWrapper<>(null);

        try (final FormDataMultiPart multiPart = new FormDataMultiPart()) {
            if (!m_running.get()) {
                return implCreateJSONResponse(Status.GONE);
            }

            if (parametersValid) {
                final int requestedIdCount = ids.size();

                ids.forEach((curId) -> {
                    try {
                        if (m_cache.contains(group, key, curId)) {
                            try (final IReadAccess readAcc = m_cache.getReadAccess(group, key, curId);
                                 final ByteArrayOutputStream outputStm = new ByteArrayOutputStream()) {

                                IOUtils.copy(readAcc.getInputFile(), outputStm);
                                outputStm.flush();

                                multiPart.field(
                                    curId,
                                    new ByteArrayInputStream(outputStm.toByteArray()),
                                    MediaType.APPLICATION_OCTET_STREAM_TYPE);
                            }
                        }
                    } catch (Exception e) {
                        if (removeFileOnError) {
                            LOG.warn("CS Server could not get object(s) in #/getObjects call => Removing object: " + group + " / " + key + " / " + curId, e);

                            try {
                                m_cache.remove(group, key, curId);
                            } catch (CacheException e1) {
                                LOG.error(Throwables.getRootCause(e1).getMessage());
                            }
                        } else {
                            lastExceptionWrapper.set(e);
                        }
                    }
                });

                // remove complete subgroup in case of an error and if requested
                final Exception lastException = lastExceptionWrapper.get();

                if (null != lastException) {
                    if (removeKeyOnError) {
                        LOG.warn("CS Server could not get object(s) in #/getObjects call => Removing key: " + group + " / " + key, lastException);

                        try {
                            m_cache.removeKey(group, key);
                        } catch (CacheException e) {
                            LOG.error(Throwables.getRootCause(e).getMessage());
                        }
                    } else {
                        LOG.error("CS Server could not get object(s) in #/getObjects call: " + group + " / " + key, lastException);
                    }
                }

                // 'Not Found' status code in case one of the requested objects was not found
                return (multiPart.getBodyParts().size() == requestedIdCount) ?
                    Response.ok(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE).build() :
                        implCreateJSONResponse(Status.NOT_FOUND);
            }

            return implCreateJSONResponse(Status.BAD_REQUEST);
        } catch (Exception e) {
            return implCreateJSONExceptionResponse(e);
        } finally {
            if (parametersValid) {
                m_cacheMetrics.incrementRequestCount(group, RequestType.GETOBJECTS, System.currentTimeMillis() - requestStartTimeMillis);
            }
        }
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("CS Server shutdown finished");
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param metrics
     * @return
     */
    private Response implGetStatus(final boolean metrics) {
        final JSONObject jsonStatus = new JSONObject();

        try {
            // general
            jsonStatus.
                put(JSON_KEY_NAME, JSON_VALUE_CACHESERVICE).
                put(JSON_KEY_SERVERID, CacheUtils.CACHE_SERVER_ID).
                put(JSON_KEY_API, OX_CACHE_SERVER_API_VERSION).
                put(JSON_KEY_APITEXT, JSON_VALUE_APITEXT).
                put(JSON_KEY_STATUS, (m_running.get() && m_cache.isValid()) ? JSON_VALUE_UP : JSON_VALUE_DOWN).
                put(JSON_KEY_CACHE, m_cache.getJSONHealth());

            // metrics on demand
            if (metrics) {
                jsonStatus.put(JSON_KEY_METRICS, implGetJSONMetrics(m_cacheMetrics));
            }
        } catch (JSONException e) {
            LOG.error("CS Could not create JSON response for status request", e);
        }

        return Response.ok().type(MediaType.APPLICATION_JSON).entity(jsonStatus.toString()).build();
    }

    /**
     * @param contentArray
     * @param cacheMetrics
     *
     * @return
     */
    private static String implCreateCacheServiceServerHtmlPage(final String[] contentArray, @Nullable final CacheMetrics cacheMetrics) {
        final StringBuilder htmlBuilder = new StringBuilder(1024);

        htmlBuilder.append("<html>").append("<head><meta charset=\"UTF-8\"><title>").append(CACHE_SERVER_TITLE).append("</title></head>").append("<body><h1 align=\"center\">").append(CACHE_SERVER_TITLE).append("</h1>");

        // print server id in every case
        htmlBuilder.append("<p>Id: ").append(CacheUtils.CACHE_SERVER_ID).append("</p>");

        // print API version in every case
        htmlBuilder.append("<p>API: v").append(OX_CACHE_SERVER_API_VERSION).append("</p>");

        if (ArrayUtils.isNotEmpty(contentArray)) {
            for (int i = 0; i < contentArray.length;) {
                htmlBuilder.append("<p>").append(contentArray[i++]).append("</p>");
            }
        }

        if (null != cacheMetrics) {
            htmlBuilder.append("<br />").append("<p><u>Metrics</u></p>");

            final Method[] methods = cacheMetrics.getClass().getMethods();

            for (Method curMethod : methods) {
                if (null != curMethod) {
                    final String methodName = curMethod.getName();

                    if (StringUtils.isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (curMethod.getParameterTypes().length == 0)) {
                        final String metricsPropertyName = methodName.substring(3);
                        final boolean isRatioMetric = metricsPropertyName.endsWith("Ratio");

                        try {
                            final String numberString = String.valueOf(curMethod.invoke(cacheMetrics));

                            if (NumberUtils.isParsable(numberString)) {
                                htmlBuilder.append("<p>").append(metricsPropertyName).append(": ");

                                if (isRatioMetric) {
                                    htmlBuilder.append(Double.parseDouble(numberString));
                                } else {
                                    htmlBuilder.append(Long.parseLong(numberString));
                                }

                                htmlBuilder.append("</p>");
                            }
                        } catch (@SuppressWarnings("unused") Exception e) {
                            //
                        }
                    }
                }
            }
        }

        return htmlBuilder.append("</body></html>").toString();
    }

    /**
     * @param cacheMetrics
     * @return
     * @throws JSONException
     */
    private JSONObject implGetJSONMetrics(final CacheMetrics cacheMetrics) throws JSONException {
        final List<Method> methodList = Arrays.asList(cacheMetrics.getClass().getMethods());
        final JSONObject jsonMetrics = new JSONObject();

        methodList.sort(Comparator.comparing(Method::getName));

        for (Method curMethod : methodList) {
            if (null != curMethod) {
                final String methodName = curMethod.getName();

                if (StringUtils.isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (curMethod.getParameterTypes().length == 0)) {
                    final String metricsPropertyName = methodName.substring(3);
                    final boolean isRatioMetric = metricsPropertyName.contains("Ratio");

                    try {
                        final String numberString = String.valueOf(curMethod.invoke(cacheMetrics));

                        if (NumberUtils.isParsable(numberString)) {
                            if (isRatioMetric) {
                                jsonMetrics.put(metricsPropertyName, Double.parseDouble(numberString));
                            } else {
                                jsonMetrics.put(metricsPropertyName, Long.parseLong(numberString));
                            }
                        }
                    } catch (@SuppressWarnings("unused") Exception e) {
                        //
                    }
                }
            }
        }

        return jsonMetrics;
    }

    /**
     * @param statusType
     *
     * @return
     */
    private static Response implCreateJSONResponse(@Nullable final StatusType statusType) {
        final StatusType statusToUse = (null != statusType) ? statusType : Status.OK;

        return Response.status(statusToUse).type(MediaType.APPLICATION_JSON).entity(implCreateJSONResult(statusToUse.getStatusCode(), statusToUse.getReasonPhrase()).toString()).build();
    }

    /**
     * @param e
     * @return
     */
    private static Response implCreateJSONExceptionResponse(final Exception e) {
        return Response.serverError().type(MediaType.APPLICATION_JSON).entity(implCreateJSONResult(Status.INTERNAL_SERVER_ERROR.getStatusCode(), Throwables.getRootCause(e).getMessage()).toString()).build();
    }

    /**
     * @param code
     * @param message
     * @return
     */
    private static JSONObject implCreateJSONResult(final int code, @Nullable final String message) {
        try {
            return new JSONObject().put("code", code).put("message", (null != message) ? message : "n/a");
        } catch (JSONException e1) {
            LOG.error("CS Could not create JSON object with code {} and message {}: {}", Integer.toString(code), message, Throwables.getRootCause(e1).getMessage());
        }

        return new JSONObject();
    }

    /**
     * @param cacheObject
     * @param removeFileOnError
     * @param removeKeyOnError
     * @return
     */
    private boolean implCheckConsistency(@NonNull final ICacheObject cacheObject, final boolean removeFileOnError, final boolean removeKeyOnError) {

        try (final IReadAccess readAcc = m_cache.getReadAccess(cacheObject)) {
            // just a simple access test
            if (null != readAcc) {
                return true;
            }

            throw new IOException("Server not able to access object: " + cacheObject);
        } catch (Exception e) {
            if (removeFileOnError) {
                LOG.warn("CS Server consistency check failed => Removing object {} / {} / {}! (Reason: {})", cacheObject.getGroupId(), cacheObject.getKeyId(), cacheObject.getFileId(), Throwables.getRootCause(e).getMessage());

                try {
                    m_cache.remove(cacheObject);
                } catch (CacheException e1) {
                    LOG.error(Throwables.getRootCause(e1).getMessage());
                }
            } else if (removeKeyOnError) {
                LOG.warn("CS Server consistency check failed => Removing key {} / {}! (Reason: {})", cacheObject.getGroupId(), cacheObject.getKeyId(), Throwables.getRootCause(e).getMessage());

                try {
                    m_cache.removeKey(cacheObject.getGroupId(), cacheObject.getKeyId());
                } catch (CacheException e1) {
                    LOG.error(Throwables.getRootCause(e1).getMessage());
                }
            } else {
                LOG.error("CS Server consistency check failed for object {} / {} / {}! (Reason: {})", cacheObject.getGroupId(), cacheObject.getKeyId(), cacheObject.getFileId(), Throwables.getRootCause(e).getMessage());
            }
        }

        return false;
    }

    /**
     * @param group
     *
     * @return
     */
    private @Nullable String implGetValidatedGroup(@Nullable final String group) {
        final String validatedGroup = (null != group) ? group.trim() : null;

        // check if given group parameter matches allowed pattern for group
        if ((null == validatedGroup) || (!ALLOWED_GROUP_PATTERN.matcher(validatedGroup).matches())) {
            LOG.error("CS detected suspicious group request parameter not matching allowed pattern => Rejecting request: {}", validatedGroup);
            return null;
        }

        // prepend each given group with GROUPID_PREFIX
        return (group.startsWith(GROUPID_PREFIX) ? validatedGroup : (GROUPID_PREFIX + validatedGroup));
    }

    /**
     * @param id
     *
     * @return
     */
    private @Nullable String implGetValidatedId(@Nullable final String id) {
        final String validatedId = (null != id) ? id.trim() : null;

        if ((null == validatedId) || (!ALLOWED_ID_PATTERN.matcher(validatedId).matches())) {
            LOG.error("CS detected suspicious id request parameter not matching allowed pattern => Rejecting request: {}", validatedId);
            return null;
        }

        return validatedId;
    }

    /**
     * @param objectIds
     *
     * @return
     */
    private @Nullable Set<String> implGetValidatedIds(@Nullable final Set<String> objectIds) {
        final Set<String> validatedObjectIds = new HashSet<>();

        if (null != objectIds) {
            for (final String curId : objectIds) {
                final String curValidateObjectId = implGetValidatedId(curId);

                // clear set of already validated ids and leave immediately in case of a detected invalid id
                if (null == curValidateObjectId) {
                    validatedObjectIds.clear();
                    break;
                }

                validatedObjectIds.add(curValidateObjectId);
            }
        }

        // if an invalid id has been found, the set is empty => return null
        return (validatedObjectIds.size() > 0) ? validatedObjectIds : null;
    }


    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private ICache m_cache;

    final private CacheMetrics m_cacheMetrics;

    // - Static Members --------------------------------------------------------

    final private static Pattern ALLOWED_GROUP_PATTERN = Pattern.compile("^[a-zA-Z0-9_]+$");

    final private static Pattern ALLOWED_ID_PATTERN = Pattern.compile("^[a-zA-Z0-9_~@#:\\-]+$");

    final private static String GROUPID_PREFIX = "OX_";

    final private static String OX_CACHE_SERVER_API_VERSION = "1";

    final private static String CACHE_SERVER_TITLE = "OX Software GmbH CacheService server";

    final private static String[] CACHE_SERVER_OK = { "Code: 0", " Status: running..." };

    final private static String[] CACHE_SERVER_GONE = { "Code: 410", " terminated" };

    final private static String[] CACHE_SERVER_UNAVAILABLE = { "Code: 503", " Status: unavailable" };

    final private static String JSON_KEY_API = "api";

    final private static String JSON_KEY_APITEXT = "apitext";

    final private static String JSON_KEY_CACHE = "cache";

    final private static String JSON_KEY_CACHE_KEYCOUNT = "keyCount";

    final private static String JSON_KEY_CACHE_MAX_KEYAGE_MILLIS = "maxKeyAgeMillis";

    final private static String JSON_KEY_CACHE_GROUPLENGTH = "groupLength";

    final private static String JSON_KEY_GROUPS = "groups";

    final private static String JSON_KEY_SERVERID = "serverId";

    final private static String JSON_KEY_METRICS = "metrics";

    final private static String JSON_KEY_NAME = "name";

    final private static String JSON_KEY_STATUS = "status";

    final private static String JSON_VALUE_APITEXT = "API: v" + OX_CACHE_SERVER_API_VERSION;

    final private static String JSON_VALUE_DOWN = "DOWN";

    final private static String JSON_VALUE_CACHESERVICE = "CacheService";

    final private static String JSON_VALUE_UP = "UP";

    final private static String STR_ALL = "all";

    final private static String STR_TRUE = "true";
}
