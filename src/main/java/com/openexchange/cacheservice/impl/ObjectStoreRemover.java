/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;
import org.springframework.lang.NonNull;
import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheException;

/**
 * {@link ObjectStoreRemover}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectStoreRemover extends Thread {

    final private static String CLEANUP_FILENAME = "filestoredata.bin";

    final private static int MAX_REMOVE_FILE_COUNT = 999;

    /**
     * Initializes a new {@link ObjectStoreRemover}.
     */
    public ObjectStoreRemover(@NonNull Cache cache) {
        super("ObjectStoreRemover");

        m_cache = cache;
        m_cleanupFile = new File(CacheUtils.getDataDir(), CLEANUP_FILENAME);

        start();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        final ArrayList<ObjectStoreData> curRemoveList = new ArrayList<>(MAX_REMOVE_FILE_COUNT);

        // read all left entries from last shutdown first
        // and fill removal list with these entries
        implReadLeftOverObjectStoreData();

        while (isRunning()) {
            final boolean trace = LOG.isTraceEnabled();

            synchronized (m_objectStoreDataList) {
                if (!m_objectStoreDataList.isEmpty()) {
                    curRemoveList.ensureCapacity(MAX_REMOVE_FILE_COUNT);

                    for (final ListIterator<ObjectStoreData> iter = m_objectStoreDataList.listIterator(); iter.hasNext() && (curRemoveList.size() < MAX_REMOVE_FILE_COUNT);) {
                        // move entry from toDeleteList to work list
                        curRemoveList.add(iter.next());
                        iter.remove();
                    }
                }
            }

            // perform removal of FileStore files in work list
            if (!curRemoveList.isEmpty()) {
                try {
                    final int removedCount = CacheUtils.deleteObjectStoreObjects(m_cache, curRemoveList);

                    if (trace) {
                        LOG.trace("CS ObjectStoreRemover removed {} files ({} files left to be  removed)", removedCount, m_objectStoreDataList.size());
                    }
                } catch (CacheException e) {
                    CacheUtils.logExcp(e);
                } finally {
                    curRemoveList.clear();
                }
            }

            synchronized (m_objectStoreDataList) {
                if (m_objectStoreDataList.isEmpty()) {
                    try {
                        m_objectStoreDataList.wait(100);
                    } catch (@SuppressWarnings("unused") InterruptedException e) {
                        // ok
                    }
                }
            }
        }

        // make all left over entries to be deleted persistent after shutdown,
        // in order to be processed the next time the service is started
        implWriteLeftOverFileStoreData();
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            interrupt();
        }
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_running.get() && !interrupted();
    }

    /**
     * @param fileStoreData
     */
    public int addObjectStoreDatasToRemove(@NonNull final List<ObjectStoreData> objectStoreDataList) {
        synchronized (m_objectStoreDataList) {
            final int sizeBefore = m_objectStoreDataList.size();

            m_objectStoreDataList.addAll(objectStoreDataList);
            m_objectStoreDataList.notify();

            return (m_objectStoreDataList.size() - sizeBefore);
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    void implReadLeftOverObjectStoreData() {
        if (m_cleanupFile.canRead()) {
            try (final ObjectInputStream objInputStm = new ObjectInputStream(new FileInputStream(m_cleanupFile))) {
                final Object readObject = objInputStm.readObject();
                int size = 0;

                if (readObject instanceof Collection<?>) {
                    synchronized (m_objectStoreDataList) {
                        final int sizeBefore = m_objectStoreDataList.size();

                        m_objectStoreDataList.addAll((Collection<ObjectStoreData>) readObject);
                        size = m_objectStoreDataList.size() - sizeBefore;
                    }

                    if (LOG.isTraceEnabled()) {
                        LOG.trace("CS ObjectStoreRemover read {} files to be removed", size);
                    }
                } else {
                    LOG.error("CS Unexpected type of object when reading cleanupFile, ecxpected List<ObjectStoreData>");
                }
            } catch (Exception e) {
                LOG.error("CS Cleanup file could not be read (Reason: {})", Throwables.getRootCause(e).getMessage());
            }
        }
    }

    /**
     *
     */
    void implWriteLeftOverFileStoreData() {
        if (m_cleanupFile.getParentFile().canWrite()) {
            try (final ObjectOutputStream objOutputStm = new ObjectOutputStream(new FileOutputStream(m_cleanupFile))) {
                int size = 0;

                synchronized (m_objectStoreDataList) {
                    size = m_objectStoreDataList.size();
                    objOutputStm.writeObject(m_objectStoreDataList);
                }

                if (LOG.isTraceEnabled()) {
                    LOG.trace("CS ObjectStoreRemover wrote {} files to be removed after restart", size);
                }
            } catch (Exception e) {
                LOG.error("CS Cleanup file could not be written (Reason: {})", Throwables.getRootCause(e).getMessage());
            }
        } else {
            LOG.error("CS Cleanup file could not be written due to missing write permissions to: {}", m_cleanupFile.getAbsolutePath());
        }
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private Cache m_cache;

    final private File m_cleanupFile;

    final private LinkedList<ObjectStoreData> m_objectStoreDataList = new LinkedList<>();
}
