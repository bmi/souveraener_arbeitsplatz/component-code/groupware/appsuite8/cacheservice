/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl.adapters.s3;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.cacheservice.impl.adapters.sproxyd.SproxydAdapterConfig;
import io.micrometer.core.instrument.MeterRegistry.Config;

/**
 * {@link SproxydAdapterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class S3AdapterConfig {

    /**
     * {@link Builder}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class Builder {

        /**
         * Initializes a new {@link Builder}.
         */
        public Builder() {
            super();
        }

        /**
         * @return
         */
        public S3AdapterConfig build() {
            final S3AdapterConfig config = new S3AdapterConfig();

            config.m_endpoint = m_endpoint;
            config.m_region = m_region;
            config.m_accessKey = m_accessKey;
            config.m_secretKey = m_secretKey;
            config.m_bucketName = m_bucketName;
            config.m_socketFactory = m_socketFactory;

            return config;
        }

        /**
         * @param endpoint
         * @return
         */
        public Builder withEndpoint(final String endpoint) {
            m_endpoint = endpoint;
            return this;
        }

        /**
         * @param region
         * @return
         */
        public Builder withRegion(final String region) {
            m_region = region;
            return this;
        }

        /**
         * @param bucketName
         * @return
         */
        public Builder withBucketName(final String bucketName) {
            m_bucketName = bucketName;
            return this;
        }

        /**
         * @param accessKey
         * @return
         */
        public Builder withAccessKey(final String accessKey) {
            m_accessKey = accessKey;
            return this;
        }

        /**
         * @param secretKey
         * @return
         */
        public Builder withSecretKey(final String secretKey) {
            m_secretKey = secretKey;
            return this;
        }

        /**
         * @param socketFactory
         * @return
         */
        public Builder withSocketFactory(final SocketFactory socketFactory) {
            m_socketFactory = socketFactory;
            return this;
        }

        // - Members ---------------------------------------------------------------

        private String m_endpoint = null;

        private String m_region = "eu-central-1";

        private String m_bucketName = null;

        private String m_accessKey = null;

        private String m_secretKey = null;

        private SocketFactory m_socketFactory = SSLSocketFactory.getDefault();
    }

    /**
     * Initializes a new {@link Config}.
     */
    private S3AdapterConfig() {
        super();
    }

    /**
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * @return <code>true</code> if all necessary config values are set, <code>false</code> otherwise
     */
    public boolean isValid() {
        return StringUtils.isNotBlank(m_endpoint) && StringUtils.isNotBlank(m_region) && StringUtils.isNotBlank(m_bucketName) && StringUtils.isNotBlank(m_accessKey) && StringUtils.isNotBlank(m_secretKey);
    }

    /**
     * @return
     */
    public String getEndpoint() {
        return m_endpoint;
    }

    /**
     * @return
     */
    public String getRegion() {
        return m_region;
    }

    /**
     * @return
     */
    public String getBucketName() {
        return m_bucketName;
    }

    /**
     * @return
     */
    public String getAccessKey() {
        return m_accessKey;
    }

    /**
     * @return
     */
    public String getSecretKey() {
        return m_secretKey;
    }

    /**
     * @return
     */
    public SocketFactory getSocketFactory() {
        return m_socketFactory;
    }

    // - Members ---------------------------------------------------------------

    private String m_endpoint;

    private String m_region;

    private String m_bucketName;

    private String m_accessKey;

    private String m_secretKey;

    private SocketFactory m_socketFactory;
}
