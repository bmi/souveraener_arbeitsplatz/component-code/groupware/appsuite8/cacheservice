/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl.adapters.file;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.lang.NonNull;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.IObjectStore;

/**
 * {@link FileAdapter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class FileAdapter implements IObjectStore {

    final private static int FILE_PREFIX_NUMBER = 7073;

    final private static int BUFFER_SIZE = 8192;

    final private static String HASHED_SUBPATH = "fis1/hashed";

    /**
     * Unused
     *
     * Initializes a new {@link FileAdapter}.
     */
    @SuppressWarnings("unused")
    private FileAdapter() {
        super();

        m_storeId = 0;
        m_path = null;
        m_canonicalPath = null;
    }

    /**
     * Initializes a new {@link FileAdapter}.
     *
     * @param config
     * @param storeId
     * @throws CacheException
     */
    public FileAdapter(@NonNull final FileAdapterConfig config, final int storeId) throws CacheException {
        super();

        if (!config.isValid()) {
            throw new CacheException("Given file based config is not valid. A valid host must be configured");
        }

        if ((storeId < 1) || (Integer.toString(storeId).length() > 5)) {
            throw new CacheException("Given file based store id must be a number between 1 and 99999");
        }

        m_storeId = storeId;
        m_path = new File(config.getPath(), HASHED_SUBPATH);

        try {
            m_canonicalPath = m_path.getCanonicalPath();
        } catch (IOException e) {
            throw new CacheException(e);
        }
    }

    @Override
    public int getId() {
        return m_storeId;
    }

    @Override
    public String createObject(File inputFile) throws CacheException {
        if (null == inputFile) {
            throw new CacheException("Input file for object to create must not be null");
        }

        final String objectName = implCreateObjectParentDirAndGetObjectName();
        final File fileToCreate = new File(m_path, objectName);

        implCheckForValidChildFile(fileToCreate);
        implCopyFile(inputFile, fileToCreate);

        return objectName;
    }

    @Override
    public void getObject(String objectId, File outputFile) throws CacheException {
        if (null == objectId) {
            throw new CacheException("Object id for object to read must not be null");
        }

        if (null == outputFile) {
            throw new CacheException("Object content output file for object to read must not be null and writable");
        }

        final File fileToGet = new File(m_path, objectId);

        implCheckForValidChildFile(fileToGet);
        implCopyFile(fileToGet, outputFile);
    }

    @Override
    public void deleteObject(String objectId) throws CacheException {
        if (null == objectId) {
            throw new CacheException("ObjectId for object to delete must not be null");
        }

        final File fileToDelete = new File(m_path, objectId);

        implCheckForValidChildFile(fileToDelete);

        if (!fileToDelete.delete()) {
            throw new CacheException("Not able to delete object: " + fileToDelete.getAbsolutePath());
        }
    }

    @Override
    public Set<String> deleteObjects(String[] objectIds) throws CacheException {
        if (ArrayUtils.isEmpty(objectIds)) {
            throw new CacheException("Object id array for objects to delete must not be null");
        }

        final Set<String> leftOverObjects = new HashSet<>();

        for (final String curObjectId : objectIds) {
            final File curFileToDelete = new File(m_path, curObjectId);

            try {
                implCheckForValidChildFile(curFileToDelete);

                if (!curFileToDelete.delete()) {
                    leftOverObjects.add(curObjectId);
                }
            } catch (CacheException e) {
                // just log invalid entry but don't add to valid left over objects
                LOG.error("CS received exception when deleting file based objects => Ignoring current file for object id: " + curObjectId, e);
            }
        }

        return leftOverObjects;
    }

    // - Implementation --------------------------------------------------------

    private String implCreateObjectParentDirAndGetObjectName() throws CacheException {
        final UUID objectUUID = UUID.randomUUID();
        final String objectId = Integer.toHexString(objectUUID.hashCode());
        final StringBuilder objectSubPathBuilder = new StringBuilder(32);

        // fill up with double hex chars up to prefix length 6, use zeros if length < 6
        for (int i = 0, objectIdLength = objectId.length(); i < 6; ++i) {
            objectSubPathBuilder.append((i < objectIdLength) ? objectId.charAt(i) : '0');

            if ((i & 1) == 1) {
                objectSubPathBuilder.append('/');
            }
        }

        final File objectFileParentDir = new File(m_path, objectSubPathBuilder.toString());

        // create parent dir of new object if it does not exist yet
        if (!objectFileParentDir.exists() && !objectFileParentDir.mkdirs()) {
            throw new CacheException("Not able to create new object parent directory: " + objectFileParentDir.getAbsolutePath());
        }

        final String objectFilename = objectUUID.toString();

        for (int i = 0, objectFilenameLength = objectFilename.length(); i < objectFilenameLength; ++i) {
            final char curChar = objectFilename.charAt(i);

            if ('-' != curChar) {
                objectSubPathBuilder.append(curChar);
            }
        }

        return objectSubPathBuilder.toString();
    }

    /**
     * @param inputFile
     * @param outputFile
     * @throws IOException
     */
    private void implCopyFile(@NonNull final File inputFile, @NonNull final File outputFile) throws CacheException {
        final byte[] buffer = new byte[BUFFER_SIZE];
        int curRead = 0;

        try (final InputStream inputStream = new FileInputStream(inputFile);
             final OutputStream outputStream = new FileOutputStream(outputFile)) {

            while ((curRead = inputStream.read(buffer)) > -1) {
                outputStream.write(buffer, 0, curRead);
            }

            outputStream.flush();
        } catch (IOException e) {
            throw new CacheException(e);
        }
    }

    /**
     * @param fileToCheck
     *
     * @throws CacheException
     */
    void implCheckForValidChildFile(@NonNull final File fileToCheck) throws CacheException {
        try {
            final String fileToCheckCanonicalPath = fileToCheck.getCanonicalPath();

            // the given file path must be a child path of the file cache root path
            // and if the file already exists, it needs to be a real file as well
            // (e.g. for delete operations)
            if (!fileToCheckCanonicalPath.startsWith(m_canonicalPath) || (fileToCheck.exists() && !fileToCheck.isFile())) {
                throw new CacheException("Invalid child path for file based entry: " + fileToCheck.getPath());
            }
        } catch (IOException e) {
            throw new CacheException(e);
        }
    }

    // - Members ---------------------------------------------------------------

    final private int m_storeId;

    final private File m_path;

    final String m_canonicalPath;
}
