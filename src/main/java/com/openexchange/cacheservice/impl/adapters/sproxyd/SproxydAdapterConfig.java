/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl.adapters.sproxyd;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.net.MalformedURLException;
import java.net.URL;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Throwables;
import io.micrometer.core.instrument.MeterRegistry.Config;

/**
 * {@link SproxydAdapterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class SproxydAdapterConfig {

    /**
     * DELIMITER
     */
    final private static String DELIMITER = "/";

    /**
     * {@link Builder}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class Builder {

        /**
         * Initializes a new {@link Builder}.
         */
        public Builder() {
            super();
        }

        /**
         * @return
         */
        public SproxydAdapterConfig build() {
            final SproxydAdapterConfig config = new SproxydAdapterConfig();

            config.m_endpoint = m_endpoint;
            config.m_path = m_path;
            config.m_maxConnections = m_maxConnections;
            config.m_connectTimeoutMillis = m_connectTimeoutMillis;
            config.m_socketTimeoutMillis = m_socketTimeoutMillis;
            config.m_heartbeatIntervalMillis = m_heartbeatIntervalMillis;
            config.m_socketFactory = m_socketFactory;

            return config;
        }

        /**
         * @param host
         * @return
         */
        public Builder withEndpoint(final String endpoint) {
            if (null != (m_endpoint = endpoint)) {
                m_endpoint = m_endpoint.trim();

                while (m_endpoint.endsWith(DELIMITER)) {
                    m_endpoint = m_endpoint.substring(0, m_endpoint.length() - 1);
                }

                if (StringUtils.isBlank(m_endpoint)) {
                    m_endpoint = null;
                }
            }

            return this;
        }

        /**
         * @param path
         * @return
         */
        public Builder withPath(final String path) {
            if (null != (m_path = path)) {
                m_path = m_path.trim();

                while (m_path.startsWith(DELIMITER)) {
                    m_path = m_path.substring(1);
                }
            }

            if (StringUtils.isBlank(m_path)) {
                m_path = null;
            }

            return this;
        }

        /**
         * @param maxConnections
         * @return
         */
        public Builder withMaxConnections(final int maxConnections) {
            m_maxConnections = maxConnections;
            return this;
        }

        /**
         * @param connectTimeoutMillis
         * @return
         */
        public Builder withConnectTimeoutMillis(final long connectTimeoutMillis) {
            m_connectTimeoutMillis = connectTimeoutMillis;
            return this;
        }

        /**
         * @param socketTimeoutMillis
         * @return
         */
        public Builder withSocketTimeoutMillis(final long socketTimeoutMillis) {
            m_socketTimeoutMillis = socketTimeoutMillis;
            return this;
        }

        /**
         * @param heartbeatIntervalMillis
         * @return
         */
        public Builder withHeartbeatIntervalMillis(final long heartbeatIntervalMillis) {
            m_heartbeatIntervalMillis = heartbeatIntervalMillis;
            return this;
        }

        public Builder withSocketFactory(final SocketFactory socketFactory) {
            m_socketFactory = socketFactory;
            return this;
        }

        // - Members ---------------------------------------------------------------

        private String m_endpoint = null;

        private String m_path = "/";

        private int m_maxConnections = 100;

        private long m_connectTimeoutMillis = 5000;

        private long m_socketTimeoutMillis = 30000;

        private long m_heartbeatIntervalMillis = 60000;

        private SocketFactory m_socketFactory = SSLSocketFactory.getDefault();
    }

    /**
     * Initializes a new {@link Config}.
     */
    private SproxydAdapterConfig() {
        super();
    }

    /**
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * @return <code>true</code> if all necessary config values are set, <code>false</code> otherwise
     */
    public boolean isValid() {
        return (StringUtils.isNotBlank(m_endpoint));
    }

    /**
     * @return
     */
    public String getBaseURLString() {
        if (isValid()) {
            final StringBuilder baseURLBuilder = new StringBuilder(256).append(m_endpoint).append(DELIMITER);

            if (StringUtils.isNotBlank(m_path)) {
                baseURLBuilder.append(m_path).append(DELIMITER);
            }

            final String urlStr = baseURLBuilder.toString();

            try {
                // just check if URL string is valid
                new URL(urlStr);
                return urlStr;
            } catch (MalformedURLException e) {
                LOG.error("CS Not able to create valid host URL from given config", Throwables.getRootCause(e));
            }
        }

        return null;
    }

    /**
     * getHost
     */
    public String getEndpoint() {
        return m_endpoint;
    }

    /**
     * @return
     */
    public String getPath() {
        return m_path;
    }

    /**
     * @return
     */
    public int getMaxConnections() {
        return m_maxConnections;
    }

    /**
     * @return
     */
    public long getConnectTimeoutMillis() {
        return m_connectTimeoutMillis;
    }

    /**
     * @return
     */
    public long getSocketTimeoutMillis() {
        return m_socketTimeoutMillis;
    }

    /**
     * @return
     */
    public long getHeartbeatIntervalMillis() {
        return m_heartbeatIntervalMillis;
    }

    /**
     * @return
     */
    public SocketFactory getSocketFactory() {
        return m_socketFactory;
    }

    // - Members ---------------------------------------------------------------

    private String m_endpoint;

    private String m_path;

    private int m_maxConnections;

    private long m_connectTimeoutMillis;

    private long m_socketTimeoutMillis;

    private long m_heartbeatIntervalMillis;

    private SocketFactory m_socketFactory;
}
