/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl.adapters.file;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import com.google.common.base.Throwables;
import io.micrometer.core.instrument.MeterRegistry.Config;

/**
 * {@link FileAdapterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class FileAdapterConfig {

    /**
     * {@link FileAdapterConfig#Builder}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class Builder {

        /**
         * Initializes a new {@link Builder}.
         */
        public Builder() {
            super();
        }

        /**
         * @return
         */
        public FileAdapterConfig build() {
            final FileAdapterConfig config = new FileAdapterConfig();

            config.m_path = m_path;

            return config;
        }

        /**
         * @param path
         * @return
         */
        public Builder withPath(final File path) {
            m_path = path;
            return this;
        }

        // - Members ---------------------------------------------------------------

        private File m_path = null;
    }

    /**
     * Initializes a new {@link Config}.
     */
    private FileAdapterConfig() {
        super();
    }

    /**
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * @return <code>true</code> if all config values are set and valid, <code>false</code> otherwise
     */
    public boolean isValid() {
        // check or create file based ObjectStore directory
        if (null != m_path) {
            if (m_path.isDirectory() && m_path.canWrite()) {
                return true;
            }

            try {
                FileUtils.forceMkdir(m_path);
                return true;
            } catch (IOException e) {
                LOG.error("CS Not able to create file based ObjectStore directory: {} ({})", m_path.getAbsolutePath(), Throwables.getRootCause(e).getMessage());
            }
        }

        return false;
    }

    /**
     * @return
     */
    public File getPath() {
        return m_path;
    }

    // - Members ---------------------------------------------------------------

    private File m_path;
}
