/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl.adapters.sproxyd;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.lang.NonNull;
import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.IObjectStore;
import com.openexchange.cacheservice.api.ObjectStoreResult;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import okhttp3.ConnectionPool;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * {@link SproxydAdapter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class SproxydAdapter implements IObjectStore {

    final private static int SPROXYDSTORE_PREFIX_NUMBER = 8380;
    final private static long DEFAULT_CONNECTION_POOL_IDLETIME_MILLIS = 600000;
    final private static long CHUNK_SIZE = 1024 * 1024;
    final private static String JSON_KEY_CHUNK_INFO = "chunkInfo";
    final private static String JSON_KEY_OBJECT_LENGTH = "objectLength";
    final private static String JSON_KEY_MAX_CHUNK_SIZE = "maxChunkSize";
    final private static String JSON_KEY_CHUNK_NAMES = "chunkNames";
    final private static MediaType MEDIA_TYPE_OCTET_STREAM = MediaType.parse("application/octet-stream");
    final private static MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
    final private static List<String> STR_LIST_EMPTY = new ArrayList<>();

    /**
     * Unused
     */
    @SuppressWarnings("unused")
    private SproxydAdapter() {
        super();

        m_config = null;
        m_storeId = 0;
        m_httpClient = null;
        m_baseURLStr = null;
    }

    /**
     * Initializes a new {@link SproxydAdapter}.
     *
     * @param config
     * @param storeId
     * @throws CacheException
     */
    public SproxydAdapter(@NonNull final SproxydAdapterConfig config, final int storeId) throws CacheException {
        super();

        if (!config.isValid()) {
            throw new CacheException("Given SproxyD based ObjectStore configuration is not valid. A valid host must be configured");
        }

        if ((storeId < 1) || (Integer.toString(storeId).length() > 5)) {
            throw new CacheException("Given SproxyD based ObjectStore id must be a number between 1 and 99999");
        }

        m_config = config;
        m_storeId = Integer.parseInt(("" + SPROXYDSTORE_PREFIX_NUMBER) + storeId);
        m_baseURLStr = m_config.getBaseURLString();

        try {
            // TODO (KA): impl! socketFactory(m_config.getSocketFactory()).
            m_httpClient = new OkHttpClient.Builder().
                connectionPool(new ConnectionPool(m_config.getMaxConnections(), DEFAULT_CONNECTION_POOL_IDLETIME_MILLIS, TimeUnit.MILLISECONDS)).
                retryOnConnectionFailure(true).connectTimeout(m_config.getConnectTimeoutMillis(), TimeUnit.MILLISECONDS).
                readTimeout(m_config.getSocketTimeoutMillis(), TimeUnit.MILLISECONDS).
                writeTimeout(m_config.getSocketTimeoutMillis(), TimeUnit.MILLISECONDS).
                followRedirects(false).
                followSslRedirects(false).
                build();
        } catch (Exception e) {
            LOG.error("CS SproxyD based ObjectStore could not create HttpClient", e);
            throw new CacheException(e);
        }

        if (LOG.isInfoEnabled()) {
            LOG.info("CS SproxyD based ObjectStore successfully initialized [endpoint: {}, path: {}]", config.getEndpoint(), config.getPath());
        }
    }

    /**
     *
     */
    @Override
    public int getId() {
        return m_storeId;
    }

    /**
     *
     */
    @Override
    public String createObject(final File inputFile) throws CacheException {
        if (null == inputFile) {
            throw new CacheException("Input file for object to create must not be null");
        }

        final String objectId = StringUtils.remove(UUID.randomUUID().toString(), '-');

        if (null == objectId) {
            throw new CacheException("Object id for object to update must not be null");
        }

        if (null == inputFile) {
            throw new CacheException("Input file for object to update must not be null");
        }

        final List<String> writtenChunkList = new LinkedList<>();
        final long inputFileLen = inputFile.length();
        final long chunkCount = (inputFileLen / CHUNK_SIZE) + (((inputFileLen % CHUNK_SIZE) > 0) ? 1 : 0);
        AtomicLong bytesWritten = new AtomicLong(0);

        // write single chunks
        for (int chunkPos = 0; chunkPos++ < chunkCount;) {
            final String chunkName = objectId + '.' + chunkPos;
            final byte[] chunk = new byte[(int) Math.min(CHUNK_SIZE, inputFileLen - bytesWritten.get())];

            try (final RandomAccessFile raFile = new RandomAccessFile(inputFile, "r")) {
                raFile.seek(bytesWritten.get());
                raFile.read(chunk);

                final Callable<ObjectStoreResult<Void>> putChunkCallable = (() -> {
                    try (final Response putChunkResponse = m_httpClient.newCall(implGetRequestBuilder(chunkName).put(RequestBody.create(chunk, MEDIA_TYPE_OCTET_STREAM)).build()).execute()) {
                        if (putChunkResponse.isSuccessful()) {
                            bytesWritten.addAndGet(chunk.length);
                            writtenChunkList.add(chunkName);

                            return new ObjectStoreResult<>();
                        }

                        return new ObjectStoreResult<>(putChunkResponse.code());
                    } catch (Exception e) {
                        return new ObjectStoreResult<>(e);
                    }
                });

                final ObjectStoreResult<Void> curResult = implExecute(putChunkCallable);

                if (curResult.isNotOk()) {
                    // TODO (KA): delete written chunk list?
                    throw new CacheException(curResult.getOrCreateExceptionIfNotOk());
                }
            } catch (IOException e) {
                // TODO (KA): delete written chunk list?
                throw new CacheException(e);
            }
        }

        // write chunk info as JSON object with given objectId
        if (inputFileLen == bytesWritten.get()) {
            try {
                final String jsonContentStr = new JSONObject().put(JSON_KEY_CHUNK_INFO, new JSONObject().put(JSON_KEY_OBJECT_LENGTH, inputFileLen).put(JSON_KEY_MAX_CHUNK_SIZE, CHUNK_SIZE).put(JSON_KEY_CHUNK_NAMES, new JSONArray(writtenChunkList))).toString();

                final Callable<ObjectStoreResult<Void>> putChunkInfoCallable = (() -> {
                    try (final Response putChunkInfoResponse = m_httpClient.newCall(implGetRequestBuilder(objectId).put(RequestBody.create(jsonContentStr, MEDIA_TYPE_JSON)).build()).execute()) {
                        return putChunkInfoResponse.isSuccessful() ? new ObjectStoreResult<>() : new ObjectStoreResult<>(putChunkInfoResponse.code());
                    } catch (Exception e) {
                        return new ObjectStoreResult<>(e);
                    }
                });

                final ObjectStoreResult<Void> curResult = implExecute(putChunkInfoCallable);

                if (curResult.isNotOk()) {
                    // TODO (KA): delete written chunk list?
                    throw new CacheException(curResult.getOrCreateExceptionIfNotOk());
                }
            } catch (JSONException e) {
                // TODO (KA): delete written chunk list?
                throw new CacheException(e);
            }
        } else {
            // TODO (KA): delete written chunk list?
            throw new CacheException("Total size of written chunks does not match expected size");
        }

        return objectId;
    }

    /**
     *
     */
    @Override
    public void getObject(String objectId, File outputFile) throws CacheException {
        if (null == objectId) {
            throw new CacheException("Object id for object to read must not be null");
        }

        if (null == outputFile) {
            throw new CacheException("Object content output file for object to read must not be null and writable");
        }

        final List<ObjectStoreResult<Void>> errorResutList = new ArrayList<>();

        // read single chunks after retrieving the chunk names from JSON chunk info object with given objectId
        implGetObjectChunkNames(objectId).forEach((curChunkName) -> {
            final Callable<ObjectStoreResult<Void>> getChunkCallable = (() -> {
                try (final Response getChunkResponse = m_httpClient.newCall(implGetRequestBuilder(curChunkName).get().build()).execute()) {
                    if (getChunkResponse.isSuccessful()) {
                        final ResponseBody getChunkResponseBody = getChunkResponse.body();

                        if (getChunkResponseBody.contentLength() > 0) {
                            try (final RandomAccessFile raFile = new RandomAccessFile(outputFile, "rwd")) {
                                raFile.seek(outputFile.length());

                                try (final InputStream inputStm = getChunkResponseBody.byteStream()) {
                                    raFile.write(IOUtils.toByteArray(inputStm));
                                    return new ObjectStoreResult<>();
                                }
                            }
                        }

                        return new ObjectStoreResult<>(new CacheException("Invalid chunk content in GET response although response code is ok"));
                    }

                    return new ObjectStoreResult<>(getChunkResponse.code());
                } catch (Exception e) {
                    return new ObjectStoreResult<>(e);
                }
            });

            final ObjectStoreResult<Void> curResult = implExecute(getChunkCallable);

            if (curResult.isNotOk()) {
                errorResutList.add(curResult);
            }
        });

        // throw exception if one of the previous operations did not succeed
        if (!errorResutList.isEmpty()) {
            throw new CacheException(errorResutList.get(0).getOrCreateExceptionIfNotOk());
        }
    }

    /**
     *
     */
    @Override
    public void deleteObject(String objectId) throws CacheException {
        if (null == objectId) {
            throw new CacheException("ObjectId for object to delete must not be null");
        }

        try {
            // delete single chunks after retrieving chunk names from JSON chunk info object with given objectId
            implGetObjectChunkNames(objectId).forEach(this::implDelete);
        } catch (CacheException e) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("CS Not able to delete object: " + objectId, e);
            }
        }

        // delete chunk info object with given objectId
        implDelete(objectId);
    }

    /**
     *
     */
    @Override
    public Set<String> deleteObjects(String[] objectIds) throws CacheException {
        if (ArrayUtils.isEmpty(objectIds)) {
            throw new CacheException("Object id array for objects to delete must not be null");
        }

        final Set<String> leftOverObjectIdSet = new HashSet<>();
        final boolean trace = LOG.isTraceEnabled();

        for (final String curObjectId : objectIds) {
            try {
                deleteObject(curObjectId);
            } catch (Exception e) {
                leftOverObjectIdSet.add(curObjectId);

                if (trace) {
                    LOG.trace("CS Not able to delete object from delete objects list: " + curObjectId, e);
                }
            } finally {
                Thread.yield();
            }
        }

        return leftOverObjectIdSet;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param id
     *
     * @return
     */
    private Request.Builder implGetRequestBuilder(@NonNull String id) {
        try {
            return new Request.Builder().url(new URL(m_baseURLStr + id));
        } catch (Exception e) {
            LOG.error(Throwables.getRootCause(e).getMessage());
        }

        return null;
    }

    /**
     * @param objectId
     * @return the list of object chnunk names for the given chunk info object
     * @throws CacheException
     */
    private List<String> implGetObjectChunkNames(@NonNull final String objectId) throws CacheException {
        // The Failsafe callable to be executed one or more times up to the retry limit
        final Callable<ObjectStoreResult<List<String>>> getChunkNamesCallable = (() -> {
            try (final Response getInfoResponse = m_httpClient.newCall(implGetRequestBuilder(objectId).get().build()).execute()) {
                if (getInfoResponse.isSuccessful()) {
                    final ResponseBody responseBody = getInfoResponse.body();

                    if (responseBody.contentLength() > 0) {
                        final JSONObject jsonChunkInfo = new JSONObject(responseBody.string()).optJSONObject(JSON_KEY_CHUNK_INFO);

                        if ((null != jsonChunkInfo) && jsonChunkInfo.has(JSON_KEY_CHUNK_NAMES)) {
                            return new ObjectStoreResult<>((List) jsonChunkInfo.getJSONArray(JSON_KEY_CHUNK_NAMES).toList());
                        }
                    }

                    return new ObjectStoreResult<>(new CacheException("Invalid get chunk name response although response code is ok"));
                }

                return new ObjectStoreResult<>(getInfoResponse.code());
            } catch (Exception e) {
                return new ObjectStoreResult<>(e);
            }
        });

        final ObjectStoreResult<List<String>> result = implExecute(getChunkNamesCallable);

        if (result.isNotOk()) {
            throw new CacheException(result.getOrCreateExceptionIfNotOk());
        }

        return (result.hasResult()) ? result.getResult() : STR_LIST_EMPTY;
    }

    /**
     * @param elementId
     * @throws CacheException
     */
    private void implDelete(@NonNull final String elementId) {
        // The Failsafe callable to be executed one or more times up to the retry limit
        final Callable<ObjectStoreResult<Void>> deleteCallable = (() -> {
            try (final Response deleteResponse = m_httpClient.newCall(implGetRequestBuilder(elementId).delete().build()).execute()) {
                return deleteResponse.isSuccessful() ? new ObjectStoreResult<>() : new ObjectStoreResult<>(deleteResponse.code());
            } catch (Exception e) {
                return new ObjectStoreResult<>(e);
            }
        });

        implExecute(deleteCallable);
    }

    /**
     * @param executor
     * @return
     */
    private <T> ObjectStoreResult<T> implExecute(@NonNull final Callable<ObjectStoreResult<T>> executor) {
        final RetryPolicy<ObjectStoreResult<T>> retryPolicy = new RetryPolicy<ObjectStoreResult<T>>().withMaxRetries(3).withDelay(10, 100, ChronoUnit.MILLIS).handleResultIf(ObjectStoreResult::isNotOk);

        return Failsafe.with(retryPolicy).get(executor::call);
    }

    // - Members ---------------------------------------------------------------

    final private SproxydAdapterConfig m_config;

    final private int m_storeId;

    final private String m_baseURLStr;

    final private OkHttpClient m_httpClient;
}
