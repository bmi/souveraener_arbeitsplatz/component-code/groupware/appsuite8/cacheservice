/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import javax.sql.DataSource;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheConfig;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.GroupConfig;
import com.openexchange.cacheservice.api.ICache;
import com.openexchange.cacheservice.api.ICache.DatabaseType;
import com.openexchange.cacheservice.api.KeyObject;

/**
 * {@link CacheDatabase}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@Repository
public class CacheDatabase {

    final private static String DB_VARCHAR_TYPE = "VARCHAR(190)";
    final private static String EMPTY_JSON_OBJECT_STR = "{}";

    /**
     * @param dataSource
     * @throws CacheException
     */
    @Autowired
    public CacheDatabase(@NonNull final DataSource dataSource, @NonNull final CacheConfig cacheConfig) throws CacheException {
        super();
        m_writeDataSource = m_readDataSource = dataSource;
        m_cacheConfig = cacheConfig;
    }

    /**
     * @throws CacheException
     */
    public void initialize() throws CacheException {
        try {
            if (isValid()) {
                // try to read existing groups
                final String selectSql = STR_BUILDER("SELECT fa.GroupId, fa.PropertyKeys, fa.Sync, fa.Config FROM ").append(TBL_GROUPADMIN("fa")).toString();

                synchronized (m_groupMap) {
                    try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(selectSql)) {

                        try (final ResultSet resultSet = stmt.executeQuery()) {
                            if (null != resultSet) {
                                while (resultSet.next()) {
                                    final String curGroupId = resultSet.getString(1);

                                    if (CacheUtils.isValid(curGroupId)) {
                                        // create and insert GroupConfig for currently read group, if not available yet
                                        final Set<String> customKeySet = new LinkedHashSet<>();
                                        final String keysStr = resultSet.getString(2);

                                        // add to custom key set if it is not a default key
                                        if (isNotBlank(keysStr)) {
                                            for (final String curKey : keysStr.split(",", 0)) {
                                                if (isNotBlank(curKey)) {
                                                    final String targetKey = curKey.trim();

                                                    if (!ArrayUtils.contains(GroupConfig.DEFAULT_KEYS, targetKey)) {
                                                        customKeySet.add(targetKey);
                                                    }
                                                }
                                            }
                                        }

                                        final GroupConfig.Builder readConfigBuilder = GroupConfig.builder();
                                        final boolean sync = resultSet.getBoolean(3);
                                        final String configStr = resultSet.getString(4);

                                        readConfigBuilder.withGroupId(curGroupId);
                                        readConfigBuilder.withCustomKeys(customKeySet.toArray(new String[0]));

                                        // a stored config (available for newer releases >= 7.10.6) is always put on top
                                        if (StringUtils.isNotBlank(configStr)) {
                                            readConfigBuilder.withJSON(new JSONObject(configStr));
                                        }

                                        readConfigBuilder.withUseCountTable(implCreateTriggers(curGroupId) && (!sync || implSyncGroup(curGroupId)));

                                        final GroupConfig updatedGroupConfig = readConfigBuilder.build();

                                        if (implUpdateGroupConfig(updatedGroupConfig)) {
                                            m_groupMap.put(curGroupId, updatedGroupConfig);
                                        }
                                    }
                                }
                            }
                        } finally {
                            // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                            conWrapper.commit();
                        }
                    } catch (Exception e) {
                        throw new CacheException(e);
                    }
                }
            }
        } finally {
            m_initializing.set(false);

            m_initializedLock.lock();

            try {
                m_initializedCondition.signalAll();
            } finally {
                m_initializedLock.unlock();
            }
        }
    }

    // - Database access methods -----------------------------------------------

    /**
     * blocks the calling thread until this instance will have been initialized completely
     */
    public void waitUntilInitialized() {
        while (m_initializing.get()) {
            m_initializedLock.lock();

            try {
                m_initializedCondition.await();
            } catch (@SuppressWarnings("unused") InterruptedException e) {
                // ok
            } finally {
                m_initializedLock.unlock();
            }
        }
    }

    /**
     * @return
     */
    public boolean isValid() {
        return (getDatabaseType() != DatabaseType.NONE);
    }

    /**
     * @return
     */
    public ICache.DatabaseType getDatabaseType() {
        return m_databaseType;
    }

    /**
     * @param groupConfig
     * @return
     * @throws CacheException
     */
    public GroupConfig registerGroup(@NonNull GroupConfig groupConfig) throws CacheException {
        final String groupId = groupConfig.getGroupId();
        final GroupConfig.Builder configBuilder = GroupConfig.builder().withConfig(groupConfig);

        synchronized (m_groupMap) {
            final Set<String> defaultKeys = groupConfig.getKeys();
            final Set<String> registeredKeys = new LinkedHashSet<>();
            final Set<String> registeredCustomKeys = new LinkedHashSet<>();

            // create Content and Property tables for new group
            if (!m_groupMap.containsKey(groupId)) {
                implCreateGroupTables(groupConfig);

                defaultKeys.forEach((curKey) -> {
                    try {
                        implInsertPropertyKey(groupId, registeredKeys, curKey, "BIGINT(20)", "NOT NULL");
                    } catch (CacheException e) {
                        // Ignorable exception since column might already exists at tables
                        if (LOG.isTraceEnabled()) {
                            LOG.trace("CS Default key column already exists for groupId: {} => {} (Reason: {})", groupId, curKey, Throwables.getRootCause(e).getMessage());
                        }
                    }
                });
            } else {
                registeredKeys.addAll(groupConfig.getKeys());
            }

            // add custom keys if they're not contained yet
            final Set<String> configCustomKeys = groupConfig.getCustomKeys();

            if (configCustomKeys.size() > 0) {
                configCustomKeys.forEach((curKey) -> {
                    if (CacheUtils.isValid(curKey) && !registeredKeys.contains(curKey)) {
                        try {
                            implInsertPropertyKey(groupId, registeredKeys, curKey, "TEXT", "NULL");
                        } catch (CacheException e) {
                            // Ignorable exception since column might already exists at tables
                            if (LOG.isTraceEnabled()) {
                                LOG.trace("CS CustomKey column already exists for groupId: => {} (Reason: {})", groupId, curKey, Throwables.getRootCause(e).getMessage());
                            }
                        }
                    }
                });
            }

            // finally determine all custom keys that have been registered and update GroupConfig
            registeredKeys.forEach((curKey) -> {
                if (!defaultKeys.contains(curKey)) {
                    registeredCustomKeys.add(curKey);
                }
            });

            final GroupConfig updatedGroupConfig = configBuilder.withCustomKeys(registeredCustomKeys.toArray(new String[0])).withUseCountTable(implCreateTriggers(groupId)).build();

            if (implUpdateGroupConfig(updatedGroupConfig)) {
                m_groupMap.put(groupId, updatedGroupConfig);
            }

            return updatedGroupConfig;
        }
    }

    /**
     * @param groupId
     * @return
     */
    public GroupConfig getGroupConfig(@NonNull final String groupId) {
        return m_groupMap.get(groupId);
    }

    /**
     * @param groupId
     * @return
     */
    public Set<String> getPropertyKeys(@NonNull final String groupId) {
        return implGetKeySet(groupId, false);
    }

    /**
     * @param groupId
     * @return
     */
    public Set<String> getCustomPropertyKeys(@NonNull final String groupId) {
        return implGetKeySet(groupId, true);
    }

    /**
     * @param groupId
     * @param customKey
     * @return
     */
    public boolean hasCustomPropertyKey(@NonNull final String groupId, @NonNull final String customKey) {
        return implGetKeySet(groupId, true).contains(customKey);
    }

    /**
     * @param groupId
     * @return
     */
    public JSONObject getUserData(@NonNull final String groupId) {
        final GroupConfig groupConfig = m_groupMap.get(groupId);

        return null != groupConfig ? groupConfig.getUserData() : new JSONObject();
    }

    /**
     * @param groupId
     * @param userData
     * @return
     * @throws CacheException
     */
    public boolean setUserData(@NonNull final String groupId, @NonNull final JSONObject userData) throws CacheException {
        final GroupConfig groupConfiguration = m_groupMap.get(groupId);
        String userDataText = userData.toString();

        if (null != groupConfiguration && null != userDataText) {
            final String updateGroupUserDataSql = STR_BUILDER(" ").append(TBL_GROUPADMIN("fa")).append(" SET fa.=?").append(" WHERE fa.GroupId=?").toString();

            Connection usedCon = null;

            for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
                try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final PreparedStatement fgUserDataUpdateStmt = conWrapper.prepareStatement(updateGroupUserDataSql)) {

                    usedCon = conWrapper.getConnection();

                    // delete/insert groupId in Groups table
                    fgUserDataUpdateStmt.setString(1, userDataText);
                    fgUserDataUpdateStmt.setString(2, groupId);

                    // execute all statements and commit
                    fgUserDataUpdateStmt.executeUpdate();

                    usedCon.commit();

                    return true;
                } catch (Exception e) {
                    final String cause = "Exception occured when trying to update UserData column for GroupId: " + groupId + " (" +
                                         Throwables.getRootCause(e).getMessage() + ')';

                    implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, cause);
                }
            }
        }

        return false;
    }

    /**
     * @param cacheObject
     * @param objectStoreData
     * @param cacheObjectProperties
     * @return
     * @throws CacheException
     */
    public boolean createEntry(@NonNull final CacheObject cacheObject, @NonNull final ObjectStoreData objectStoreData, @NonNull final CacheObjectProperties cacheObjectProperties) throws CacheException {
        // check CacheObject
        if (cacheObject.isInvalid()) {
            throw new CacheException("CacheObject is not valid: " + cacheObject);
        }

        // check ObjectStoreData
        if (objectStoreData.isInvalid()) {
            throw new CacheException("ObjectStoreData is not valid: " + objectStoreData);
        }

        final String groupId = cacheObject.getGroupId();
        final String keyId = cacheObject.getKeyId();
        final String fileId = cacheObject.getFileId();
        final Properties customProperties = cacheObjectProperties.getCustomProperties();
        final Set<Object> customKeys = customProperties.keySet();
        int index = 0;

        final String insertContentSql = STR_BUILDER("INSERT INTO ").append(TBL_CONTENT(groupId, null)).
            append(" (FileStoreNumber,FileStoreId,SubGroupId,FileId) VALUES (?,?,?,?)").
            append(" ON DUPLICATE KEY UPDATE").
            append(" FileStoreNumber=?, FileStoreId=?, SubGroupId=?, FileId=?").toString();
        final StringBuilder insertPropertiesBuilderStart = STR_BUILDER("INSERT INTO ").append(TBL_PROPERTIES(groupId, null)).
            append(" (FileStoreNumber,FileStoreId,CreateDate,ModificationDate,Length");
        final StringBuilder insertPropertiesBuilderEnd = STR_BUILDER(" VALUES (?,?,?,?,?");
        final StringBuilder updatePropertiesBuilder = STR_BUILDER(" ON DUPLICATE KEY UPDATE ").
            append(" FileStoreNumber=?,FileStoreId=?,CreateDate=?,ModificationDate=?,Length=?");

        if (customKeys.size() > 0) {
            for (final Object curCustomKey : customKeys) {
                insertPropertiesBuilderStart.append(',').append(curCustomKey);
                insertPropertiesBuilderEnd.append(",?");

                updatePropertiesBuilder.append(',').append(curCustomKey).append("=?");
            }
        }

        final String insertOrUpdatePropertiesSql = insertPropertiesBuilderStart.append(") ").append(insertPropertiesBuilderEnd).append(')').
            append(updatePropertiesBuilder).toString();

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= 1; sleepTimeoutMillis <<= 1) {
            Connection usedCon = null;

            // Insert new data into Content table if
            // fileElement and ObjectStoreData are valid
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper();
                 final PreparedStatement fcStmt = conWrapper.prepareStatement(insertContentSql);
                 final PreparedStatement fpStmt = conWrapper.prepareStatement(insertOrUpdatePropertiesSql)) {

                final int ojectStoreNumber = objectStoreData.getObjectStoreNumber();
                final String fileStoreId = objectStoreData.getObjectStoreId();
                final long createDateMillis = cacheObjectProperties.getCreateDateMillis();
                final long modificationDateMillis = cacheObjectProperties.getModificationDateMillis();
                final long length = cacheObjectProperties.getLength();

                usedCon = conWrapper.getConnection();

                // create entry in Content table
                fcStmt.setInt(index = 1, ojectStoreNumber);
                fcStmt.setString(++index, fileStoreId);
                fcStmt.setString(++index, keyId);
                fcStmt.setString(++index, fileId);

                fcStmt.setInt(++index, ojectStoreNumber);
                fcStmt.setString(++index, fileStoreId);
                fcStmt.setString(++index, keyId);
                fcStmt.setString(++index, fileId);

                // create properties
                fpStmt.setInt(index = 1, ojectStoreNumber);
                fpStmt.setString(++index, fileStoreId);
                fpStmt.setLong(++index, createDateMillis);
                fpStmt.setLong(++index, modificationDateMillis);
                fpStmt.setLong(++index, length);

                // set given insert key custom values
                if (customKeys.size() > 0) {
                    for (final Object curCustomKey : customKeys) {
                        fpStmt.setString(++index, cacheObjectProperties.getCustomKeyValue(curCustomKey.toString()));
                    }
                }

                fpStmt.setInt(++index, ojectStoreNumber);
                fpStmt.setString(++index, fileStoreId);
                fpStmt.setLong(++index, createDateMillis);
                fpStmt.setLong(++index, modificationDateMillis);
                fpStmt.setLong(++index, length);

                // set given update custom values
                if (customKeys.size() > 0) {
                    for (final Object curCustomKey : customKeys) {
                        fpStmt.setString(++index, cacheObjectProperties.getCustomKeyValue(curCustomKey.toString()));
                    }
                }

                // execute both statements and commit
                fcStmt.executeUpdate();
                fpStmt.executeUpdate();

                usedCon.commit();

                // if inserting of new entries into both
                // tables went fine, set objectStoreData
                // at fileElement and set return value to true
                cacheObject.setObjectStoreData(objectStoreData);

                return true;
            } catch (Exception e) {
                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, "Error while creating entry");
            }
        }

        return false;
    }

    /**
     * @param groupId
     * @param keyId
     * @return
     * @throws CacheException
     */
    public CacheObject[] getFileItems(@NonNull final String groupId, @NonNull final String keyId) throws CacheException {
        final String querySql = STR_BUILDER("SELECT fc.FileStoreNumber, fc.FileStoreId, fc.SubGroupId, fc.FileId").append(" FROM ").append(TBL_CONTENT(groupId, "fc")).append(" WHERE fc.SubGroupId=?").toString();

        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement queryStmt = conWrapper.prepareStatement(querySql)) {

            queryStmt.setString(1, keyId);

            return implExecuteQuery(conWrapper.getConnection(), groupId, queryStmt);
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    /**
     * @param groupId
     * @param properties
     * @return
     * @throws CacheException
     */
    public CacheObject[] getFileItems(@NonNull final String groupId, @NonNull final Properties properties) throws CacheException {
        final Properties customProperties = new Properties();

        for (final Object curKey : properties.keySet()) {
            customProperties.put("fp." + curKey, properties.getProperty(curKey.toString()));
        }

        final String querySql = CacheUtils.createSqlWithPropertyVariables(STR_BUILDER("SELECT fc.FileStoreNumber, fc.FileStoreId, fc.SubGroupId, fc.FileId FROM ").append(TBL_CONTENT(groupId, "fc")).append(" INNER JOIN ").append(TBL_PROPERTIES(groupId, "fp")).append(" ON fc.FileStoreNumber=fp.FileStoreNumber AND fc.FileStoreId=fp.FileStoreId ").append(" WHERE ").toString(), customProperties, " AND ");

        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement queryStmt = conWrapper.prepareStatement(querySql)) {

            CacheUtils.setStatementValues(queryStmt, customProperties.values().toArray(), 1);

            return implExecuteQuery(conWrapper.getConnection(), groupId, queryStmt);
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    /**
     * @return
     */
    public long getGroupCount() {
        return m_groupMap.size();
    }

    /**
     * @param groupId
     * @return
     * @throws CacheException
     */
    public long getKeyCount(@NonNull final String groupId) throws CacheException {
        final GroupConfig groupConfig = m_groupMap.get(groupId);

        return null != groupConfig ? implExecuteGetKeyCount(groupId, groupConfig.isUseCountTable()) : 0;
    }

    /**
     * @param groupId
     * @return
     */
    public boolean contains(@NonNull final String groupId) {
        return m_groupMap.containsKey(groupId);
    }

    /**
     * @param groupId
     * @param keyId
     * @return
     * @throws CacheException
     */
    public boolean contains(@NonNull final String groupId, @NonNull final String keyId) throws CacheException {
        return implExecuteContains(groupId, STR_BUILDER("fc.SubGroupId='").append(keyId).append("'").toString());
    }

    /**
     * @param groupId
     * @param keyId
     * @param fileId
     * @return
     * @throws CacheException
     */
    public boolean contains(@NonNull final String groupId, @NonNull final String keyId, @NonNull final String fileId) throws CacheException {
        return implExecuteContains(groupId, STR_BUILDER("fc.SubGroupId='").append(keyId).append("'").append(" AND fc.FileId='").append(fileId).append("'").toString());
    }

    /**
     * @param groupId
     * @return
     * @throws CacheException
     */
    public String[] getKeyIds(@NonNull final String groupId) throws CacheException {
        try {
            return implExecuteGetIds(STR_BUILDER("SELECT DISTINCT fc.SubGroupId FROM ").append(TBL_CONTENT(groupId, "fc")).toString());
        } catch (Exception e) {
            throw new CacheException("Exception caught when getting SubGroupIds", e);
        }
    }

    /**
     * @param groupId
     * @param whereClause
     * @param limitClause
     * @param keyConsumer
     * @return
     * @throws CacheException
     */
    public long iterateKeysByDescendingAge(@NonNull final String groupId, @Nullable final String whereClause, @Nullable final String limitClause, @Nullable final Consumer<KeyObject> keyConsumer) throws CacheException {

        final StringBuilder querySql = STR_BUILDER("SELECT fc.SubGroupId,").append(" SUM(fp.Length) AS SubGroupLength,").append(" MIN(fp.CreateDate) AS SubGroupCreateDate,").append(" MAX(fp.ModificationDate) AS SubGroupModificationDate").append(" FROM ").append(TBL_CONTENT(groupId, "fc")).append(" INNER JOIN ").append(TBL_PROPERTIES(groupId, "fp")).append(" ON fc.FileStoreNumber=fp.FileStoreNumber AND fc.FileStoreId=fp.FileStoreId ");

        if (isNotBlank(whereClause)) {
            querySql.append(" WHERE ").append(whereClause);
        }

        // descending age means ascending order by timestamp in query
        querySql.append(" GROUP BY fc.SubGroupId ORDER BY SubGroupModificationDate ASC, SubGroupLength DESC");

        if (isNotBlank(limitClause)) {
            querySql.append(" LIMIT ").append(limitClause);
        }

        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(querySql.toString())) {

            long ret = 0;

            try (final ResultSet resultSet = stmt.executeQuery()) {
                if (null != resultSet) {
                    while (resultSet.next()) {
                        final String keyId = resultSet.getString(1);
                        final long keyLength = resultSet.getLong(2);
                        final long keyCreateDateMillis = resultSet.getLong(3);
                        final long keyModificationDateMillis = resultSet.getLong(4);

                        ++ret;

                        if (null != keyConsumer) {
                            keyConsumer.accept(new KeyObject(keyId, new Date(keyCreateDateMillis), new Date(keyModificationDateMillis), keyLength));
                        }
                    }
                }
            } finally {
                // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                conWrapper.commit();
            }

            return ret;
        } catch (Exception e) {
            throw new CacheException("Exception caught when getting KeyIds by WHERE and/or LIMIT clause", e);
        }
    }

    /**
     * @param groupId
     * @return
     * @throws CacheException
     */
    public long getGroupLength(@NonNull final String groupId) throws CacheException {
        final GroupConfig groupConfig = m_groupMap.get(groupId);

        return null != groupConfig ? implExecuteGetGroupLength(groupId, groupConfig.isUseCountTable()) : 0;
    }

    /**
     * @param groupId
     * @param keyId
     * @param properties
     * @return
     * @throws CacheException
     */
    public long getGroupLength(@NonNull final String groupId, @Nullable final String keyId, @Nullable final Properties properties) throws CacheException {
        final Properties whereProperties = new Properties();
        StringBuilder selectSumSql = STR_BUILDER("SELECT SUM(fp.Length) from ").append(TBL_PROPERTIES(groupId, "fp"));
        Object[] whereValues = null;

        if (CacheUtils.isValid(keyId)) {
            whereProperties.put("fc.SubGroupId", keyId);
        }

        if (null != properties) {
            for (final Object curKey : properties.keySet()) {
                whereProperties.put("fp." + curKey, properties.getProperty(curKey.toString()));
            }
        }

        if (whereProperties.size() > 0) {
            selectSumSql = STR_BUILDER(CacheUtils.createSqlWithPropertyVariables(selectSumSql.append(" INNER JOIN ").append(TBL_CONTENT(groupId, "fc")).append(" ON fc.FileStoreNumber=fp.FileStoreNumber AND fc.FileStoreId=fp.FileStoreId WHERE").toString(), whereProperties, " AND "));

            whereValues = whereProperties.values().toArray(new Object[0]);
        }

        try {
            return implExecuteGetLong(selectSumSql.toString(), whereValues);
        } catch (CacheException e) {
            throw new CacheException("Exception caught when getting summed up length of FileItems", e);
        }
    }

    /**
     * @return
     */
    public String[] getGroupIds() {
        return m_groupMap.keySet().toArray(new String[0]);
    }

    /**
     * @param cacheObject
     * @return
     * @throws CacheException
     */
    public ObjectStoreData getObjectStoreData(@NonNull final CacheObject cacheObject) throws CacheException {
        final String selectSql = STR_BUILDER("SELECT fc.FileStoreNumber, fc.FileStoreId FROM ").append(TBL_CONTENT(cacheObject.getGroupId(), "fc")).append(" WHERE fc.SubGroupId=? AND fc.FileId=?").toString();

        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(selectSql)) {

            stmt.setString(1, cacheObject.getKeyId());
            stmt.setString(2, cacheObject.getFileId());

            try (final ResultSet resultSet = stmt.executeQuery()) {
                if (null != resultSet && resultSet.first()) {
                    return new ObjectStoreData(resultSet.getInt(1), resultSet.getString(2));
                }
            } finally {
                // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                conWrapper.commit();
            }
        } catch (Exception e) {
            throw new CacheException(e);
        }

        return null;
    }

    /**
     * @param cacheObject
     * @return
     * @throws CacheException
     */
    public CacheObjectProperties getFileItemProperties(@NonNull CacheObject cacheObject) throws CacheException {
        final String groupId = cacheObject.getGroupId();
        final Set<String> customKeys = getCustomPropertyKeys(groupId);
        final StringBuilder selectSqlBuilderStart = STR_BUILDER("SELECT fp.FileStoreNumber,fp.FileStoreId,fp.CreateDate,fp.ModificationDate,fp.Length");
        final String selectSqlEnd = STR_BUILDER(" FROM ").append(TBL_CONTENT(groupId, "fc")).append(" INNER JOIN ").append(TBL_PROPERTIES(groupId, " fp ")).append(" ON fc.FileStoreNumber=fp.FileStoreNumber AND fc.FileStoreId=fp.FileStoreId").append(" WHERE fc.SubGroupId=? AND fc.FileId=?").toString();

        // append all property keys for given groupId to Sql statement
        for (final String curCustomKey : customKeys) {
            selectSqlBuilderStart.append(",fp.").append(curCustomKey);
        }

        final String selectSql = selectSqlBuilderStart.append(selectSqlEnd).toString();

        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(selectSql)) {

            stmt.setString(1, cacheObject.getKeyId());
            stmt.setString(2, cacheObject.getFileId());

            try (final ResultSet resultSet = stmt.executeQuery()) {

                if (null != resultSet && resultSet.first()) {
                    int index = 0;

                    final int objectStoreNumber = resultSet.getInt(++index);
                    final String objectStoreId = resultSet.getString(++index);

                    cacheObject.setObjectStoreData(new ObjectStoreData(objectStoreNumber, objectStoreId));

                    final CacheObjectProperties ret = new CacheObjectProperties(customKeys);

                    ret.setCreateDateMillis(resultSet.getLong(++index));
                    ret.setModificationDateMillis(resultSet.getLong(++index));
                    ret.setLength(resultSet.getLong(++index));

                    // read all key property values for the
                    // given groupId and set at result CacheObjectProperties
                    for (final String curCustomKey : customKeys) {
                        ret.setCustomKeyValue(curCustomKey, resultSet.getString(++index));
                    }

                    return ret;
                }
            } finally {
                // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                conWrapper.commit();
            }
        } catch (Exception e) {
            throw new CacheException(e);
        }

        return null;
    }

    /**
     * @param objectStoreData
     * @param groupId
     * @param properties
     * @throws CacheException
     */
    public void updateProperties(@NonNull final ObjectStoreData objectStoreData, @NonNull final String groupId, @NonNull final Properties properties) throws CacheException {
        if (properties.size() > 0) {
            final Properties setProperties = new Properties();

            for (final Object curKey : properties.keySet()) {
                setProperties.put("fp." + curKey, properties.get(curKey));
            }

            final String sqlStmt = CacheUtils.createSqlWithPropertyVariables(STR_BUILDER("UPDATE ").append(TBL_PROPERTIES(groupId, "fp")).append(" SET").toString(), setProperties, ",", " WHERE fp.FileStoreNumber=? AND fp.FileStoreId=?");

            for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
                Connection usedCon = null;

                try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(sqlStmt)) {

                    usedCon = conWrapper.getConnection();

                    int curIndex = CacheUtils.setStatementValues(stmt, setProperties.values().toArray(), 1);

                    stmt.setInt(curIndex++, objectStoreData.getObjectStoreNumber());
                    stmt.setString(curIndex++, objectStoreData.getObjectStoreId());

                    stmt.executeUpdate();

                    usedCon.commit();
                    break;
                } catch (Exception e) {
                    implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, null);
                }
            }
        }
    }

    /**
     * @param groupId
     * @param keyId
     * @param fileId
     * @param deletedObjectStoreDataList
     * @return
     * @throws CacheException
     */
    public int deleteEntry(@NonNull final String groupId, @NonNull final String keyId, @NonNull final String fileId, @NonNull final List<ObjectStoreData> deletedObjectStoreDataList) throws CacheException {
        final String whereStr = STR_BUILDER("fc.SubGroupId=").append("'").append(keyId).append("'").append(" AND").append(" fc.FileId=").append("'").append(fileId).append("'").toString();

        return implExecuteDelete(groupId, whereStr, deletedObjectStoreDataList);
    }

    /**
     * @param groupId
     * @param keyId
     * @param deletedObjectStoreDataList
     * @return
     * @throws CacheException
     */
    public int deleteByKey(@NonNull final String groupId, @Nullable final String keyId, @NonNull final List<ObjectStoreData> deletedObjectStoreDataList) throws CacheException {
        final String whereStr = null != keyId ? STR_BUILDER("fc.SubGroupId=").append("'").append(keyId).append("'").toString() : null;

        return implExecuteDelete(groupId, whereStr, deletedObjectStoreDataList);
    }

    /**
     * @param groupId
     * @param keyIds
     * @param deletedObjectStoreDataList
     * @return
     * @throws CacheException
     */
    public int deleteByGroupKeys(@NonNull final String groupId, @NonNull final String[] keyIds, @NonNull final List<ObjectStoreData> deletedObjectStoreDataList) throws CacheException {
        final int count = keyIds.length;
        int ret = 0;

        if (count > 0) {
            final int blockCount = 1024;
            int curPos = 0;

            // remove keys in blocks with a maximum of blockCount keys
            do {
                final StringBuilder whereStrBuilder = STR_BUILDER("fc.SubGroupId IN (");

                for (int i = 0, loopCount = Math.min(blockCount, count - curPos); i < loopCount; ++i, ++curPos) {
                    if (i > 0) {
                        whereStrBuilder.append(',');
                    }

                    whereStrBuilder.append("'").append(keyIds[curPos]).append("'");
                }

                whereStrBuilder.append(')');

                ret += implExecuteDelete(groupId, whereStrBuilder.toString(), deletedObjectStoreDataList);
            } while (curPos < count);
        }

        return ret;
    }

    /**
     * @param groupId
     * @param properties
     * @param deletedObjectStoreDataList
     * @return
     * @throws CacheException
     */
    public int deleteByProperties(@NonNull final String groupId, @NonNull final Properties properties, @NonNull final List<ObjectStoreData> deletedObjectStoreDataList) throws CacheException {
        final StringBuilder whereStrBuilder = STR_BUILDER(null);

        for (final Object curKey : properties.keySet()) {
            if (whereStrBuilder.length() > 0) {
                whereStrBuilder.append(" AND ");
            }

            whereStrBuilder.append("fp.").append(curKey).append("=").append("'").append(properties.get(curKey)).append("'");
        }

        return implExecuteDelete(groupId, whereStrBuilder.toString(), deletedObjectStoreDataList);
    }

    /**
     * @param groupId
     * @param objectStoreData
     * @param deletedObjectStoreDataList
     * @return
     * @throws CacheException
     */
    public int deleteByObjectStoreData(@NonNull final String groupId, @NonNull final ObjectStoreData objectStoreData, @NonNull final List<ObjectStoreData> deletedObjectStoreDataList) throws CacheException {
        final String whereStr = STR_BUILDER("fc.FileStoreNumber=").append("'").append(objectStoreData.getObjectStoreNumber()).append("'").append(" AND").append(" fc.FileStoreId=").append("'").append(objectStoreData.getObjectStoreId()).append("'").toString();

        return implExecuteDelete(groupId, whereStr, deletedObjectStoreDataList);
    }

    //  - Public interface to retrieve connections -------------------------

    /**
     * @return
     * @throws CacheException
     */
    public ConnectionWrapper getReadConnectionWrapper() throws CacheException {
        if (null == m_readDataSource) {
            throw new CacheException("No read connection pool available. Please check logging output and setup!");
        }

        try {
            final ConnectionWrapper ret = new ConnectionWrapper(m_readDataSource.getConnection());
            final Connection readCon = ret.getConnection();

            // Throw exception in case of invalid connection
            if (null == readCon) {
                // Thrown exception is caught in outer catch block and rethrown there
                throw new SQLException("Could not get valid read connection from connection pool");
            }

            return ret;
        } catch (SQLException e) {
            throw new CacheException("Exception caught when getting read connection", e);
        }
    }

    /**
     * @return
     * @throws CacheException
     */
    public ConnectionWrapper getWriteConnectionWrapper() throws CacheException {
        return implGetWriteConnectionWrapper(false);
    }

    /**
     * @return
     * @throws CacheException
     */
    public ConnectionWrapper getExclusiveWriteConnectionWrapper() throws CacheException {
        return implGetWriteConnectionWrapper(true);
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param con
     * @param exception
     * @param sleepTimeout
     * @param cause
     * @throws CacheException
     */
    private static void implHandleDatabaseException(@Nullable final Connection con, @NonNull final Exception exception, int sleepTimeout, @Nullable String cause) throws CacheException {
        if (null != con) {
            try {
                // perform transaction rollback first
                con.rollback();
            } catch (@SuppressWarnings("unused") SQLException e) {
                // not interested in
            }
        }

        // reschedule in case of a transaction deadlock
        if (exception instanceof SQLException && ((SQLException) exception).getErrorCode() == 1213) {
            CacheUtils.sleepThread(sleepTimeout);
        } else {
            throw StringUtils.isEmpty(cause) ? new CacheException(exception) : new CacheException(cause, exception);
        }
    }

    /**
     * @param exclusive
     * @return
     * @throws CacheException
     */
    private ConnectionWrapper implGetWriteConnectionWrapper(boolean exclusive) throws CacheException {
        if (null == m_writeDataSource) {
            throw new CacheException("No write connection pool available. Please check logging output and setup!");
        }

        // synchronize for exclusive connection gets and signal availability handling;
        // access to m_exclusiveWriteConnection needs to synchronized as well
        m_writeConnectionLock.lock();

        try {
            // if somebody is currently using an exclusive write connection =>
            // wait until this exclusive connection is closed,
            // m_exclusiveWriteConnectionreset is reset to null and/or we get signaled
            while (null != m_exclusiveWriteConnection) {
                try {
                    m_writeConnectionAvailableCondition.await();
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok
                }
            }

            final ConnectionWrapper ret = new ConnectionWrapper(m_writeDataSource.getConnection());
            final Connection writeCon = ret.getConnection();

            // Throw exception in case of invalid connection
            if (null == writeCon) {
                // Thrown exception is caught in outer catch block and rethrown there
                throw new SQLException("Could not get valid write connection from connection pool");
            }

            // set m_exclusiveWriteConnection to
            // just opened connection if requested
            if (exclusive) {
                m_exclusiveWriteConnection = writeCon;
            }

            // set handler interface so that we get notified when the
            // just retrieved connection is closed;
            // in case of an exclusive connection, all waiting threads
            // need to be signaled when the exclusive connection is closed
            ret.setConnectionClosedHandler((closedCon) -> {
                // handler is called from outside this method => synchronize!
                m_writeConnectionLock.lock();

                try {
                    // check if the closed connection is the one and only exclusive one
                    if (m_exclusiveWriteConnection == closedCon) {
                        // set m_exclusiveWriteConnection back to null
                        m_exclusiveWriteConnection = null;

                        // signal all waiting threads that no
                        // more exclusive connection is open
                        m_writeConnectionAvailableCondition.signalAll();
                    }
                } finally {
                    m_writeConnectionLock.unlock();
                }
            });

            return ret;
        } catch (SQLException e) {
            throw new CacheException("Exception caught when getting write connection", e);
        } finally {
            m_writeConnectionLock.unlock();
        }

    }

    /**
     * @param selectSql
     * @param stmtValues
     * @return
     * @throws Exception
     */
    private String[] implExecuteGetIds(@NonNull final String selectSql, final Object... stmtValues) throws Exception {
        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(selectSql)) {

            CacheUtils.setStatementValues(stmt, stmtValues, 1);

            try (final ResultSet resultSet = stmt.executeQuery()) {
                final List<String> idList = new ArrayList<>();

                if (null != resultSet) {
                    while (resultSet.next()) {
                        idList.add(resultSet.getString(1));
                    }
                }

                return idList.toArray(new String[0]);
            } finally {
                // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                conWrapper.commit();
            }
        }
    }

    /**
     * @param selectLongReturnSql
     * @param stmtValues
     * @return
     * @throws CacheException
     */
    private long implExecuteGetLong(@NonNull final String selectLongReturnSql, final Object... stmtValues) throws CacheException {
        try (final ConnectionWrapper conWrapper = getReadConnectionWrapper(); final PreparedStatement stmt = conWrapper.prepareStatement(selectLongReturnSql)) {

            CacheUtils.setStatementValues(stmt, stmtValues, 1);

            try (final ResultSet resultSet = stmt.executeQuery()) {
                if (null != resultSet && resultSet.first()) {
                    return resultSet.getLong(1);
                }
            } finally {
                // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                conWrapper.commit();
            }
        } catch (Exception e) {
            throw new CacheException("Error while getting long value from database", e);
        }

        return 0;
    }

    /**
     * @param groupId
     * @param whereStr
     * @return
     * @throws CacheException
     */
    private boolean implExecuteContains(@NonNull final String groupId, @NonNull final String whereStr) throws CacheException {
        if (m_groupMap.containsKey(groupId)) {
            try (final ConnectionWrapper conWrapper = getReadConnectionWrapper()) {
                final StringBuilder existsBuilder = STR_BUILDER("SELECT EXISTS (SELECT * FROM ").append(TBL_CONTENT(groupId, "fc")).append(" WHERE ").append(whereStr).append(" LIMIT 1)");

                try (final PreparedStatement existsStmt = conWrapper.prepareStatement(existsBuilder.toString()); final ResultSet resultSet = existsStmt.executeQuery()) {

                    return null != resultSet && resultSet.first() && resultSet.getBoolean(1);
                } finally {
                    // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
                    conWrapper.commit();
                }
            } catch (Exception e) {
                throw new CacheException("Error while deleting entry(ies))", e);
            }
        }

        return false;
    }

    /**
     * @param groupId
     * @param whereStr
     * @param deletedObjectStoreDataList
     * @return
     * @throws CacheException
     */
    private int implExecuteDelete(@NonNull final String groupId, @Nullable final String whereStr, @NonNull final List<ObjectStoreData> deletedObjectStoreDataList) throws CacheException {
        final StringBuilder selectStrBuilder = STR_BUILDER("SELECT fc.FileStoreNumber, fc.FileStoreId FROM ").append(TBL_CONTENT(groupId, "fc")).append(" INNER JOIN ").append(TBL_PROPERTIES(groupId, "fp")).append(" ON fc.FileStoreNumber=fp.FileStoreNumber AND fc.FileStoreId=fp.FileStoreId");

        final StringBuilder deleteStrBuilder = STR_BUILDER("DELETE fc, fp FROM ").append(TBL_CONTENT(groupId, "fc")).append(" INNER JOIN ").append(TBL_PROPERTIES(groupId, "fp")).append(" ON fc.FileStoreNumber=fp.FileStoreNumber AND fc.FileStoreId=fp.FileStoreId");

        if (isNotBlank(whereStr)) {
            selectStrBuilder.append(" WHERE ").append(whereStr);
            deleteStrBuilder.append(" WHERE ").append(whereStr);
        }

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            Connection usedCon = null;

            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final PreparedStatement selectStmt = conWrapper.prepareStatement(selectStrBuilder.toString()); final PreparedStatement deleteStmt = conWrapper.prepareStatement(deleteStrBuilder.toString())) {

                usedCon = conWrapper.getConnection();

                selectStmt.executeQuery();
                deleteStmt.executeUpdate();

                usedCon.commit();

                try (final ResultSet resultSet = selectStmt.getResultSet()) {
                    if (null != resultSet) {
                        while (resultSet.next()) {
                            deletedObjectStoreDataList.add(new ObjectStoreData(resultSet.getInt(1), resultSet.getString(2)));
                        }
                    }
                }

                return deletedObjectStoreDataList.size();
            } catch (Exception e) {
                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, "Error while deleting entry(ies))");
            }
        }

        return 0;
    }

    /**
     * @param con
     * @param groupId
     * @param queryStmt
     * @param returnValues
     * @return
     * @throws Exception
     */
    private static CacheObject[] implExecuteQuery(@NonNull final Connection con, @NonNull final String groupId, @NonNull final PreparedStatement queryStmt, Object... returnValues) throws Exception {
        try (final ResultSet resultSet = queryStmt.executeQuery()) {
            final ArrayList<CacheObject> fileItemList = new ArrayList<>();

            if (null != resultSet) {
                while (resultSet.next()) {
                    final int objectStoreNumber = resultSet.getInt(1);
                    final String objectStoreId = resultSet.getString(2);
                    final String keyId = resultSet.getString(3);
                    final String fileId = resultSet.getString(4);

                    final CacheObject cacheObject = new CacheObject(groupId, keyId, fileId);

                    cacheObject.setObjectStoreData(new ObjectStoreData(objectStoreNumber, objectStoreId));
                    fileItemList.add(cacheObject);

                    if (ArrayUtils.isNotEmpty(returnValues)) {
                        for (int i = 0, count = returnValues.length; i < count; ++i) {
                            final Object curReturnObj = returnValues[i];

                            if (curReturnObj instanceof Collection<?>) {
                                @SuppressWarnings("unchecked") final Collection<Object> curReturnCollection = (Collection<Object>) curReturnObj;

                                switch (i) {
                                    case 0:
                                        curReturnCollection.add(Integer.valueOf(objectStoreNumber));
                                        break;
                                    case 1:
                                        curReturnCollection.add(objectStoreId);
                                        break;
                                    case 2:
                                        curReturnCollection.add(groupId);
                                        break;
                                    case 3:
                                        curReturnCollection.add(keyId);
                                        break;
                                    case 4:
                                        curReturnCollection.add(fileId);
                                        break;

                                    default: {
                                        // GroupId is not part of the SQL query itself,
                                        // so that the index of the return paramater equals
                                        // the current return array position instead
                                        // of (position + 1)
                                        curReturnCollection.add(resultSet.getObject(i));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return CacheUtils.listToArray(fileItemList, CacheObject.class);
        } catch (Exception e) {
            throw new CacheException(e);
        } finally {
            // commit even pure query statements, since they could produce transactions (e.g. for AWS RDB)
            con.commit();
        }
    }

    /**
     * @param groupId
     * @param useCountTable
     * @return
     * @throws CacheException
     */
    private long implExecuteGetGroupLength(@NonNull final String groupId, final boolean useCountTable) throws CacheException {
        try {
            final StringBuilder selectGroupLengthSql = useCountTable ? STR_BUILDER("SELECT fa.TotalLength FROM ").append(TBL_GROUPADMIN("fa")).append(" WHERE fa.GroupId='").append(groupId).append("'") : STR_BUILDER("SELECT SUM(fp.Length) from ").append(TBL_PROPERTIES(groupId, "fp"));

            return implExecuteGetLong(selectGroupLengthSql.toString());
        } catch (CacheException e) {
            throw new CacheException("Exception caught when getting summed up length of FileItems for group: " + groupId, e);
        }
    }

    /**
     * @param groupId
     * @param useCountTable
     * @return
     * @throws CacheException
     */
    public long implExecuteGetKeyCount(@NonNull final String groupId, final boolean useCountTable) throws CacheException {
        try {
            final StringBuilder keyCountSql = useCountTable ? STR_BUILDER("SELECT fa.SubGroupCount FROM ").append(TBL_GROUPADMIN("fa")).append(" WHERE fa.GroupId='").append(groupId).append("'") : STR_BUILDER("SELECT COUNT(DISTINCT fc.SubGroupId) FROM ").append(TBL_CONTENT(groupId, "fc"));

            return implExecuteGetLong(keyCountSql.toString());
        } catch (CacheException e) {
            throw new CacheException("Exception caught when getting SubGroupId count for group: " + groupId, e);
        }
    }

    /**
     * @param groupId
     * @return
     */
    private boolean implSyncGroup(@NonNull final String groupId) {
        try {
            final String updateGroupCountsSql = STR_BUILDER("UPDATE ").append(TBL_GROUPADMIN("fa")).append(" SET fa.SubGroupCount=?, fa.TotalLength=?, fa.Sync=?").append(" WHERE fa.GroupId=?").toString();

            Connection usedCon = null;

            for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
                try (final ConnectionWrapper conWrapper = getExclusiveWriteConnectionWrapper(); final PreparedStatement fgGroupCountsUpdateStmt = conWrapper.prepareStatement(updateGroupCountsSql)) {

                    usedCon = conWrapper.getConnection();

                    final long keyCount = implExecuteGetKeyCount(groupId, false);
                    final long groupLength = implExecuteGetGroupLength(groupId, false);

                    // update 'SubGroupCount', 'TotalLength' and 'Sync' rows;
                    // Sync value  is set to 'false' if sync was successful
                    fgGroupCountsUpdateStmt.setLong(1, keyCount);
                    fgGroupCountsUpdateStmt.setLong(2, groupLength);
                    fgGroupCountsUpdateStmt.setBoolean(3, false);
                    fgGroupCountsUpdateStmt.setString(4, groupId);

                    // execute all statements and commit
                    fgGroupCountsUpdateStmt.executeUpdate();

                    usedCon.commit();

                    return true;
                } catch (Exception e) {
                    final String cause =
                        "Exception occured when trying to update/sync SubGroupCount and TotalLength columns for GroupId: " + groupId + " (" +
                        Throwables.getRootCause(e).getMessage() + ')';

                    implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, cause);
                }
            }
        } catch (@SuppressWarnings("unused") CacheException e) {
            LOG.info("CS Database could not update count tables for group {}. Using database server SQL counting.", groupId);
        }

        return false;
    }

    /**
     * @param groupConfig
     * @return
     */
    private boolean implUpdateGroupConfig(@NonNull final GroupConfig groupConfig) {
        final String groupId = groupConfig.getGroupId();

        try {
            final String updateGroupConfigSql = STR_BUILDER("UPDATE ").append(TBL_GROUPADMIN("fa")).append(" SET fa.Config=?").append(" WHERE fa.GroupId=?").toString();

            Connection usedCon = null;

            for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
                try (final ConnectionWrapper conWrapper = getExclusiveWriteConnectionWrapper(); final PreparedStatement fgGroupConfigUpdateStmt = conWrapper.prepareStatement(updateGroupConfigSql)) {

                    usedCon = conWrapper.getConnection();

                    // update 'Config' column for ;
                    // Sync value  is set to 'false' if sync was successful
                    fgGroupConfigUpdateStmt.setString(1, groupConfig.toJSON().toString());
                    fgGroupConfigUpdateStmt.setString(2, groupId);

                    // execute all statements and commit
                    fgGroupConfigUpdateStmt.executeUpdate();

                    usedCon.commit();

                    return true;
                } catch (Exception e) {
                    final String cause =
                        "Exception occured when trying to update config for GroupId: " + groupId + " (" + Throwables.getRootCause(e).getMessage() +
                        ')';

                    implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, cause);
                }
            }
        } catch (@SuppressWarnings("unused") CacheException e) {
            LOG.info("CS Database could not update config for group {}. Using database server SQL counting.", groupId);
        }

        return false;
    }

    /**
     * @param groupId
     * @return
     */
    private boolean implCreateTriggers(@NonNull final String groupId) {
        try {
            implCreateInsertTrigger(groupId);
            implCreateDeleteTrigger(groupId);
            implCreateUpdateTrigger(groupId);

            // Log creation oif triggers only once
            if (!m_groupMap.containsKey(groupId)) {
                LOG.info("CS Successfully created triggers to use count table for group: {}", groupId);
            }

            return true;
        } catch (@SuppressWarnings("unused") Exception e) {
            LOG.info("CS Could not create triggers to use count table for group {}, falling back to triggerless operation, please consider upgrading MySQL database server to support triggers and ensure that TRIGGER privilege is granted for current database user", groupId);
        }

        return false;
    }

    /**
     * @param groupId
     * @throws CacheException
     */
    private void implCreateInsertTrigger(@NonNull final String groupId) throws CacheException {
        final String contentTriggerName = STR_BUILDER("insert_").append(TBL_CONTENT(groupId, null)).append("_before").toString();
        final String dropInsertContentTriggerSql = STR_BUILDER("DROP TRIGGER IF EXISTS ").append(contentTriggerName).toString();
        final String createInsertContentTriggerSql = STR_BUILDER("CREATE TRIGGER ").append(contentTriggerName).append(" BEFORE INSERT ON ").
            append(TBL_CONTENT(groupId, null)).
            append(" FOR EACH ROW BEGIN").
            append("  UPDATE GroupAdmin fa SET fa.SubGroupCount=fa.SubGroupCount+1 WHERE").append(" fa.GroupId='").append(groupId).append("'").
            append("   AND NOT EXISTS (SELECT 1 FROM ").append(TBL_CONTENT(groupId, "fc")).append(" WHERE fc.SubGroupId=NEW.SubGroupId LIMIT 1);").
            append(" END").toString();

        final String propertiesTriggerName = STR_BUILDER("insert_").append(TBL_PROPERTIES(groupId, null)).append("_after").toString();
        final String dropInsertPropertiesTriggerSql = STR_BUILDER("DROP TRIGGER IF EXISTS ").append(propertiesTriggerName).toString();
        final String createInsertPropertiesTriggerSql = STR_BUILDER("CREATE TRIGGER ").append(propertiesTriggerName).append(" AFTER INSERT ON ").
            append(TBL_PROPERTIES(groupId, null)).
            append(" FOR EACH ROW BEGIN").
            append("   UPDATE GroupAdmin fa SET fa.TotalLength=fa.TotalLength+NEW.Length WHERE fa.GroupId='").append(groupId).append("';").
            append(" END").toString();

        Connection usedCon = null;

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper();
                 final Statement insertStmt = conWrapper.createStatement()) {

                usedCon = conWrapper.getConnection();

                insertStmt.executeUpdate(dropInsertContentTriggerSql);
                insertStmt.executeUpdate(createInsertContentTriggerSql);

                insertStmt.executeUpdate(dropInsertPropertiesTriggerSql);
                insertStmt.executeUpdate(createInsertPropertiesTriggerSql);

                conWrapper.commit();
                break;
            } catch (Exception e) {
                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, "Error while creating insert trigger");
            }
        }
    }

    /**
     * @param groupId
     * @throws CacheException
     */
    private void implCreateDeleteTrigger(@NonNull final String groupId) throws CacheException {
        final String contentTriggerName = STR_BUILDER("delete_").append(TBL_CONTENT(groupId, null)).append("_after").toString();
        final String dropDeleteContentTriggerSql = STR_BUILDER("DROP TRIGGER IF EXISTS ").append(contentTriggerName).toString();
        final String createDeleteContentTriggerSql = STR_BUILDER("CREATE TRIGGER ").append(contentTriggerName).append(" AFTER DELETE ON ").append(TBL_CONTENT(groupId, null)).
           append(" FOR EACH ROW BEGIN").
           append("  UPDATE GroupAdmin fa SET fa.SubGroupCount=fa.SubGroupCount-1 WHERE fa.GroupId='").append(groupId).append("'").
           append("   AND NOT EXISTS (SELECT 1 FROM ").append(TBL_CONTENT(groupId, "fc")).append(" WHERE fc.SubGroupId=OLD.SubGroupId LIMIT 1);").
           append(" END").toString();

        final String propertiesTriggerName = STR_BUILDER("delete_").append(TBL_PROPERTIES(groupId, null)).append("_after").toString();
        final String dropDeletePropertiesTriggerSql = STR_BUILDER("DROP TRIGGER IF EXISTS ").append(propertiesTriggerName).toString();
        final String createDeletePropertiesTriggerSql = STR_BUILDER("CREATE TRIGGER ").append(propertiesTriggerName).append(" AFTER DELETE ON ").append(TBL_PROPERTIES(groupId, null)).
           append(" FOR EACH ROW BEGIN").
           append("   UPDATE GroupAdmin fa SET fa.TotalLength=fa.TotalLength-OLD.Length WHERE fa.GroupId='").append(groupId).append("';").
           append(" END").toString();

        Connection usedCon = null;

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final Statement deleteStmt = conWrapper.createStatement()) {

                usedCon = conWrapper.getConnection();

                deleteStmt.executeUpdate(dropDeleteContentTriggerSql);
                deleteStmt.executeUpdate(createDeleteContentTriggerSql);

                deleteStmt.executeUpdate(dropDeletePropertiesTriggerSql);
                deleteStmt.executeUpdate(createDeletePropertiesTriggerSql);

                conWrapper.commit();
                break;
            } catch (Exception e) {
                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, "Error while creating delete trigger");
            }
        }
    }

    /**
     * @param groupId
     * @throws CacheException
     */
    private void implCreateUpdateTrigger(@NonNull final String groupId) throws CacheException {
        final String propertiesTriggerName = STR_BUILDER("update_").append(TBL_PROPERTIES(groupId, null)).append("_after").toString();
        final String dropUpdatePropertiesTriggerSql = STR_BUILDER("DROP TRIGGER IF EXISTS ").append(propertiesTriggerName).toString();
        final String createUpdatePropertiesTriggerSql = STR_BUILDER("CREATE TRIGGER ").append(propertiesTriggerName).append(" AFTER UPDATE ON ").append(TBL_PROPERTIES(groupId, null)).
           append(" FOR EACH ROW BEGIN").
           append("  UPDATE GroupAdmin fa SET fa.TotalLength=fa.TotalLength+(NEW.Length - OLD.Length)").
           append("   WHERE fa.GroupId='").append(groupId).append("';").
           append(" END").toString();

        Connection usedCon = null;

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final Statement updateStmt = conWrapper.createStatement()) {

                usedCon = conWrapper.getConnection();

                updateStmt.executeUpdate(dropUpdatePropertiesTriggerSql);
                updateStmt.executeUpdate(createUpdatePropertiesTriggerSql);

                updateStmt.executeUpdate(dropUpdatePropertiesTriggerSql);
                updateStmt.executeUpdate(createUpdatePropertiesTriggerSql);

                conWrapper.commit();
                break;
            } catch (Exception e) {
                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, "Error while creating update trigger");
            }
        }
    }

    /**
     * @param groupConfig
     * @return
     * @throws CacheException
     */
    private boolean implCreateGroupTables(@NonNull final GroupConfig groupConfig) throws CacheException {
        final String groupId = groupConfig.getGroupId();

        // create Content_GroupId table
        final String createContentTableSql = STR_BUILDER("CREATE TABLE ").append(TBL_CONTENT(groupId, null)).
                append(" (").
                append(" FileStoreNumber INT(10) NOT NULL,").
                append(" FileStoreId ").append(DB_VARCHAR_TYPE).append(" NOT NULL,").
                append(" SubGroupId ").append(DB_VARCHAR_TYPE).append(" NOT NULL,").
                append(" FileId ").append(DB_VARCHAR_TYPE).append(" NOT NULL").
                append(" )").toString();

        final String alterContentTableSql = STR_BUILDER("ALTER TABLE ").append(TBL_CONTENT(groupId, null)).append(" ADD PRIMARY KEY (SubGroupId, FileId)").toString();

        final String createSecondaryContentIndexSql = STR_BUILDER("CREATE INDEX ").append("idx_FileStore").append(" ON ").append(TBL_CONTENT(groupId, " ")).append(" (").append(" FileStoreNumber,").append(" FileStoreId (128)").append(" )").toString();

        // create Properties_GroupId table
        final String createPropertiesTableSql = STR_BUILDER("CREATE TABLE ").append(TBL_PROPERTIES(groupId, null)).append(" (").append(" FileStoreNumber INT(10) NOT NULL,").append(" FileStoreId ").append(DB_VARCHAR_TYPE).append(" NOT NULL").append(" )").toString();

        final String alterPropertiesTableSql = STR_BUILDER("ALTER TABLE ").append(TBL_PROPERTIES(groupId, null)).append(" ADD PRIMARY KEY (FileStoreNumber, FileStoreId)").toString();

        // insert new group into Group table
        final String insertGroupSql = STR_BUILDER("INSERT INTO ").append(TBL_GROUPADMIN(null)).
                append(" (GroupId,PropertyKeys,SubGroupCount,TotalLength,Sync,Config,Data) VALUES (?,?,?,?,?,?,?)").toString();

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            Connection usedCon = null;

            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper()) {
                usedCon = conWrapper.getConnection();

                try (final PreparedStatement createStmt = usedCon.prepareStatement(createContentTableSql);
                     final PreparedStatement alterStmt = usedCon.prepareStatement(alterContentTableSql); final PreparedStatement createSecondaryIndexStmt = usedCon.prepareStatement(createSecondaryContentIndexSql)) {

                    createStmt.executeUpdate();
                    alterStmt.executeUpdate();
                    createSecondaryIndexStmt.executeUpdate();
                } catch (@SuppressWarnings("unused") SQLException e) {
                    LOG.error(Throwables.getRootCause(e).getMessage());
                }

                try (final PreparedStatement createStmt = usedCon.prepareStatement(createPropertiesTableSql);
                     final PreparedStatement alterStmt = usedCon.prepareStatement(alterPropertiesTableSql)) {

                    createStmt.executeUpdate();
                    alterStmt.executeUpdate();
                } catch (@SuppressWarnings("unused") SQLException e) {
                    LOG.error(Throwables.getRootCause(e).getMessage());
                }

                try (final PreparedStatement insertStmt = usedCon.prepareStatement(insertGroupSql)) {
                    insertStmt.setString(1, groupId);
                    insertStmt.setString(2, "");
                    insertStmt.setLong(3, 0);
                    insertStmt.setLong(4, 0);
                    insertStmt.setBoolean(5, false);
                    insertStmt.setString(6, groupConfig.toJSON().toString());
                    insertStmt.setString(7, EMPTY_JSON_OBJECT_STR);

                    insertStmt.executeUpdate();
                } catch (@SuppressWarnings("unused") SQLException e) {
                    LOG.error(Throwables.getRootCause(e).getMessage());
                }

                usedCon.commit();
                return true;
            } catch (Exception e) {
                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, "Error while creating group tables");
            }
        }

        return false;
    }

    /**
     * @param groupId
     * @param registeredKeys
     * @param key
     * @param dbType
     * @param dbColumnConstraints
     * @throws CacheException
     */
    private void implInsertPropertyKey(@NonNull final String groupId, @NonNull final Set<String> registeredKeys, @NonNull final String key, @NonNull final String dbType, @Nullable final String dbColumnConstraints) throws CacheException {

        final StringBuilder propertyKeysBuilder = STR_BUILDER("");
        final String updateGroupPropertyKeysSql = STR_BUILDER("UPDATE ").append(TBL_GROUPADMIN("fa")).append(" SET fa.PropertyKeys=?").append(" WHERE fa.GroupId=?").toString();

        registeredKeys.add(key);

        for (final String curKey : registeredKeys) {
            if (isNotEmpty(curKey)) {
                if (propertyKeysBuilder.length() > 0) {
                    propertyKeysBuilder.append(',');
                }

                propertyKeysBuilder.append(curKey);
            }
        }

        Connection usedCon = null;

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final PreparedStatement fgPropertyKeysUpdateStmt = conWrapper.prepareStatement(updateGroupPropertyKeysSql)) {

                usedCon = conWrapper.getConnection();

                // delete/insert groupId in Groups table
                fgPropertyKeysUpdateStmt.setString(1, propertyKeysBuilder.toString());
                fgPropertyKeysUpdateStmt.setString(2, groupId);

                // execute all statements and commit
                fgPropertyKeysUpdateStmt.executeUpdate();

                usedCon.commit();
                break;
            } catch (Exception e) {
                final String cause = "Exception occured when trying to update PropertyKeys column for GroupId: " + groupId + " (" +
                                     Throwables.getRootCause(e).getMessage() + ')';

                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, cause);
            }
        }

        // Add new customKey column to Properties table
        final StringBuilder insertPropertiesColSql = STR_BUILDER("ALTER TABLE ").append(TBL_PROPERTIES(groupId, null)).append(" ADD COLUMN ").append(key).append(' ').append(dbType);

        if (null != dbColumnConstraints) {
            insertPropertiesColSql.append(' ').append(dbColumnConstraints);
        }

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final PreparedStatement fpInsertColStmt = conWrapper.prepareStatement(insertPropertiesColSql.toString())) {

                usedCon = conWrapper.getConnection();
                fpInsertColStmt.executeUpdate();

                usedCon.commit();
                break;
            } catch (Exception e) {
                final String cause =
                    "Exception occured when trying to add new column for GroupId: " + groupId + " (" + Throwables.getRootCause(e).getMessage() + ')';

                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, cause);
            }
        }

        // create index for new column
        final String indexName = "idx_" + key;
        final String createPropertiesColIndexSql = STR_BUILDER("CREATE INDEX ").append(indexName).append(" ON ").append(TBL_PROPERTIES(groupId, " ")).append(" (").append(key).append(dbType.equalsIgnoreCase("text") ? " (128)" : "").append(" )").toString();

        for (int sleepTimeoutMillis = 1; sleepTimeoutMillis <= MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS; sleepTimeoutMillis <<= 1) {
            try (final ConnectionWrapper conWrapper = getWriteConnectionWrapper(); final PreparedStatement fpCreateIndexStmt = conWrapper.prepareStatement(createPropertiesColIndexSql)) {

                usedCon = conWrapper.getConnection();

                fpCreateIndexStmt.executeUpdate();

                usedCon.commit();
                break;
            } catch (Exception e) {
                final String cause =
                    "Exception occured when trying to create new index for GroupId: " + groupId + " (" + Throwables.getRootCause(e).getMessage() +
                    ')';

                implHandleDatabaseException(usedCon, e, sleepTimeoutMillis, cause);
            }
        }
    }

    /**
     * @param groupId
     * @param customKeysOnly
     * @return
     */
    private @NonNull
    Set<String> implGetKeySet(@NonNull final String groupId, final boolean customKeysOnly) {
        final GroupConfig groupConfig = m_groupMap.get(groupId);

        if (null != groupConfig) {
            if (customKeysOnly) {
                return groupConfig.getCustomKeys();
            }

            final Set<String> allKeySet = new LinkedHashSet<>();

            allKeySet.addAll(groupConfig.getKeys());
            allKeySet.addAll(groupConfig.getCustomKeys());

            return allKeySet;
        }

        return new LinkedHashSet<>();
    }

    /**
     * @param tableAlias
     * @return
     */
    private static @NonNull
    StringBuilder TBL_GROUPADMIN(@Nullable String tableAlias) {
        final StringBuilder groupAdminTableNameBuilder = new StringBuilder(STR_BUILDER_SMALL_CAPACITY).append("GroupAdmin");

        return null != tableAlias ? groupAdminTableNameBuilder.append(' ').append(tableAlias) : groupAdminTableNameBuilder;
    }

    /**
     * @param groupId
     * @param tableAlias
     * @return
     */
    private static @NonNull
    StringBuilder TBL_CONTENT(@NonNull final String groupId, @Nullable String tableAlias) {
        final StringBuilder contentTableNameBuilder = new StringBuilder(STR_BUILDER_SMALL_CAPACITY).append("Content").append('_').append(groupId);

        return null != tableAlias ? contentTableNameBuilder.append(' ').append(tableAlias) : contentTableNameBuilder;
    }

    /**
     * @param groupId
     * @param tableAlias
     * @return
     */
    private static @NonNull
    StringBuilder TBL_PROPERTIES(@NonNull final String groupId, @Nullable String tableAlias) {
        final StringBuilder propertiesTableNameBuilder = new StringBuilder(STR_BUILDER_SMALL_CAPACITY).append("Properties").append('_').append(groupId);

        return null != tableAlias ? propertiesTableNameBuilder.append(' ').append(tableAlias) : propertiesTableNameBuilder;
    }

    /**
     * @param unprefixedKey
     * @return
     */
    private static @NonNull
    String CFG_KEY_1(final String unprefixedKey) {
        return STR_BUILDER(FILEITEM_CONFIG_PREFIX_1).append(unprefixedKey).toString();
    }

    /**
     * @param unprefixedKey
     * @return
     */
    private static @NonNull
    String CFG_KEY_2(final String unprefixedKey) {
        return STR_BUILDER(FILEITEM_CONFIG_PREFIX_2).append(unprefixedKey).toString();
    }

    /**
     * @param initStr
     * @return
     */
    private static @NonNull
    StringBuilder STR_BUILDER(@Nullable final String initStr) {
        final StringBuilder ret = new StringBuilder(STR_BUILDER_DEFAULT_CAPACITY);

        return null != initStr ? ret.append(initStr) : ret;
    }

    // - Static members --------------------------------------------------------

    final private static int MAX_TRANSACTION_REPEAT_TIMEOUT_MILLIS = 1024;

    final private static int STR_BUILDER_SMALL_CAPACITY = 64;
    final private static int STR_BUILDER_DEFAULT_CAPACITY = 128;
    final private static int STR_BUILDER_LARGE_CAPACITY = 256;

    final private static String FILEITEM_CONFIG_PREFIX_1 = "com.openexchange.fileItem.";
    final private static String FILEITEM_CONFIG_PREFIX_2 = "com.openexchange.fileitem.";

    // - Members ---------------------------------------------------------------

    final private ReentrantLock m_initializedLock = new ReentrantLock(true);

    final private Condition m_initializedCondition = m_initializedLock.newCondition();

    final private ReentrantLock m_writeConnectionLock = new ReentrantLock(true);

    final private Condition m_writeConnectionAvailableCondition = m_writeConnectionLock.newCondition();

    final private AtomicBoolean m_initializing = new AtomicBoolean(true);

    final private Map<String, GroupConfig> m_groupMap = Collections.synchronizedMap(new HashMap<>());

    final private DataSource m_writeDataSource;

    final private DataSource m_readDataSource;

    final private CacheConfig m_cacheConfig;

    final private ICache.DatabaseType m_databaseType = ICache.DatabaseType.MYSQL;

    private Connection m_exclusiveWriteConnection = null;
}
