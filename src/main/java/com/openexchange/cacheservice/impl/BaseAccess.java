/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import java.io.IOException;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.IBaseAccess;
import com.openexchange.cacheservice.api.IdLocker;

/**
 * {@link BaseAccess}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public abstract class BaseAccess implements IBaseAccess {

    /**
     * Initializes a new {@link BaseAccess}.
     */
    public BaseAccess(@NonNull Cache cache,
        @NonNull final CacheDatabase database,
        @NonNull CacheObject cacheObject) {

        super();

        m_cache = cache;
        m_database = database;
        m_cacheObjectKey = (m_cacheObject = cacheObject).getKey();
    }

    /**
     *
     */
    @Override
    public abstract void open() throws IOException;

    /**
     *
     */
    @Override
    public abstract void close() throws IOException;

    /**
     *
     */
    @Override
    public long getLength() {
        return (null != m_cacheObjectProperties) ? m_cacheObjectProperties.getLength() : 0;
    }

    /**
     *
     */
    @Override
    public Date getCreateDate() {
        return (null != m_cacheObjectProperties) ? new Date(m_cacheObjectProperties.getCreateDateMillis()) : null;
    }

    /**
     *
     */
    @Override
    public Date getModificationDate() {
        return (null != m_cacheObjectProperties) ? new Date(m_cacheObjectProperties.getModificationDateMillis()) : null;
    }

    /**
     *
     */
    @Override
    public String getKeyValue(String key) {
        return (null != m_cacheObjectProperties) ? m_cacheObjectProperties.getCustomKeyValue(key) : StringUtils.EMPTY;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    protected boolean tryOpen() {
        try {
            m_cacheObjectProperties = m_database.getFileItemProperties(m_cacheObject);
            return (null != m_cacheObjectProperties) && CacheUtils.isValid(m_cacheObject.getOjectStoreData());
        } catch (@SuppressWarnings("unused") CacheException e) {
            // OK, it's a try method!
        }

        return false;
    }

    // - Members ---------------------------------------------------------------

    final protected Cache m_cache;

    final protected CacheDatabase m_database;

    protected IdLocker m_cacheObjectLocker = null;

    final protected CacheObject m_cacheObject;

    protected String m_cacheObjectKey = null;

    protected CacheObjectProperties m_cacheObjectProperties = null;
}
