package com.openexchange.cacheservice.impl.micrometer;

import java.util.function.ToDoubleFunction;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.lang.Nullable;

/**
 * Provides common utility methods for Micrometer metrics.
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class Micrometer {

    /**
     * Registers and returns a Gauge with given arguments at the given registry. If a Gauge with equal name and tags already exists,
     * it is removed from the registry first. Effectively an existing Gauge gets overridden within the registry. This is desired in
     * cases, where the observed instance {@code obj} gets replaced during application life time.
     *
     * @param registry The registry
     * @param name The name
     * @param tags The tags
     * @param description The description
     * @param baseUnit The base unit
     * @param obj The source object
     * @param f The value conversion function
     * @return The newly registered Gauge
     */
    public static <T> Gauge registerOrUpdateGauge(MeterRegistry registry, String name, Tags tags, @Nullable String description, @Nullable String baseUnit, @Nullable T obj, ToDoubleFunction<T> f) {
        Gauge oldGauge = registry.find(name).tags(tags).gauge();
        if (oldGauge != null) {
            registry.remove(oldGauge);
        }
        return Gauge.builder(name, obj, f)
            .description(description)
            .tags(tags)
            .baseUnit(baseUnit)
            .register(registry);
    }

}
