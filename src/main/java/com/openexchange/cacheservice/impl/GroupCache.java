/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.GroupConfig;
import com.openexchange.cacheservice.api.ICache;

/**
 * {@link GroupCache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class GroupCache extends Thread {

    final private static int MAX_FILE_COUNT = 1000;

    /**
     * Initializes a new {@link GroupCache}.
     *
     * @param cache
     * @param config
     * @throws CacheException
     */
    public GroupCache(@NonNull final GroupConfig config, @NonNull final Cache cache) throws CacheException {
        this(config, cache, null);
    }

    /**
     * Initializes a new {@link GroupCache}.
     */
    public GroupCache(@NonNull final GroupConfig config, @NonNull final Cache cache, @Nullable final String displayName) throws CacheException {
        super((null != displayName) ? displayName : ("CacheGroup-" + config.getGroupId()));

        m_groupConfig = config;
        m_cache = cache;

        if (!config.isValid()) {
            throw new CacheException("Provided GroupConfig instance is not valid");
        }

        m_groupId = config.getGroupId();
        m_maxGroupSize = config.getMaxGroupSize();
        m_maxKeyCount = config.getMaxKeyCount();
        m_keyTimeoutMillis = config.getKeyTimeoutMillis();
        m_cleanupPeriodMillis = config.getCleanupPeriodMillis();

        start();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        LOG.trace("CS GroupCache thread worker started");

        // time to wait for first cleanup, afterwards the configured cleanup period is used
        long cleanupWaitTimeMillis = Math.min(m_cleanupPeriodMillis, 15000);

        while (isRunning()) {
            try {
                sleep(cleanupWaitTimeMillis);
                implPerformCleanup();
                cleanupWaitTimeMillis = m_cleanupPeriodMillis;
            } catch (@SuppressWarnings("unused") InterruptedException e) {
                LOG.trace("CS GroupCache thread worker received interrupt: " + m_groupId);
            }
        }

        LOG.trace("CS GroupCache thread worker finished: " + m_groupId);
    }

    /**
     * @return
     */
    public GroupConfig getConfig() {
        return m_groupConfig;
    }

    /**
    *
    */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            final boolean info = LOG.isInfoEnabled();
            long shutdownStartTimeMillis = 0;

            if (info) {
                LOG.info("CS GroupCache starting shutdown: " + m_groupId);
                shutdownStartTimeMillis = System.currentTimeMillis();
            }

            interrupt();

            if (info) {
                LOG.info("CS GroupCache shutdown finished in " + (System.currentTimeMillis() - shutdownStartTimeMillis) + "ms: " + m_groupId);
            }
        }
    }

    /**
     * isRunning
     */
    public boolean isRunning() {
        return m_running.get();
    }

    /**
     * @return
     */
    public long getMaxKeyAgeMillis() {
        final long oldestKeyTimestampMillis = m_oldestKeyTimestampMillis.get();
        return (oldestKeyTimestampMillis > 0) ? (System.currentTimeMillis() - oldestKeyTimestampMillis) : 0;
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    private void implPerformCleanup() {
        final long startTimeMillis = System.currentTimeMillis();
        long endTimeMillis = 0;
        long countDurationMillis = 0;
        long timeDurationMillis = 0;
        long sizeDurationMillis = 0;
        int countRemoved = 0;
        int timeRemoved = 0;
        int sizeRemoved = 0;

        try {
            countRemoved = implRemoveObsoleteCountBasedKeys();
        } catch (CacheException e) {
            LOG.error("CS GroupCache received exception during detection of removable count based keys: {} (Reason: {})", m_groupId, Throwables.getRootCause(e).getMessage());
        } finally {
            countDurationMillis = (timeDurationMillis = System.currentTimeMillis()) - startTimeMillis;
        }

        try {
            timeRemoved = implRemoveObsoleteTimeBasedKeys();
        } catch (CacheException e) {
            LOG.error("CS GroupCache received exception during detection of removable time based keys: {} (Reason: {})", m_groupId, Throwables.getRootCause(e).getMessage());
        } finally {
            timeDurationMillis = (sizeDurationMillis = System.currentTimeMillis()) - timeDurationMillis;
        }

        try {
            sizeRemoved = implRemoveObsoleteSizeBasedKeys();
        } catch (CacheException e) {
            LOG.error("CS GroupCache received exception during detection of removable size based keys: {} (Reason: {})", m_groupId, Throwables.getRootCause(e).getMessage());
        } finally {
            sizeDurationMillis = (endTimeMillis = System.currentTimeMillis()) - sizeDurationMillis;
        }

        try {
            // get age of oldest key for this group
            final AtomicLong oldestKeyTimestampMillis = new AtomicLong(0);

            if (1 == m_cache.iterateKeysByDescendingAge(
                m_groupId,
                null,
                "1",
                (curKeyObject) -> oldestKeyTimestampMillis.set(curKeyObject.getModificationDate().getTime()))) {

                m_oldestKeyTimestampMillis.set(oldestKeyTimestampMillis.get());
            }
        } catch (CacheException e) {
            LOG.error("GroupCache received exception during retrieval of max key age: {} (Reason: {})", m_groupId, Throwables.getRootCause(e).getMessage());
        }

        // log on INFO or TRACE level, depending on number of removed keys
        final long cleanupDurationMillis = endTimeMillis - startTimeMillis;
        final String cleanupLogInfo =
            "CS GroupCache cleanup for " + m_groupId + " removed keys based on count/size/time " + countRemoved + '/' + sizeRemoved + '/' +
            timeRemoved + " with duration " + countDurationMillis + '/' + sizeDurationMillis + '/' + timeDurationMillis + " ms";

        if ((countRemoved > 0) || (timeRemoved > 0) || (sizeRemoved > 0)) {
            if (m_constraintLimitNotReached) {
                implLogGeometry("CS GroupCache reached constraint limit for the first time for group: ", cleanupDurationMillis, true);
                m_constraintLimitNotReached = false;
            } else {
                implLogGeometry("CS GroupCache enabled for group: ", cleanupDurationMillis, false);
            }

            LOG.info(cleanupLogInfo);
        } else {
            implLogGeometry("CS GroupCache enabled for group: ", cleanupDurationMillis, false);
            LOG.trace(cleanupLogInfo);
        }
    }

    /**
     * @return
     * @throws CacheException
     */
    private int implRemoveObsoleteCountBasedKeys() throws CacheException {
        int removedKeys = 0;

        if (m_maxKeyCount >= 0) {
            final Set<String> keyIdSet = new LinkedHashSet<>(MAX_FILE_COUNT);
            long curCacheKeyCount = m_cache.getKeyCount(m_groupId);

            while (curCacheKeyCount > m_maxKeyCount) {
                final String limitClause = Long.toString(Math.min(curCacheKeyCount - m_maxKeyCount, MAX_FILE_COUNT));

                keyIdSet.clear();
                m_cache.iterateKeysByDescendingAge(m_groupId, null, limitClause, (curElement) -> keyIdSet.add(curElement.getKeyId()));

                final int curKeyCountToRemove = keyIdSet.size();

                if (curKeyCountToRemove > 0) {
                    m_cache.removeKeys(m_groupId, keyIdSet.toArray(new String[0]));

                    removedKeys += curKeyCountToRemove;
                    curCacheKeyCount -= curKeyCountToRemove;
                } else {
                    // leave in case, we got no items at all
                    break;
                }
            }
        }

        return removedKeys;
    }

    /**
     * @return
     * @throws CacheException
     */
    private int implRemoveObsoleteTimeBasedKeys() throws CacheException {
        int removedKeys = 0;

        if (m_keyTimeoutMillis >= 0) {
            final long maxKeyTimeMillis = System.currentTimeMillis() - m_keyTimeoutMillis;
            final Set<String> keyIdSet = new LinkedHashSet<>(MAX_FILE_COUNT);
            final String whereClause = "fp.ModificationDate <= " + maxKeyTimeMillis;
            final String limitClause = Integer.toString(MAX_FILE_COUNT);

            // no removal, if keys to remove < 0
            while (true) {
                keyIdSet.clear();
                m_cache.iterateKeysByDescendingAge(m_groupId, whereClause, limitClause, (curElement) -> keyIdSet.add(curElement.getKeyId()));

                final int curKeyCountToRemove = keyIdSet.size();

                if (curKeyCountToRemove > 0) {
                    m_cache.removeKeys(m_groupId, keyIdSet.toArray(new String[0]));
                    removedKeys += curKeyCountToRemove;
                } else {
                    // leave in case, we got no items at all
                    break;
                }
            }
        }

        return removedKeys;
    }

    /**
     * @return
     * @throws CacheException
     */
    private int implRemoveObsoleteSizeBasedKeys() throws CacheException {
        int removedKeys = 0;

        if (m_maxGroupSize >= 0) {
            final long sizeToRemove = m_cache.getGroupSize(m_groupId) - m_maxGroupSize;

            if (sizeToRemove > 0) {
                final Set<String> keyIdSet = new LinkedHashSet<>(MAX_FILE_COUNT);
                final String limitClause = Integer.toString(MAX_FILE_COUNT);
                final AtomicLong removedSize = new AtomicLong(0);

                while (removedSize.get() < sizeToRemove) {
                    keyIdSet.clear();
                    m_cache.iterateKeysByDescendingAge(m_groupId, null, limitClause, (curElement) -> {
                        if (removedSize.get() < sizeToRemove) {
                            keyIdSet.add(curElement.getKeyId());
                            removedSize.addAndGet(curElement.getLength());
                        }
                    });

                    final int curKeyCountToRemove = keyIdSet.size();

                    if (curKeyCountToRemove > 0) {
                        m_cache.removeKeys(m_groupId, keyIdSet.toArray(new String[0]));
                        removedKeys += curKeyCountToRemove;
                    } else {
                        // leave in case, we got no items at all
                        break;
                    }
                }
            }
        }

        return removedKeys;
    }

    /**
     * @param message
     * @param durationMillis
     * @param warn
     */
    private void implLogGeometry(@NonNull final String message, final long durationMillis, final boolean warn) {
        final String logMessage = message + "{} [cur/max keys: {}/{}, cur/max size: {}/{}, oldest key: {}s, duration: {}ms]";
        final String groupId = m_groupId;

        try {
            final long curKeyCount = m_cache.getKeyCount(groupId);
            final long curGroupSize = m_cache.getGroupSize(groupId);
            final long oldestEntrySeconds = m_cache.getMaxKeyAgeMillis(groupId) / 1000;

            if (warn) {
                LOG.warn(logMessage, groupId, curKeyCount, m_maxKeyCount, curGroupSize, m_maxGroupSize, oldestEntrySeconds, durationMillis);
            } else  {
                LOG.info(logMessage, groupId, curKeyCount, m_maxKeyCount, curGroupSize, m_maxGroupSize, oldestEntrySeconds, durationMillis);
            }
        } catch (CacheException e) {
            LOG.error("CS GroupCache received exception when retrieving cache group data", e);
        }
    }


    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicLong m_oldestKeyTimestampMillis = new AtomicLong(0);

    final private GroupConfig m_groupConfig;

    final private ICache m_cache;

    final private String m_groupId;

    final private long m_maxGroupSize;

    final private long m_maxKeyCount;

    final private long m_keyTimeoutMillis;

    final private long m_cleanupPeriodMillis;

    private boolean m_constraintLimitNotReached = true;
}
