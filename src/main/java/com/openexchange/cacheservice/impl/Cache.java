/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheConfig;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.GroupConfig;
import com.openexchange.cacheservice.api.ICache;
import com.openexchange.cacheservice.api.ICacheObject;
import com.openexchange.cacheservice.api.IObjectStore;
import com.openexchange.cacheservice.api.IReadAccess;
import com.openexchange.cacheservice.api.IWriteAccess;
import com.openexchange.cacheservice.api.IdLocker;
import com.openexchange.cacheservice.api.KeyObject;

/**
 * {@link Cache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@Component
public class Cache implements ICache {

    final public static Logger LOG = LoggerFactory.getLogger(Cache.class);

    final private static GroupConfig GROUP_CONFIG_EMPTY = GroupConfig.builder().build();
    final private static CacheObject[] CACHE_OBJECT_ARRAY_EMPTY = new CacheObject[0];
    final private static JSONObject JSON_OBJECT_EMPTY = new JSONObject();
    final private static String JSON_VALUE_NULL = "null";

    /**
     * Initializes a new {@link Cache}.
     */
    @Autowired
    public Cache(@NonNull final Environment env, final CacheConfig config, final CacheDatabase database) throws Exception {

        super();

        m_env = env;
        m_config = config;
        m_database = database;
        m_valid = (null != m_config) && m_config.isValid() && (null != m_database) && m_database.isValid();

        // initialize asynchronously (late init due to Spring-boot async init. sequence with Liquibase etc.)
        Executors.newSingleThreadExecutor().submit(this::implInit);
    }

    // - ICache ------------------------------------------------------

    @Override
    public CacheConfig getCacheConfig() {
        return m_config;
    }

    @Override
    public JSONObject getJSONHealth() {
        final JSONObject jsonHealth = new JSONObject();

        try {
            final boolean hasDatabase = hasDatabase();

            jsonHealth.put(CACHE_DATABASE_AVAILABLE, hasDatabase).put(CACHE_DATABASE_CONNECTION, m_env.getProperty("spring.datasource.url", JSON_VALUE_NULL)).put(CACHE_OBJECTSTORE_AVAILABLE, hasObjectStore()).put(CACHE_OBJECTSTORE_IDS, hasObjectStore() ?
                String.valueOf(m_objectStoreMap.keySet()) : JSON_VALUE_NULL);
        } catch (JSONException e) {
            LOG.error("CS Unable to create health JSON object: {}", Throwables.getRootCause(e).getMessage());
        }

        return jsonHealth;
    }

    @Override
    public boolean isValid() {
        return m_valid;
    }

    @Override
    public boolean hasDatabase() {
        return (null != m_database) && (m_database.isValid());
    }

    @Override
    public boolean hasObjectStore() {
        return (null != m_objectStoreMap);
    }

    @Override
    public DatabaseType getDatabaseType() {
        return (null != m_database) ? m_database.getDatabaseType() : DatabaseType.NONE;
    }

    @Override
    public GroupConfig registerGroup(final GroupConfig groupConfig) throws CacheException {
        if ((null == groupConfig) || !groupConfig.isValid()) {
            throw new CacheException("Not able to register group with empty or invalid configuration");
        }

        if (hasDatabase()) {
            // register group at database
            final GroupConfig registeredGroupConfig = m_database.registerGroup(groupConfig);

            // add/update returned GroupConfig to available GroupConfig map
            if (null != registeredGroupConfig) {
                implCreateOrUpdateGroupCache(registeredGroupConfig);
            }

            return registeredGroupConfig;
        }

        return null;
    }

    @Override
    public GroupConfig getGroupConfig(final String groupId) {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.getGroupConfig(groupId) : GROUP_CONFIG_EMPTY;
    }

    @Override
    public void shutdownGroup(final String groupId) {
        if (null != groupId) {
            GroupCache groupCacheToShutdown = null;

            synchronized (m_groupCacheMap) {
                groupCacheToShutdown = m_groupCacheMap.remove(groupId);
            }

            if (null != groupCacheToShutdown) {
                groupCacheToShutdown.shutdown();
            }
        }
    }

    @Override
    public String[] getCustomKeys(final String groupId) {
        if (hasDatabase() && CacheUtils.isValid(groupId)) {
            final Set<String> keySet = m_database.getCustomPropertyKeys(groupId);
            return keySet.toArray(new String[0]);
        }

        return ArrayUtils.EMPTY_STRING_ARRAY;
    }

    @Override
    public boolean hasCustomKey(final String groupId, final String keyId) {
        return hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId) && m_database.hasCustomPropertyKey(groupId, keyId);
    }

    @Override
    public JSONObject getUserData(@NonNull final String groupId) {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.getUserData(groupId) : JSON_OBJECT_EMPTY;
    }

    // -------------------------------------------------------------------------

    @Override
    public boolean containsGroup(final String groupId) {
        return hasDatabase() && CacheUtils.isValid(groupId) && m_database.contains(groupId);
    }

    @Override
    public boolean containsKey(final String groupId, final String keyId) throws CacheException {
        return hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId) && m_database.contains(groupId, keyId);
    }

    @Override
    public boolean contains(final String groupId, final String keyId, final String fileId) throws CacheException {
        return hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId) && CacheUtils.isValid(fileId) && m_database.contains(groupId, keyId, fileId);
    }

    // -------------------------------------------------------------------------

    @Override
    public ICacheObject get(String groupId, String keyId, String fileId) throws CacheException {
        if (hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId) && CacheUtils.isValid(fileId)) {
            final CacheObject cacheObj = new CacheObject(groupId, keyId, fileId);
            final ObjectStoreData objectStoreData = m_database.getObjectStoreData(cacheObj);

            if (CacheUtils.isValid(objectStoreData)) {
                cacheObj.setObjectStoreData(objectStoreData);
                return cacheObj;
            }
        }

        return new CacheObject(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
    }

    @Override
    public ICacheObject[] get(final String groupId, final String keyId) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId)) ? m_database.getFileItems(groupId, keyId) : CACHE_OBJECT_ARRAY_EMPTY;
    }

    @Override
    public ICacheObject[] get(final String groupId, final Properties properties) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId) && (null != properties) && (properties.size() > 0)) ? m_database.getFileItems(groupId, properties) : CACHE_OBJECT_ARRAY_EMPTY;
    }

    // -------------------------------------------------------------------------

    @Override
    public long getKeyCount(final String groupId) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.getKeyCount(groupId) : 0;
    }

    @Override
    public String[] getKeys(final String groupId) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.getKeyIds(groupId) : ArrayUtils.EMPTY_STRING_ARRAY;
    }

    @Override
    public long iterateKeysByDescendingAge(String groupId, String whereClause, String limitClause, Consumer<KeyObject> keyConsumer) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.iterateKeysByDescendingAge(groupId, whereClause, limitClause, keyConsumer) : 0;
    }

    @Override
    public long getGroupCount() {
        return hasDatabase() ? m_database.getGroupCount() : 0;
    }

    @Override
    public String[] getGroups() {
        return hasDatabase() ? m_database.getGroupIds() : ArrayUtils.EMPTY_STRING_ARRAY;
    }

    // -------------------------------------------------------------------------

    @Override
    public long getGroupSize(String groupId) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.getGroupLength(groupId) : 0;
    }

    @Override
    public long getGroupSize(String groupId, Properties properties) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId)) ? m_database.getGroupLength(groupId, null, properties) : 0;
    }

    @Override
    public long getKeySize(String groupId, String keyId) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId)) ? m_database.getGroupLength(groupId, keyId, null) : 0;
    }

    @Override
    public long getKeySize(String groupId, String keyId, Properties properties) throws CacheException {
        return (hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId)) ? m_database.getGroupLength(groupId, keyId, properties) : 0;
    }

    @Override
    public long getMaxKeyAgeMillis(String groupId) {
        GroupCache groupCache = null;

        synchronized (m_groupCacheMap) {
            groupCache = m_groupCacheMap.get(groupId);
        }

        if (null != groupCache) {
            return groupCache.getMaxKeyAgeMillis();
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    @Override
    public boolean remove(ICacheObject iFileItem) throws CacheException {
        final CacheObject cacheObject = (CacheObject) iFileItem;

        if (hasDatabase() && CacheUtils.isValid(cacheObject)) {
            final String cacheObjectKey = cacheObject.getKey();

            try {
                final List<ObjectStoreData> deletedOjectStoreDataList = new ArrayList<>();

                IdLocker.lock(cacheObjectKey);

                if (1 == m_database.deleteEntry(cacheObject.getGroupId(), cacheObject.getKeyId(), cacheObject.getFileId(), deletedOjectStoreDataList)) {
                    return (m_objectStoreRemover.addObjectStoreDatasToRemove(deletedOjectStoreDataList) > 0);
                }
            } finally {
                IdLocker.unlock(cacheObjectKey);
            }
        }

        return false;
    }

    @Override
    public boolean remove(String groupId, String keyId, String fileId) throws CacheException {
        return remove(new CacheObject(groupId, keyId, fileId));
    }

    @Override
    public int remove(ICacheObject[] fileElements) throws CacheException {
        int removedFileCount = 0;

        if (null != fileElements) {
            for (final ICacheObject curFileItem : fileElements) {
                remove(curFileItem);
                ++removedFileCount;
            }
        }

        return removedFileCount;
    }

    @Override
    public int remove(final String groupId, final Properties properties) throws CacheException {
        if (hasDatabase() && isNotEmpty(groupId) && (null != properties) && (properties.size() > 0)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(8192);
            final int deletedEntries = m_database.deleteByProperties(groupId, properties, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    @Override
    public int removeKey(final String groupId, final String keyId) throws CacheException {
        if (hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyId)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(8192);
            final int deletedEntries = m_database.deleteByKey(groupId, keyId, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    @Override
    public int removeKeys(final String groupId, final String[] keyIds) throws CacheException {
        if (hasDatabase() && CacheUtils.isValid(groupId) && CacheUtils.isValid(keyIds)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(8192);
            final String[] targetKeyIds = Arrays.stream(keyIds).filter(CacheUtils::isValid).toArray(String[]::new);

            final int deletedEntries = m_database.deleteByGroupKeys(groupId, targetKeyIds, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    @Override
    public int removeGroup(final String groupId) throws CacheException {
        if (hasDatabase() && CacheUtils.isValid(groupId)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(8192);
            final int deletedEntries = m_database.deleteByKey(groupId, null, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    @Override
    public IReadAccess getReadAccess(final ICacheObject iCacheObject) throws CacheException {
        final CacheObject cacheObject = (CacheObject) iCacheObject;

        if (CacheUtils.isInvalid(cacheObject)) {
            throw new CacheException("Invalid cache object");
        }

        final String cacheObjectKey = cacheObject.getKey();

        try {
            IdLocker.lock(cacheObjectKey);

            final ReadAccess readAcc = new ReadAccess(this, m_database, cacheObject);

            try {
                readAcc.open();
                return readAcc;
            } catch (Exception e) {
                CacheUtils.close(readAcc);
                throw new CacheException(e);
            }
        } finally {
            IdLocker.unlock(cacheObjectKey);
        }
    }

    @Override
    public IReadAccess getReadAccess(String groupId, String keyId, String fileId) throws CacheException {
        return getReadAccess(new CacheObject(groupId, keyId, fileId));
    }

    @Override
    public IWriteAccess getWriteAccess(final ICacheObject iCacheObject) throws CacheException {
        final CacheObject cacheObject = (CacheObject) iCacheObject;

        if (CacheUtils.isInvalid(cacheObject)) {
            throw new CacheException("Invalid cache object");
        }

        final String cacheObjectKey = cacheObject.getKey();

        try {
            IdLocker.lock(cacheObjectKey);

            final WriteAccess writeAcc = new WriteAccess(this, m_database, cacheObject);

            try {
                writeAcc.open();
                return writeAcc;
            } catch (Exception e) {
                CacheUtils.close(writeAcc);
                throw new CacheException(e);
            }
        } finally {
            IdLocker.unlock(cacheObjectKey);
        }
    }

    @Override
    public IWriteAccess getWriteAccess(String groupId, String keyId, String fileId) throws CacheException {
        return getWriteAccess(new CacheObject(groupId, keyId, fileId));
    }

    // - Package internal API --------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            synchronized (m_groupCacheMap) {
                m_groupCacheMap.forEach((curKey, curGroupCache) -> curGroupCache.shutdown());
                m_groupCacheMap.clear();
            }

            if (null != m_objectStoreRemover) {
                m_objectStoreRemover.shutdown();
            }

            CacheUtils.shutdown();
        }
    }

    /**
     * @param objectStoreData
     *
     * @return
     */
    protected IObjectStore getObjectStore(@NonNull ObjectStoreData objectStoreData) {
        return m_objectStoreMap.get(objectStoreData.getObjectStoreNumber());
    }

    /**
     * @param cacheObject
     * @param contentFile
     *
     * @throws Exception
     */
    protected void getObjectStoreObject(@NonNull CacheObject cacheObject, @NonNull final File contentFile) throws Exception {
        if (CacheUtils.isValid(cacheObject)) {
            final ObjectStoreData objectStoreData = cacheObject.getOjectStoreData();

            if (CacheUtils.isValid(objectStoreData)) {
                final IObjectStore objectStore = getObjectStore(objectStoreData);

                if (null != objectStore) {
                    objectStore.getObject(objectStoreData.getObjectStoreId(), contentFile);
                }
            }
        }
    }

    /**
     * @param cacheObject
     * @return
     */
    protected ObjectStoreData createNewObjectStoreObject(@NonNull ICacheObject cacheObject, @NonNull final File contentFile) throws IOException {
        final int objectStoreCount = m_objectStoreArray.length;

        if (objectStoreCount > 0) {
            // walk over all available ObjectStores round-robin wise and create a new ObjectStore object
            for (int i = 0; i < objectStoreCount; ++i) {
                final IObjectStore objectStore = m_objectStoreArray[implGetNextObjectStorePos()];

                try {
                    // create new ObjectStore object
                    final String objectId = objectStore.createObject(contentFile);

                    if (CacheUtils.isValid(objectId)) {
                        return new ObjectStoreData(objectStore.getId(), objectId);
                    }
                } catch (Exception e) {
                    throw new IOException("Creation of new ObjectStore object failed: " + cacheObject, e);
                }
            }
        }

        return null;
    }

    // - Implementation --------------------------------------------------------

    /**
     * Late init handler to be called asynchronously from Ctor
     */
    protected void implInit() {
        if (m_valid) {
            // we need to wait until the database instance will be initialized completely
            m_database.waitUntilInitialized();

            final Set<IObjectStore> objectStoreSet = m_config.getObjectStores();
            final int objectStoreCount = objectStoreSet.size();
            int curArrayPos = -1;

            m_objectStoreMap = new HashMap<>(objectStoreCount);
            m_objectStoreArray = new IObjectStore[objectStoreCount];

            for (final IObjectStore curObjectStore : objectStoreSet) {
                m_objectStoreArray[++curArrayPos] = curObjectStore;
                m_objectStoreMap.put(curObjectStore.getId(), curObjectStore);
            }

            // read existing GroupConfigs from database and register/start GroupCaches
            final String[] groupIds = m_database.getGroupIds();

            if (null != groupIds) {
                for (final String curGroupId : groupIds) {
                    final GroupConfig curGroupConfig = m_database.getGroupConfig(curGroupId);

                    if ((null != curGroupConfig) && curGroupConfig.isValid()) {
                        try {
                            implCreateOrUpdateGroupCache(curGroupConfig);
                        } catch (CacheException e) {
                            CacheUtils.logExcp(e);
                        }
                    }
                }
            }

            m_objectStoreRemover = new ObjectStoreRemover(this);

            LOG.info("CS Cache service running...");
        } else {
            m_objectStoreArray = null;
            m_objectStoreMap = null;
            m_objectStoreRemover = null;

            LOG.error("CS Cache service not available! [database available: {}, file store available: {}]", hasDatabase(), hasObjectStore());
        }
    }

    /**
     * @return
     */
    protected synchronized int implGetNextObjectStorePos() {
        return (m_curObjectStorePos = ++m_curObjectStorePos % m_objectStoreArray.length);
    }

    /**
     * @param groupConfig
     *
     * @throws CacheException
     */
    protected void implCreateOrUpdateGroupCache(@NonNull final GroupConfig groupConfig) throws CacheException {
        final String groupId = groupConfig.getGroupId();

        synchronized (m_groupCacheMap) {
            final GroupCache oldGroupCache = m_groupCacheMap.get(groupId);

            if (null != oldGroupCache) {
                // replace old GroupCache with new one if config for this group has changed
                if (!oldGroupCache.getConfig().toString().equals(groupConfig.toString()))
                {
                    m_groupCacheMap.remove(groupId);

                    oldGroupCache.shutdown();

                    try {
                        oldGroupCache.join(1000);
                    } catch (@SuppressWarnings("unused") InterruptedException e) {
                        // ok
                    }

                    m_groupCacheMap.put(groupId, new GroupCache(groupConfig, this));
                }
            } else {
                // add new GroupCache if it did not exist before
                m_groupCacheMap.put(groupId, new GroupCache(groupConfig, this));
            }
        }
    }

    // - Members ---------------------------------------------------------------

    final protected AtomicBoolean m_running = new AtomicBoolean(true);

    final protected Environment m_env;

    final protected CacheConfig m_config;

    final protected boolean m_valid;

    final protected CacheDatabase m_database;

    final protected Map<String, GroupCache> m_groupCacheMap = new HashMap<>();

    protected IObjectStore[] m_objectStoreArray = null;

    protected Map<Integer, IObjectStore> m_objectStoreMap = null;

    protected ObjectStoreRemover m_objectStoreRemover = null;

    protected int m_curObjectStorePos = -1;
}
