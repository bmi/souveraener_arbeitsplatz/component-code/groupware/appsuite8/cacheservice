/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.ICache;
import com.openexchange.cacheservice.impl.micrometer.Micrometer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link CacheMetrics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@Component
public class CacheMetrics implements InitializingBean {

    final private static char CHAR_DOT = '.';

    final private static String STR_ADDOBJECTS = "addobjects";
    final private static String STR_ADMIN = "admin";
    final private static String STR_BYTES = "bytes";
    final private static String STR_CACHED_FOR_A_GROUP = "cached for a group.";
    final private static String STR_CACHESERVICE = "cacheservice";
    final private static String STR_COUNT = "count";
    final private static String STR_CURRENT = "current";
    final private static String STR_GETGROUPINFO = "getgroupinfo";
    final private static String STR_GETOBJECTS = "getobjects";
    final private static String STR_GROUP = "group";
    final private static String STR_HASOBJECTS = "hasobjects";
    final private static String STR_MAXIMUM = "maximum";
    final private static String STR_ITEMS = "items";
    final private static String STR_MEDIANDURATION = "medianduration";
    final private static String STR_MODIFICATION = "modification";
    final private static String STR_MS = "ms";
    final private static String STR_OLDEST = "oldest";
    final private static String STR_REGISTERGROUP = "registergroup";
    final private static String STR_REQUEST = "request";
    final private static String STR_REQUESTS_FOR_A_GROUP = "requests for a group.";
    final private static String STR_SIZE = "size";
    final private static String STR_STATUS = "status";
    final private static String STR_TOTAL = "total";

    final private static String NAME_REQUEST_COUNT = STR_CACHESERVICE + CHAR_DOT + STR_REQUEST + CHAR_DOT + STR_COUNT;
    final private static String NAME_REQUEST_DURATION = STR_CACHESERVICE + CHAR_DOT + STR_REQUEST + CHAR_DOT + STR_MEDIANDURATION + CHAR_DOT + STR_MS;

    final private static String NAME_CACHE_COUNT = STR_CACHESERVICE + CHAR_DOT + STR_ITEMS + CHAR_DOT;
    final private static String NAME_CACHE_MAXCOUNT = NAME_CACHE_COUNT + STR_MAXIMUM + CHAR_DOT + STR_COUNT;
    final private static String NAME_CACHE_CURCOUNT = NAME_CACHE_COUNT + STR_CURRENT + CHAR_DOT + STR_COUNT;

    final private static String NAME_CACHE_SIZE = STR_CACHESERVICE + CHAR_DOT + STR_SIZE + CHAR_DOT;
    final private static String NAME_CACHE_MAXSIZE = NAME_CACHE_SIZE + STR_MAXIMUM;
    final private static String NAME_CACHE_CURSIZE = NAME_CACHE_SIZE + STR_CURRENT;

    final private static String NAME_CACHE_OLDEST_MODIFICATION = STR_CACHESERVICE + CHAR_DOT + STR_OLDEST + CHAR_DOT + STR_MODIFICATION + CHAR_DOT + STR_MS;

    final private static Tags TAG_REQUEST_TOTAL = Tags.of(STR_REQUEST, STR_TOTAL);
    final private static Tags TAG_REQUEST_STATUS = Tags.of(STR_REQUEST, STR_STATUS);
    final private static Tags TAG_REQUEST_REGISTERGROUP = Tags.of(STR_REQUEST, STR_REGISTERGROUP);
    final private static Tags TAG_REQUEST_GETGROUPINFO = Tags.of(STR_REQUEST, STR_GETGROUPINFO);
    final private static Tags TAG_REQUEST_HASOBJECTS = Tags.of(STR_REQUEST, STR_HASOBJECTS);
    final private static Tags TAG_REQUEST_GETOBJECTS = Tags.of(STR_REQUEST, STR_GETOBJECTS);
    final private static Tags TAG_REQUEST_ADDOBJECTS = Tags.of(STR_REQUEST, STR_ADDOBJECTS);

    // use an odd number for request duration count to speed up finding of the median value
    final private static int REQUEST_DURATION_MAX_COUNT = 512;

    /**
     * {@link RequestType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum RequestType {
        TOTAL,
        STATUS,
        REGISTERGROUP,
        GETGROUPINFO,
        HASOBJECTS,
        GETOBJECTS,
        ADDOBJECTS
    }

    /**
     * Initializes a new {@link CacheMetrics}.
     *
     * @param cache
     */
    @Autowired
    public CacheMetrics(@NonNull final Cache cache) {
        super();
        m_cache = cache;
    }

    @Override
    public void afterPropertiesSet() {
        // we only register a group once
        if (!implRegisterGroup(STR_ADMIN)) {
            return;
        }

        m_requestCountTotalMap.put(STR_ADMIN, new AtomicLong());
        m_requestDurationTotalMap.put(STR_ADMIN, new LinkedList<>());

        // initialize metrics for admin group
        final Tags adminGroupTags = Tags.of(STR_GROUP, STR_ADMIN);

        // Request counts for "admin" group
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT + CHAR_DOT + STR_TOTAL, TAG_REQUEST_TOTAL.and(adminGroupTags), "The total number of " + STR_REQUESTS_FOR_A_GROUP, null, this,
            (m) -> (isRunning() ? getRequestCount(STR_ADMIN, RequestType.TOTAL) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_STATUS.and(adminGroupTags), "The number of health and status related " + STR_REQUESTS_FOR_A_GROUP, null, this,
            (m) -> (isRunning() ? getRequestCount(STR_ADMIN, RequestType.STATUS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_GETGROUPINFO.and(adminGroupTags), "The number of all getGroupInfo related " + STR_REQUESTS_FOR_A_GROUP, null, this,
            (m) -> (isRunning() ? getRequestCount(STR_ADMIN, RequestType.GETGROUPINFO) : 0.0));

        // Median request processing times for "admin" group
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION + CHAR_DOT + STR_TOTAL, TAG_REQUEST_TOTAL.and(adminGroupTags), "The median processing time of requests in milliseconds.", null, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(STR_ADMIN, RequestType.TOTAL) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_STATUS.and(adminGroupTags), "The median processing time of health and status related requests.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(STR_ADMIN, RequestType.STATUS) : 0.0));

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_GETGROUPINFO.and(adminGroupTags), "The median processing time of getGroupInfo related requests for all groups.", STR_MS, this,
            (m) -> (isRunning() ? getMedianRequestDurationMillis(STR_ADMIN, RequestType.GETGROUPINFO) : 0.0));
    }

    /**
     * @param requestType
     * @param requestDurationMillis
     */
    public void incrementAdminRequestCount(@NonNull final RequestType requestType, final long requestDurationMillis) {
        incrementRequestCount(STR_ADMIN, requestType, requestDurationMillis);
    }

    /**
     * @param group
     * @param requestType
     * @param requestDurationMillis
     */
    public void incrementRequestCount(@NonNull final String group, @NonNull final RequestType requestType, final long requestDurationMillis) {
        // always call #implCreateOrGetRequestCountMap first in oder for all group maps to be available
        final Map<RequestType, AtomicLong> curRequestCountMap = implCreateOrGetRequestCountMap(group);

        // increment request type count well as total request count within group
        curRequestCountMap.get(requestType).incrementAndGet();
        m_requestCountTotalMap.get(group).incrementAndGet();

        // append request duration for request type within group
        final Long curDuration = requestDurationMillis;
        final LinkedList<Long> curRequestTypeDurationList = implCreateOrGetRequestDurationMap(group).get(requestType);

        synchronized (curRequestTypeDurationList) {
            if (REQUEST_DURATION_MAX_COUNT == curRequestTypeDurationList.size()) {
                // ensure a maximum number of duration entries
                curRequestTypeDurationList.removeFirst();
            }

            curRequestTypeDurationList.addLast(curDuration);
        }

        // append request duration for all request types for group
        final LinkedList<Long> curRequestDurationTotalList = m_requestDurationTotalMap.get(group);

        synchronized (curRequestDurationTotalList) {
            if (REQUEST_DURATION_MAX_COUNT == curRequestDurationTotalList.size()) {
                // ensure a maximum number of duration entries
                curRequestDurationTotalList.removeFirst();
            }

            curRequestDurationTotalList.addLast(curDuration);
        }
    }

    /**
     * @param group
     * @param requestType
     *
     * @return
     */
    public long getRequestCount(@NonNull final String group, @NonNull final RequestType requestType) {
        // always call #implCreateOrGetRequestCountMap first in oder for all group maps to be available
        final Map<RequestType, AtomicLong> curRequestCountMap = implCreateOrGetRequestCountMap(group);

        return (RequestType.TOTAL == requestType) ?
            m_requestCountTotalMap.get(group).get() :
                curRequestCountMap.get(requestType).get();
    }

    /**
     * @param requestType
     * @return
     */
    public long getMedianRequestDurationMillis(@NonNull final String group, @Nullable final RequestType requestType) {
        // always call #implCreateOrGetRequestCountMap first in oder for all group maps to be available
        final Map<RequestType, LinkedList<Long>> curRequestDurationMap = implCreateOrGetRequestDurationMap(group);

        final LinkedList<Long> curRequestTypeDurationList = (RequestType.TOTAL == requestType) ?
            m_requestDurationTotalMap.get(group) :
                curRequestDurationMap.get(requestType);

        Long[] unsortedArray = new Long[curRequestTypeDurationList.size()];

        synchronized (curRequestTypeDurationList) {
            unsortedArray = curRequestTypeDurationList.toArray(unsortedArray);
        }

        return implGetMedianValue(unsortedArray);
    }

    /**
     *
     */
    public void shutdown() {
        m_running.compareAndSet(true, false);
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_running.get();
    }

    /**
     * @param group
     */
    public void registerGroup(@NonNull final String group) {
        // we only register a group once
        if (!implRegisterGroup(group)) {
            return;
        }

        final Tags curGroupTags = Tags.of(STR_GROUP, group);

        // Request counts for group
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT + CHAR_DOT + STR_TOTAL, TAG_REQUEST_TOTAL.and(curGroupTags), "The total number of " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getRequestCount(group, RequestType.TOTAL) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_REGISTERGROUP.and(curGroupTags), "The number of registerGroup " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getRequestCount(group, RequestType.REGISTERGROUP) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_GETGROUPINFO.and(curGroupTags), "The number of getGroupInfo " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getRequestCount(group, RequestType.GETGROUPINFO) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_HASOBJECTS.and(curGroupTags), "The number of hasObjects " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getRequestCount(group, RequestType.HASOBJECTS) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_GETOBJECTS.and(curGroupTags), "The number of getObjects " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getRequestCount(group, RequestType.GETOBJECTS) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_COUNT, TAG_REQUEST_ADDOBJECTS.and(curGroupTags), "The number of addObjects " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getRequestCount(group, RequestType.ADDOBJECTS) : 0.0));

        // Median request processing times for group
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION + CHAR_DOT + STR_TOTAL, TAG_REQUEST_TOTAL.and(curGroupTags), "The median processing time of requests in milliseconds  " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getMedianRequestDurationMillis(group, RequestType.TOTAL) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_REGISTERGROUP.and(curGroupTags), "The median processing time of registerGroup " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getMedianRequestDurationMillis(group, RequestType.REGISTERGROUP) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_GETGROUPINFO.and(curGroupTags), "The median processing time of getGroupInfo " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getMedianRequestDurationMillis(group, RequestType.GETGROUPINFO) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_HASOBJECTS.and(curGroupTags), "The median processing time of hasObjects " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getMedianRequestDurationMillis(group, RequestType.HASOBJECTS) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_GETOBJECTS.and(curGroupTags), "The median processing time of getObjects " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getMedianRequestDurationMillis(group, RequestType.GETOBJECTS) : 0.0));
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_REQUEST_DURATION, TAG_REQUEST_ADDOBJECTS.and(curGroupTags), "The median processing time of addObjects " + STR_REQUESTS_FOR_A_GROUP, null, this, (m) -> (isRunning() ? getMedianRequestDurationMillis(group, RequestType.ADDOBJECTS) : 0.0));

        // Cache counts
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_MAXCOUNT, curGroupTags, "The maximum number of keys that can be " + STR_CACHED_FOR_A_GROUP, null, this, (m) -> isRunning() ? m_cache.getGroupConfig(group).getMaxKeyCount() : 0.0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_CURCOUNT, curGroupTags, "The current number of keys that are " + STR_CACHED_FOR_A_GROUP, null, this, (m) -> {
            try {
                return isRunning() ? m_cache.getKeyCount(group) : 0.0;
            } catch (@SuppressWarnings("unused") CacheException e) {
                return 0.0;
            }
        });

        // Cache sizes
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_MAXSIZE, curGroupTags, "The maximum size of all objects that can be " + STR_CACHED_FOR_A_GROUP, STR_BYTES, this, (m) -> isRunning() ? m_cache.getGroupConfig(group).getMaxGroupSize() : 0.0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_CURSIZE, curGroupTags, "The current size of all objects that are " + STR_CACHED_FOR_A_GROUP, STR_BYTES, this, (m) -> {
            try {
                return isRunning() ? m_cache.getGroupSize(group) : 0.0;
            } catch (@SuppressWarnings("unused") CacheException e) {
                return 0.0;
            }
        });

        // Cache oldest key modification age
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, NAME_CACHE_OLDEST_MODIFICATION, curGroupTags, "The age of the key with the oldest modification of all keys that are " + STR_CACHED_FOR_A_GROUP, STR_MS, this, (m) -> {
            try {
                return isRunning() ? m_cache.getMaxKeyAgeMillis(group) : 0.0;
            } catch (@SuppressWarnings("unused") CacheException e) {
                return 0.0;
            }
        });
    }

    // - Implementation --------------------------------------------------------

    /**
     * Method is synchronized!
     * A new group entry is added to the specific request type count
     * for the group as well as to the total count for the group
     *
     * @param group
     * @return
     */
    @NonNull private Map<RequestType, AtomicLong> implCreateOrGetRequestCountMap(@NonNull final String group) {
        synchronized (m_requestCountMap) {
            Map<RequestType, AtomicLong> curRequestCountMap = m_requestCountMap.get(group);

            if (null == curRequestCountMap) {
                curRequestCountMap = new HashMap<>();

                // initialize request count map
                for (final RequestType curRequestType : RequestType.values()) {
                    curRequestCountMap.put(curRequestType, new AtomicLong(0));
                }

                // add request type counts in map for group
                m_requestCountMap.put(group, curRequestCountMap);

                // add total count for group
                m_requestCountTotalMap.put(group, new AtomicLong(0));
            }

            return curRequestCountMap;
        }
    }

    /**
     * Method is synchronized!
     *
     * A new group entry is added to the specific request type list
     * for the group as well as to the total list for the group
     *
     * @param group
     * @return
     */
    @NonNull private Map<RequestType, LinkedList<Long>> implCreateOrGetRequestDurationMap(@NonNull final String group) {
        synchronized (m_requestDurationMap) {
            Map<RequestType, LinkedList<Long>> curRequestDurationMap = m_requestDurationMap.get(group);

            if (null == curRequestDurationMap) {
                curRequestDurationMap = new HashMap<>();

                // initialize request count map
                for (final RequestType curRequestType : RequestType.values()) {
                    curRequestDurationMap.put(curRequestType, new LinkedList<>());
                }

                // add request type duration lists in map for group
                m_requestDurationMap.put(group, curRequestDurationMap);

                // add total duration list for group
                m_requestDurationTotalMap.put(group, new LinkedList<>());
            }

            return curRequestDurationMap;
        }
    }

    /**
     * !!! Method is not synchronized => ensure that provided array is thread safe !!!
     *
     * @param longArray
     * @return
     */
    private long implGetMedianValue(@NonNull final Long[] longArray) {
        final int length = longArray.length;

        if (length < 1) {
            return 0;
        }

        // sort array in ascending order
        Arrays.sort(longArray);

        // determine position of mid value (odd case) or of right value of mid (even case)
        final int valuePos = length >> 1;

        if ((length & 1) == 1) {
            // odd case => return mid value of array
            return longArray[valuePos];
        }

        // even case => return mean value of values left and right of mid
        return (long) ((longArray[valuePos - 1] + longArray[valuePos]) / 2.0);
    }

    /**
     * @param group
     * @return
     */
    private boolean implRegisterGroup(@NonNull final String group) {
        synchronized (m_registeredGroups) {
            return !m_registeredGroups.contains(group) && m_registeredGroups.add(group);
        }
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private ICache m_cache;

    final private Set<String> m_registeredGroups = new HashSet<>();

    final private Map<String, Map<RequestType, AtomicLong>> m_requestCountMap = new HashMap<>();

    final private Map<String, AtomicLong> m_requestCountTotalMap = new HashMap<>();

    final private Map<String, Map<RequestType, LinkedList<Long>>> m_requestDurationMap = new HashMap<>();

    final private Map<String, LinkedList<Long>> m_requestDurationTotalMap = new HashMap<>();
}
