/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.api;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Throwables;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

/**
 * {@link CacheConfig}
 * Thread safe, read only class to be initialized via
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class CacheConfig {

    /**
     * {@link CacheConfig} (not thread safe)
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class Builder {

        /**
         * Initializes a new {@link CacheConfig}.
         */
        public Builder() {
            super();

            m_dbProperties.putAll(DB_DEFAULT_PROPERTIES);
        }

        /**
         * @return
         */
        public CacheConfig build() {
            final CacheConfig config = new CacheConfig();

            config.m_objectStoreIdSet = m_objectStoreIdSet;
            config.m_objectStores = m_objectStores;
            config.m_dbHost = m_dbHost;
            config.m_dbPort = m_dbPort;
            config.m_dbSchema = m_dbSchema;
            config.m_dbUser = m_dbUser;
            config.m_dbPassword = m_dbPassword;
            config.m_dbRootPassword = m_dbRootPassword;
            config.m_dbProperties = m_dbProperties;
            config.m_dbPoolMaxSize = m_dbPoolMaxSize;
            config.m_dbPoolConnectTimeoutMillis = m_dbPoolConnectTimeoutMillis;
            config.m_dbPoolIdleTimeoutMillis = m_dbPoolIdleTimeoutMillis;
            config.m_dbPoolMaxLifetimeMillis = m_dbPoolMaxLifetimeMillis;
            config.m_spoolPath = m_spoolPath;
            config.m_defaultCacheMaxKeyCount = m_defaultCacheMaxKeyCount;
            config.m_defaultCacheMaxSize = m_defaultCacheMaxSize;
            config.m_defaultCacheKeyTimeoutMillis = m_defaultCacheKeyTimeoutMillis;
            config.m_defaultCacheCleanupPeriodMillis = m_defaultCacheCleanupPeriodMillis;

            return config;
        }

        /**
         * @param objectStoreId
         * @param objectStore
         *
         * @return
         */
        public Builder withObjectStore(final String objectStoreId, final IObjectStore objectStore) {
            if ((null != objectStoreId) && (null != objectStore)) {
                m_objectStoreIdSet.add(objectStoreId);
                m_objectStores.add(objectStore);
            }

            return this;
        }

        /**
         * @param objectStoreId
         *
         * @return
         */
        public boolean containsObjectStore(final String objectStoreId) {
            return m_objectStoreIdSet.contains(objectStoreId);
        }

        /**
         * @param dbHost
         *
         * @return
         */
        public Builder withDBHost(final String dbHost) {
            m_dbHost = dbHost;
            return this;
        }

        /**
         * @param dbPort
         *
         * @return
         */
        public Builder withDBPort(final int dbPort) {
            m_dbPort = dbPort;
            return this;
        }

        /**
         * @param dbSchema
         *
         * @return
         */
        public Builder withDBSchema(final String dbSchema) {
            m_dbSchema = dbSchema;
            return this;
        }

        /**
         * @param dbUser
         *
         * @return
         */
        public Builder withDBUser(final String dbUser) {
            m_dbUser = dbUser;
            return this;
        }

        /**
         * @param dbPassword
         *
         * @return
         */
        public Builder withDBPassword(final String dbPassword) {
            m_dbPassword = dbPassword;
            return this;
        }

        /**
         * @param dbRootPassword
         *
         * @return
         */
        public Builder withDBRootPassword(final String dbRootPassword) {
            m_dbRootPassword = dbRootPassword;
            return this;
        }

        /**
         * @param dbProperties
         *
         * @return
         */
        public Builder withDBProperties(final Properties dbProperties) {
            m_dbProperties.putAll(dbProperties);
            return this;
        }

        /**
         * @param dbPoolMaxSize
         *
         * @return
         */
        public Builder withDBPoolMaxSize(final int dbPoolMaxSize) {
            m_dbPoolMaxSize = dbPoolMaxSize;
            return this;
        }

        /**
         * @param dbPoolConnectTimeoutMillis
         *
         * @return
         */
        public Builder withDBPoolConnectTimeoutMillis(final int dbPoolConnectTimeoutMillis) {
            m_dbPoolConnectTimeoutMillis = dbPoolConnectTimeoutMillis;
            return this;
        }

        /**
         * @param dbPoolIdleTimeoutMillis
         *
         * @return
         */
        public Builder withDBPoolIdleTimeoutMillis(final int dbPoolIdleTimeoutMillis) {
            m_dbPoolIdleTimeoutMillis = dbPoolIdleTimeoutMillis;
            return this;
        }

        /**
         * @param dbPoolMaxLifetimeMillis
         *
         * @return
         */
        public Builder withDBPoolMaxLifetimeMillis(final int dbPoolMaxLifetimeMillis) {
            m_dbPoolMaxLifetimeMillis = dbPoolMaxLifetimeMillis;
            return this;
        }

        /**
         * @param spoolPath
         *
         * @return
         */
        public Builder withSpoolPath(final File spoolPath) {
            m_spoolPath = spoolPath;
            return this;
        }

        /**
         * @param defaultCacheMaxKeyCount
         *
         * @return
         */
        public Builder withDefaultCacheMaxKeyCount(final long defaultCacheMaxKeyCount) {
            m_defaultCacheMaxKeyCount = defaultCacheMaxKeyCount;
            return this;
        }

        /**
         * @param defaultCacheMaxSizeGB
         *
         * @return
         */
        public Builder withDefaultCacheMaxSizeGB(final long defaultCacheMaxSizeGB) {
            m_defaultCacheMaxSize = defaultCacheMaxSizeGB * 1024 * 1024 * 1024;
            return this;
        }

        /**
         * @param defaultCacheKeyTimeoutMinutes
         *
         * @return
         */
        public Builder withDefaultCacheKeyTimeoutMinutes(final long defaultCacheKeyTimeoutMinutes) {
            m_defaultCacheKeyTimeoutMillis = defaultCacheKeyTimeoutMinutes * 60 * 1000;
            return this;
        }

        public Builder withDefaultCacheCleanupPeriodMillis(final long defaultCacheCleanupPeriodMillis) {
            m_defaultCacheCleanupPeriodMillis = defaultCacheCleanupPeriodMillis;
            return this;
        }

        // - Members ---------------------------------------------------------------

        final private Set<String> m_objectStoreIdSet = new LinkedHashSet<>();

        final private Set<IObjectStore> m_objectStores = new LinkedHashSet<>();

        final private Properties m_dbProperties = new Properties();

        private String m_dbHost = null;

        private int m_dbPort = 3306;

        private String m_dbSchema = "cacheservicedb";

        private String m_dbUser = null;

        private String m_dbPassword = null;

        private String m_dbRootPassword = null;

        private int m_dbPoolMaxSize = 30;

        private int m_dbPoolConnectTimeoutMillis = 10000;

        private int m_dbPoolIdleTimeoutMillis = 300000;

        private int m_dbPoolMaxLifetimeMillis = 600000;

        private File m_spoolPath = new File("/var/spool/open-xchange/cacheservice");

        private long m_defaultCacheMaxKeyCount = 1000000;

        private long m_defaultCacheMaxSize = -1;

        private long m_defaultCacheKeyTimeoutMillis = 2592000;

        private long m_defaultCacheCleanupPeriodMillis = 300000;

        // - Static members ----------------------------------------------------

        final private static Properties DB_DEFAULT_PROPERTIES = new Properties();

        static {
            // initialization of static members
            DB_DEFAULT_PROPERTIES.put("useUnicode", "true");
            DB_DEFAULT_PROPERTIES.put("characterEncoding", "UTF-8");
            DB_DEFAULT_PROPERTIES.put("autoReconnect", "false");
            DB_DEFAULT_PROPERTIES.put("useServerPrepStmts", "true");
            DB_DEFAULT_PROPERTIES.put("useTimezone", "true");
            DB_DEFAULT_PROPERTIES.put("serverTimezone", "UTC");
            DB_DEFAULT_PROPERTIES.put("connectTimeout", "10000");
            DB_DEFAULT_PROPERTIES.put("socketTimeout", "300000");
            DB_DEFAULT_PROPERTIES.put("prepStmtCacheSize", "250");
            DB_DEFAULT_PROPERTIES.put("prepStmtCacheSqlLimit", "4096");
            DB_DEFAULT_PROPERTIES.put("cachePrepStmts", "true");
            DB_DEFAULT_PROPERTIES.put("useSSL", "false");
            DB_DEFAULT_PROPERTIES.put("requireSSL", "false");
            DB_DEFAULT_PROPERTIES.put("verifyServerCertificate", "false");
            DB_DEFAULT_PROPERTIES.put("enabledTLSProtocols", "TLSv1,TLSv1.1,TLSv1.2");
            DB_DEFAULT_PROPERTIES.put("clientCertificateKeyStoreUrl", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("clientCertificateKeyStorePassword", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("clientCertificateKeyStoreType", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("trustCertificateKeyStoreUrl", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("trustCertificateKeyStorePassword", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("trustCertificateKeyStoreType", StringUtils.EMPTY);
        }
    }

    /**
     * Initializes a new {@link CacheConfig}.
     */
    private CacheConfig() {
        super();
    }

    /**
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * @return <code>true</code> if all necessary config values are set, <code>false</code> otherwise
     */
    public boolean isValid() {
        boolean spoolPathValid = false;

        // check or create spool directory
        if (null != m_spoolPath) {
            if (m_spoolPath.isDirectory() && m_spoolPath.canWrite()) {
                spoolPathValid = true;
            } else {
                try {
                    FileUtils.forceMkdir(m_spoolPath);
                    spoolPathValid = true;
                } catch (IOException e) {
                    LOG.error("CS Not able to create spool directory: {} ({})", m_spoolPath.getAbsolutePath(), Throwables.getRootCause(e).getMessage());
                }
            }
        }

        return spoolPathValid && (null != m_objectStoreIdSet) && (m_objectStoreIdSet.size() > 0) && (null != m_objectStores) && (m_objectStores.size() > 0) && isNotBlank(m_dbHost) && isNotBlank(m_dbSchema) && isNotBlank(m_dbUser) && isNotBlank(m_dbPassword);
    }

    /**
     * @return
     */
    public Set<String> getObjectStoreIds() {
        return m_objectStoreIdSet;
    }

    /**
     * @return
     */
    public Set<IObjectStore> getObjectStores() {
        return m_objectStores;
    }

    /**
     * @return
     */
    public String getDBHost() {
        return m_dbHost;
    }

    /**
     * @return
     */
    public int getDBPort() {
        return m_dbPort;
    }

    /**
     * @return
     */
    public String getDBSchema() {
        return m_dbSchema;
    }

    /**
     * @return
     */
    public String getDBUser() {
        return m_dbUser;
    }

    /**
     * @return
     */
    public String getDBPassword() {
        return m_dbPassword;
    }

    /**
     * @return
     */
    public String getDBRootPassword() {
        return m_dbRootPassword;
    }

    /**
     * @return
     */
    public Properties getDBProperties() {
        return m_dbProperties;
    }

    /**
     * @return
     */
    public int getDBPoolMaxSize() {
        return m_dbPoolMaxSize;
    }

    /**
     * @return
     */
    public int getDBPoolConnectTimeoutMillis() {
        return m_dbPoolConnectTimeoutMillis;
    }

    /**
     * @return
     */
    public int getDBPoolIdleTimeoutMillis() {
        return m_dbPoolIdleTimeoutMillis;
    }

    /**
     * @return
     */
    public int getDBPoolMaxLifetimeMillis() {
        return m_dbPoolMaxLifetimeMillis;
    }

    /**
     * @return
     */
    public File getSpoolPath() {
        return m_spoolPath;
    }

    public long getDefaultCacheMaxKeyCount() { return m_defaultCacheMaxKeyCount; }

    public long getDefaultCacheMaxSize() { return m_defaultCacheMaxSize; }

    public long getDefaultCacheKeyTimeoutMillis() { return m_defaultCacheKeyTimeoutMillis; }

    public long getDefaultCacheCleanupPeriodMillisp() { return m_defaultCacheCleanupPeriodMillis; }

    // - Members ---------------------------------------------------------------

    private Set<String> m_objectStoreIdSet;

    private Set<IObjectStore> m_objectStores;

    private String m_dbHost;

    private int m_dbPort;

    private String m_dbSchema;

    private String m_dbUser;

    private String m_dbPassword;

    private String m_dbRootPassword;

    private Properties m_dbProperties;

    private int m_dbPoolMaxSize;

    private int m_dbPoolConnectTimeoutMillis;

    private int m_dbPoolIdleTimeoutMillis;

    private int m_dbPoolMaxLifetimeMillis;

    private File m_spoolPath;

    private long m_defaultCacheMaxKeyCount;

    private long m_defaultCacheMaxSize;

    private long m_defaultCacheKeyTimeoutMillis;

    private long m_defaultCacheCleanupPeriodMillis;
}
