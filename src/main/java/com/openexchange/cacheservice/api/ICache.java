/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.api;

import java.io.Closeable;
import java.util.Properties;
import java.util.function.Consumer;
import org.json.JSONObject;
import org.springframework.lang.Nullable;

/**
 * {@link ICache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
/**
 * {@link ICache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public interface ICache {

    /**
     * CACHE_DATABASE_AVAILABLE
     */
    String CACHE_DATABASE_AVAILABLE = "databaseAvailable";

    /**
     * CACHE_DATABASE_CONNECTION
     */
    String CACHE_DATABASE_CONNECTION = "databaseConnection";

    /**
     * CACHE_OBJECTSTORE_AVAILABLE
     */
    String CACHE_OBJECTSTORE_AVAILABLE = "objectStoreAvailable";

    /**
     * CACHE_OBJECTSTORE_IDS
     */
    String CACHE_OBJECTSTORE_IDS = "objectStoreIds";

    /**
     * {@link DatabaseType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    enum DatabaseType {
        /**
         * No database is available
         */
        NONE,

        /**
         * A database, that supports only standard SQL queries.
         */
        STANDARD_SQL,

        /**
         * A database, that supports standard SQL queries as well as MySQL specific queries.
         */
        MYSQL
    }

    /**
     * Getting the current status of the implementing service.
     *
     * @return <code>true</code> in case the implementing service is valid,
     *         <code>false</code> otherwise.
     */
    default boolean isValid() {
        return true;
    }

    /**
     * @return
     */
    boolean hasObjectStore();

    /**
     * @return
     */
    boolean hasDatabase();

    /**
     * Retrieving the type of database, internally used to store and retrieve file metadata.
     *
     * @return The {@link DatabaseType}.
     */
    DatabaseType getDatabaseType();

    /**
     * @return
     */
    JSONObject getJSONHealth();

    CacheConfig getCacheConfig();

    /**
     * Registering a group with possible custom keys, that can be used as user
     * defined properties of a CacheObject for the group.
     * The registering of a group with its custom keys needs to be done prior to the usage of
     * that group and key(s) when accessing the {@link IReadAccess#getKeyValue(String)}
     *
     * @param cacheConfig The configuration to register the group with
     * @return The updated {@link GroupConfig} after registration
     * @throws CacheException
     */
    GroupConfig registerGroup(GroupConfig groupConfig) throws CacheException;

    /**
     * @param groupId The groupId for which the group configuration is to be retrieved
     * @return The {@link GroupConfig} for the requestes groupId
     */
    GroupConfig getGroupConfig(String groupId);

    /**
     * Shutting down a group when it is not to be used anymore for the current instance
     *
     * @param groupId The group id of the group to shutdown
     */
    void shutdownGroup(String groupId);

    /**
     * Getting the array of custom keys, registered for a specific group.
     *
     * @param groupId The groupId for which all custom keys are to be retrieved
     * @return The array of custom keys for the given group
     * @throws CacheException
     */
    String[] getCustomKeys(String groupId) throws CacheException;

    /**
     * Querying, if a custom key is already registered for the group and thus can be used
     *
     * @param groupId The groupId to query for the given custom key
     * @param customKey The custom key to query the group for.
     * @return <code>true</code>, if the given key has been registered for the group,
     *         <code>false</code> otherwise
     */
    boolean hasCustomKey(String groupId, String customKey) throws CacheException;

    /**
     * Querying the user data for the group.
     *
     * @param groupId The groupId of the group to query for user data
     * @return The user data as {@link JSONObject}.
     *         If no user data is set, an empty object will be returned.
     */
    JSONObject getUserData(String groupId) throws CacheException;

    // -------------------------------------------------------------------------

    /**
     * Querying, if a group is contained in the collection.
     *
     * @param groupId
     * @return true, if the collections contains the group with the given id
     * @throws CacheException
     */
    boolean containsGroup(String groupId) throws CacheException;

    /**
     * Querying, if a key is contained in the collection.
     *
     * @param groupId
     * @param keyId
     * @return true, if the collections contains the group-key with the given ids
     * @throws CacheException
     */
    boolean containsKey(String groupId, String keyId) throws CacheException;

    /**
     * Querying, if a file is contained in the collection.
     *
     * @param groupId
     * @param keyId
     * @return true, if the collections contains the group-key with the given id
     * @throws CacheException
     */
    boolean contains(String groupId, String keyId, String fileId) throws CacheException;

    // -------------------------------------------------------------------------

    /**
     * Getting the {@link ICacheObject} interface for the given file item.
     * If no such item exists, <code>null</code> is returned.
     *
     * @return The {@link ICacheObject} interface or <code>null</code>.
     * @throws CacheException
     */
    ICacheObject get(String groupId, String keyId, String fileId) throws CacheException;

    /**
     * Getting all {@link ICacheObject} interfaces of the given group-key as an array.
     *
     * @param groupId
     * @param keyId
     * @return The array of {@link ICacheObject} interfaces
     * @throws CacheException
     */
    ICacheObject[] get(String groupId, String keyId) throws CacheException;

    /**
     * Getting all {@link ICacheObject} interfaces of the given group,
     * that match the given search properties, as an array.
     *
     * @param groupId
     * @return The array of key ids
     * @throws CacheException
     */
    ICacheObject[] get(String groupId, Properties properties) throws CacheException;

    /**
     * Getting the number of distinct key ids.
     *
     * @return The number of distinct key ids.
     * @throws CacheException
     */
    long getKeyCount(String groupId) throws CacheException;

    /**
     * Getting all key ids of the given group
     *
     * @param groupId
     * @return The array of key ids
     * @throws CacheException
     */
    String[] getKeys(String groupId) throws CacheException;

    /**
     * Iterate over all key ids, satisfying the optionally given WHERE and/or LIMIT clauses
     * The clauses are to be given as SQL expression without (!) the keywords WHERE and LIMIT.
     * The iteration is done in descending order of the age of the keys (=> oldest keys first)
     *
     * @param groupId The GroupId for which the query is to be performed.
     * @param whereClause The SQL 'WHERE' clause without the 'WHERE keyword. This parameter can be {@code null}.
     * @param whereClause The SQL 'LIMIT' clause without the 'LIMIT' keyword. This parameter can be {@code null}.
     * @param keyConsumer The {@link Consumer}, receiving each single {@link KeyObject} instance interface.
     * @return The number of found key items.
     * @throws CacheException
     */
    long iterateKeysByDescendingAge(String groupId, @Nullable String whereClause, @Nullable String limitClause, Consumer<KeyObject> keyConsumer) throws CacheException;

    /**
     * Getting the number of distinct group ids.
     *
     * @return The number of distinct group ids.
     * @throws CacheException
     */
    long getGroupCount() throws CacheException;

    /**
     * Getting all group ids of the collection
     *
     * @return The array of group ids
     * @throws CacheException
     */
    String[] getGroups() throws CacheException;

    /**
     * Getting the summed up length of all items within the given group
     *
     * @return The total length
     * @throws CacheException
     */
    long getGroupSize(String groupId) throws CacheException;

    /**
     * Getting the summed up length of all items within the given group,
     * satisfying the given properties
     *
     * @return The total length
     * @throws CacheException
     */
    long getGroupSize(String groupId, Properties properties) throws CacheException;

    /**
     * Getting the summed up length of all items within the given group and key,
     *
     * @return The total length
     * @throws CacheException
     */
    long getKeySize(String groupId, String keyId) throws CacheException;

    /**
     * Getting the summed up length of all items within the given group and key,
     * satisfying the given properties
     *
     * @return The total length
     * @throws CacheException
     */
    long getKeySize(String groupId, String keyId, Properties properties) throws CacheException;

    /**
     * Returns the age in milliseconds of the oldest key by modification date within the group.
     *
     * @param groupId
     * @return The oldest key age of the given group in milliseconds, based on its modification date.<br>
     *         Returns <code>-1</code> in case no entry is availble
     * @throws CacheException
     */
    long getMaxKeyAgeMillis(String groupId) throws CacheException;

    // -------------------------------------------------------------------------

    /**
     * removing a single file item
     *
     * @param cacheObject
     * @return true, if the file item was removed; false otherwise
     * @throws CacheException
     */
    boolean remove(ICacheObject cacheObject) throws CacheException;

    /**
     * removing a single file item
     *
     * @param groupId
     * @param keyId
     * @param fileId
     * @return true, if the file item was removed; false otherwise
     * @throws CacheException
     */
    boolean remove(String groupId, String keyId, String fileId) throws CacheException;

    /**
     * removing an array of file items
     *
     * @param fileElements
     * @return The number of removed file items
     * @throws CacheException
     */
    int remove(ICacheObject[] fileElements) throws CacheException;

    /**
     * removing all file item, whose properties satisfy the search properties
     *
     * @param groupId
     * @param properties
     * @return The number of removed file items
     * @throws CacheException
     */
    int remove(String groupId, Properties properties) throws CacheException;

    /**
     * Removing all file items with the given group-subgruop
     *
     * @param groupId
     * @param keyId
     * @return The number of removed file items
     * @throws CacheException
     */
    int removeKey(String groupId, String keyId) throws CacheException;

    /**
     * Removing all file items with the given group-key ids
     *
     * @param groupId
     * @param keyId
     * @return The number of removed file items
     * @throws CacheException
     */
    int removeKeys(String groupId, String[] keyIds) throws CacheException;

    /**
     * Removing all file items with the given group
     *
     * @param groupId
     * @return The number of removed file items
     * @throws CacheException
     */
    int removeGroup(String groupId) throws CacheException;

    // -------------------------------------------------------------------------

    /**
     * Acquiring read access to a single file item.
     * The file item must exist, otherwise a {@link CacheException}
     * is thrown.</br>
     * The {@link IReadAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param cacheObject The identifier of the file, created via {@link ICache#implCreateFileItem(String, String, String)}
     * @return The {@link IReadAccess} interface to access the physical file item.
     * @throws CacheException
     */
    IReadAccess getReadAccess(ICacheObject cacheObject) throws CacheException;

    /**
     * Convenience method for {@link ICache#getReadAccess(ICacheObject)}
     * to save a prior call to {@link ICache#implCreateFileItem(String, String, String)}</br>
     * The {@link IReadAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param groupId
     * @param keyId
     * @param fileId
     * @param accessOption The list of {@link AccessOption}s
     * @return The {@link IReadAccess} interface to access the physical file item.
     * @throws CacheException
     */
    IReadAccess getReadAccess(String groupId, String keyId, String fileId) throws CacheException;

    /**
     * Acquiring write access to a single file item.
     * If the file item does not yet exist, it will be created.</br>
     * The {@link IWriteAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param cacheObject The identifier of the file, created via {@link ICache#implCreateFileItem(String, String, String)}
     * @return The {@link IWriteAccess} interface to access the physical file item.
     * @throws CacheException
     */
    IWriteAccess getWriteAccess(ICacheObject cacheObject) throws CacheException;

    /**
     * Convenience method for {@link ICache#getWriteAccess(ICacheObject)}
     * to save a prior call to {@link ICache#implCreateFileItem(String, String, String)}</br>
     * The {@link IWriteAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param groupId
     * @param keyId
     * @param fileId
     * @return The {@link IWriteAccess} interface to access the physical file item.
     * @throws CacheException
     */
    IWriteAccess getWriteAccess(String groupId, String keyId, String fileId) throws CacheException;
}
