
package com.openexchange.cacheservice.api;
/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import org.springframework.lang.Nullable;

/**
 * {@link ObjectStoreResult}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectStoreResult<T> {

    /**
     * {@link ResultType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum ResultType {
        OK,
        HTTP_ERROR,
        EXCEPTION
    }

    /**
     * Initializes a new {@link ObjectStoreResult}.
     */
    public ObjectStoreResult() {
        this(ResultType.OK, null, 0, null);
    }

    /**
     * Initializes a new {@link ObjectStoreResult}.
     * 
     * @param responseCode
     */
    public ObjectStoreResult(final T result) {
        this(ResultType.OK, result, 0, null);
    }

    /**
     * Initializes a new {@link ObjectStoreResult}.
     * 
     * @param code
     */
    public ObjectStoreResult(final int httpErrorCode) {
        this(ResultType.HTTP_ERROR, null, httpErrorCode, null);
    }

    /**
     * Initializes a new {@link ObjectStoreResult}.
     * 
     * @param errorCode
     * @param exception
     */
    public ObjectStoreResult(final Exception exception) {
        this(ResultType.EXCEPTION, null, 0, exception);
    }

    /**
     * Initializes a new {@link ObjectStoreResult}.
     * 
     * @param errorCode
     * @param exception
     */
    public ObjectStoreResult(final ResultType resultType, final T result, final int httpErrorCode, final Exception exception) {
        super();

        m_resultType = resultType;
        m_result = result;
        m_httpErrorCode = httpErrorCode;
        m_exception = exception;
    }

    /**
     * @return
     */
    public boolean isOk() {
        return (m_resultType == ResultType.OK);
    }

    /**
     * @return
     */
    public boolean isNotOk() {
        return (m_resultType != ResultType.OK);
    }

    /**
     * @return
     */
    public boolean hasResult() {
        return (isOk() && (null != m_result));
    }

    /**
     * @return
     */
    public T getResult() {
        return m_result;
    }

    /**
     * @return
     */
    public boolean hasHttpErrorCode() {
        return (ResultType.HTTP_ERROR == m_resultType);
    }

    /**
     * @return
     */
    public int getHttpErrorCode() {
        return m_httpErrorCode;
    }

    /**
     * @return
     */
    public boolean hasException() {
        return (ResultType.EXCEPTION == m_resultType);
    }

    /**
     * @return
     */
    public Exception getException() {
        return m_exception;
    }

    /**
     * @return
     */
    public @Nullable Exception getOrCreateExceptionIfNotOk() {
        if (isOk()) {
            return null;
        }

        return (hasException() ? m_exception : (hasHttpErrorCode() ? new Exception("ObjectStore operation received HTTP(S) error code: " + getHttpErrorCode()) : new Exception("ObjectStore operation received unknown error")));
    }

    // Members -------------------------------------------------------------

    final private ResultType m_resultType;

    final private T m_result;

    final private int m_httpErrorCode;

    final private Exception m_exception;
}
