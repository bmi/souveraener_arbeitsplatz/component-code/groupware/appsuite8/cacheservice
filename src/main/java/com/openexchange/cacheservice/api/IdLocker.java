/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.cacheservice.api;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * {@link IdLocker}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class IdLocker {

    /**
     * {@link Mode}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum Mode {
        STANDARD,
        BEGIN_PROCESSING,
        WAIT_IF_PROCESSED,
        TRY_LOCK
    }

    /**
     * {@link UnlockMode}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum UnlockMode {
        STANDARD,
        END_PROCESSING
    }

    /**
     * {@link IdLock}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    @SuppressWarnings("serial")
    private static class IdLock extends ReentrantLock {

        /**
         * Initializes a new {@link IdLock}.
         */
        public IdLock() {
            super();
        }

        /**
         * @return
         */
        public int incrementUseCount() {
            return m_useCount.incrementAndGet();
        }

        /**
         * @return
         */
        public int decrementUseCount() {
            return m_useCount.decrementAndGet();
        }

        /**
         * @return
         */
        public boolean lock(final Mode mode) {
            if (Mode.TRY_LOCK == mode) {
                return super.tryLock();
            }

            super.lock();

            if (Mode.WAIT_IF_PROCESSED == mode) {
                while (m_isProcessing && !Thread.currentThread().isInterrupted()) {
                    try {
                        m_processingFinishedCondition.await();
                    } catch (@SuppressWarnings("unused") InterruptedException e) {
                        // interrupted
                    }
                }
            }

            if (Mode.BEGIN_PROCESSING == mode) {
                m_isProcessing = true;
            }

            return true;
        }

        /**
         * Calling this method needs to be synchronized by the caller
         *
         * @return
         */
        public void unlock(final UnlockMode unlockMode) {
            if (m_isProcessing && (UnlockMode.END_PROCESSING == unlockMode)) {
                m_isProcessing = false;
                m_processingFinishedCondition.signalAll();
            }

            super.unlock();
        }

        /**
         * @return
         */
        public boolean isProcessing() {
            return m_isProcessing;
        }

        // - Members ---------------------------------------------------------------

        final private Condition m_processingFinishedCondition = newCondition();

        final private AtomicInteger m_useCount = new AtomicInteger(1);

        volatile private boolean m_isProcessing = false;
    }

    /**
     * @param element
     */
    public static boolean lock(final String element) {
        return lock(element, Mode.STANDARD);
    }

    /**
     * @param element
     */
    public static boolean tryLock(final String element) {
        return lock(element, Mode.TRY_LOCK);
    }

    /**
     * @param element
     * @param mode
     * @return
     */
    public static boolean lock(final String element, final Mode mode) {
        boolean ret = false;

        if (null != element) {
            IdLock idLock = null;

            synchronized (m_elementLockMap) {
                idLock = m_elementLockMap.get(element);

                if (null == idLock) {
                    m_elementLockMap.put(element, idLock = new IdLock());
                } else {
                    idLock.incrementUseCount();
                }
            }

            ret = idLock.lock(mode);

            // cleaning up in case of unsuccessful try lock
            if (!ret) {
                // remove element only from map, if no one else
                // holds the lock and no processing is happening
                synchronized (m_elementLockMap) {
                    if ((0 == idLock.decrementUseCount()) && !idLock.isProcessing()) {
                        m_elementLockMap.remove(element);
                    }
                }
            }

        }

        return ret;
    }

    /**
     * @param element
     */
    public static void unlock(final String element) {
        unlock(element, UnlockMode.STANDARD);
    }

    /**
     * @param element
     * @param finishProcessing
     */
    public static void unlock(final String element, final UnlockMode unlockMode) {
        if (null != element) {
            synchronized (m_elementLockMap) {
                final IdLock idLock = m_elementLockMap.get(element);

                if (null != idLock) {
                    idLock.unlock(unlockMode);

                    if ((0 == idLock.decrementUseCount()) && !idLock.isProcessing()) {
                        m_elementLockMap.remove(element);
                    }
                }
            }
        }
    }

    // - Members ---------------------------------------------------------------

    // m_elementLockMap needs to be synchronized with every access (intended)
    final private static Map<String, IdLock> m_elementLockMap = new HashMap<>();
}
