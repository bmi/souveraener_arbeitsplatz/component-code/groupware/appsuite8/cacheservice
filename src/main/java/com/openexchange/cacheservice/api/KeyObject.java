/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice.api;

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

/**
 * {@link KeyObject}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class KeyObject {

    /**
     * Unused
     *
     * Initializes a new {@link KeyObject}.
     */
    @SuppressWarnings("unused")
    private KeyObject() {
        super();

        m_keyId = StringUtils.EMPTY;
        m_createDate = m_modificactionDate = new Date(0);
        m_length = 0;
    }

    /**
     * Initializes a new {@link KeyObject}.
     * 
     * @param keyId
     * @param createDate
     * @param modificationDate
     * @param length
     */
    public KeyObject(@NonNull final String keyId,
        @NonNull final Date createDate,
        @NonNull final Date modificationDate,
        final long length) {

        super();

        m_keyId = keyId;
        m_createDate = createDate;
        m_modificactionDate = modificationDate;
        m_length = length;
    }

    /**
     * Gets the key id.
     *
     * @return The key Id.
     */
    @NonNull
    public String getKeyId() {
        return m_keyId;
    }

    /**
     * Retrieving the earliest create {@link Date} of the Key.
     *
     * @return The earliest create {@link Date} of the Key as Gregorian calendar date.
     *
     */
    @NonNull
    public Date getCreateDate() {
        return m_createDate;
    }

    /**
     * Retrieving the latest access {@link Date} of the Key.
     *
     * @return The latest access {@link Date} of the Key as Gregorian calendar date.
     *
     */
    @NonNull
    public Date getModificationDate() {
        return m_modificactionDate;
    }

    /**
     * Retrieving the summed up length of the Key.
     *
     * @return The summed up length of all {@link ICacheObject} lengths within the Key.
     */
    public long getLength() {
        return m_length;
    }

    // - Members ---------------------------------------------------------------

    final private String m_keyId;

    final private Date m_createDate;

    final private Date m_modificactionDate;

    final private long m_length;
}
