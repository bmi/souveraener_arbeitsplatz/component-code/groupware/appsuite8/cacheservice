/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cacheservice;

import com.google.common.base.Throwables;
import com.openexchange.cacheservice.api.CacheConfig;
import com.openexchange.cacheservice.api.CacheException;
import com.openexchange.cacheservice.api.IObjectStore;
import com.openexchange.cacheservice.impl.CacheDatabase;
import com.openexchange.cacheservice.impl.adapters.file.FileAdapter;
import com.openexchange.cacheservice.impl.adapters.file.FileAdapterConfig;
import com.openexchange.cacheservice.impl.adapters.file.FileAdapterConfig.Builder;
import com.openexchange.cacheservice.impl.adapters.s3.S3Adapter;
import com.openexchange.cacheservice.impl.adapters.s3.S3AdapterConfig;
import com.openexchange.cacheservice.impl.adapters.sproxyd.SproxydAdapter;
import com.openexchange.cacheservice.impl.adapters.sproxyd.SproxydAdapterConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.net.SocketFactory;
import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.function.Consumer;

import static com.openexchange.cacheservice.impl.Cache.LOG;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * {@link Application}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@SpringBootApplication
public class Application {

    final private static String S3_PREFIX = "s3:";
    final private static String SPROXYD_PREFIX = "sproxyd:";
    final private static String FILE_PREFIX = "file:";
    final private static String HTTP_PREFIX = "http://";
    final private static String HTTPS_PREFIX = "https://";
    final private static long DB_CREATE_DATABASE_TIMEOUT_MILLIS = 600000;
    final private static long DB_CREATE_DATABASE_PERIOD_MILLIS = 15000;

    /**
     * {@link CreateDatabaseInitializer}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class CreateDatabaseInitializer {

        public CreateDatabaseInitializer(@NonNull final CacheDatabase cacheDatabase) throws Exception {
            cacheDatabase.initialize();
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final SpringApplication application = new SpringApplication(Application.class);

        application.run(args);
    }

    /**
     * @param env
     * @return
     */
    @Bean("cacheconfig")
    public CacheConfig getCacheConfig(@NonNull final Environment env) throws CacheException {
        LOG.info("CS CacheService environment: " + System.getenv().toString());

        final CacheConfig.Builder cacheConfigBuilder = CacheConfig.builder();
        final String prefix = "com.openexchange.cacheservice.";
        final String dbPrefix = prefix + "database.";
        final String dbPropertyPrefix = dbPrefix + "property.";
        final String dbConnectionPoolPrefix = dbPrefix + "connectionpool.";

        String curValue = null;

        // read objectStoreIds and create appropriate ObjectStore instances from related config values
        if (isNotBlank(curValue = env.getProperty(prefix + "objectStoreIds", StringUtils.EMPTY))) {
            // TODO (KA): impl. get/create socket factory?
            final SocketFactory socketFactory = null;

            // retrieve valid file store ids from config
            for (String curObjectStoreId : curValue.split(",")) {
                if (StringUtils.isNotBlank(curObjectStoreId)) {
                    curObjectStoreId = curObjectStoreId.trim().toLowerCase();

                    if (!cacheConfigBuilder.containsObjectStore(curObjectStoreId)) {
                        final IObjectStore curObjectStore = implCreateObjectStoreFromConfig(env, curObjectStoreId, socketFactory);

                        if (null != curObjectStore) {
                            cacheConfigBuilder.withObjectStore(curObjectStoreId, curObjectStore);
                        }
                    }
                }
            }
        }

        // database.host
        if (isNotBlank(curValue = env.getProperty(dbPrefix + "host"))) {
            cacheConfigBuilder.withDBHost(curValue);
        }

        // database.port
        if (isNotBlank(curValue = env.getProperty(dbPrefix + "port")) && NumberUtils.isParsable(curValue)) {
            final int dbPort = NumberUtils.toInt(curValue);

            if (dbPort > 0) {
                cacheConfigBuilder.withDBPort(dbPort);
            }
        }

        // database.schema
        if (isNotBlank(curValue = env.getProperty(dbPrefix + "schema"))) {
            cacheConfigBuilder.withDBSchema(curValue);
        }

        // database.user
        if (isNotBlank(curValue = env.getProperty(dbPrefix + "user"))) {
            cacheConfigBuilder.withDBUser(curValue);
        }

        // database.password
        if (isNotBlank(curValue = env.getProperty(dbPrefix + "password"))) {
            cacheConfigBuilder.withDBPassword(curValue);
        }

        // database.password
        if (isNotBlank(curValue = env.getProperty(dbPrefix + "rootPassword"))) {
            cacheConfigBuilder.withDBRootPassword(curValue);
        }

        // database.property.* (trying to read values from database.property.0 to database.property.100)
        final Properties dbProperties = new Properties();

        for (int i = 0; i < 101; ++i) {
            try {
                final String curPropertyValue = env.getProperty(dbPropertyPrefix + i);

                if (isNotBlank(curPropertyValue)) {
                    final int assignPos = curPropertyValue.indexOf('=');

                    if ((assignPos > 0) && (assignPos < (curPropertyValue.length() - 1))) {
                        dbProperties.put(curPropertyValue.substring(0, assignPos).trim(), curPropertyValue.substring(assignPos + 1).trim());
                    }
                }
            } catch (final Exception e) {
                LOG.trace("CS Could not read database properties (Reason: {})", Throwables.getRootCause(e).getMessage());
            }
        }

        if (dbProperties.size() > 0) {
            cacheConfigBuilder.withDBProperties(dbProperties);
        }

        // database.connectionpool.maxPoolSize
        if (isNotBlank(curValue = env.getProperty(dbConnectionPoolPrefix + "maxPoolSize")) && NumberUtils.isParsable(curValue)) {
            final int dbPoolMaxSize = NumberUtils.toInt(curValue);

            if (dbPoolMaxSize > 0) {
                cacheConfigBuilder.withDBPoolMaxSize(dbPoolMaxSize);
            }
        }

        // database.connectionpool.connectTimeout
        if (isNotBlank(curValue = env.getProperty(dbConnectionPoolPrefix + "connectTimeout")) && NumberUtils.isParsable(curValue)) {
            final int dbPoolConnectTimeoutMillis = NumberUtils.toInt(curValue);

            if (dbPoolConnectTimeoutMillis > 0) {
                cacheConfigBuilder.withDBPoolConnectTimeoutMillis(dbPoolConnectTimeoutMillis);
            }
        }

        // database.connectionpool.idleTimeout
        if (isNotBlank(curValue = env.getProperty(dbConnectionPoolPrefix + "idleTimeout")) && NumberUtils.isParsable(curValue)) {
            final int dbPoolIdleTimeoutMillis = NumberUtils.toInt(curValue);

            if (dbPoolIdleTimeoutMillis > 0) {
                cacheConfigBuilder.withDBPoolIdleTimeoutMillis(dbPoolIdleTimeoutMillis);
            }
        }

        // database.connectionpool.maxLifetime
        if (isNotBlank(curValue = env.getProperty(dbConnectionPoolPrefix + "maxLifetime")) && NumberUtils.isParsable(curValue)) {
            final int dbPoolMaxLifeTimeMillis = NumberUtils.toInt(curValue);

            if (dbPoolMaxLifeTimeMillis > 0) {
                cacheConfigBuilder.withDBPoolMaxLifetimeMillis(dbPoolMaxLifeTimeMillis);
            }
        }

        // spoolPath
        if (isNotBlank(curValue = env.getProperty(prefix + "spoolPath"))) {
            cacheConfigBuilder.withSpoolPath(new File(curValue));
        }

        // default max key count
        if (isNotBlank(curValue = env.getProperty(prefix + "cacheDefaults.maxKeyCount"))) {
            final long cacheDefaultMaxKeyCount = NumberUtils.toLong(curValue);

            if (cacheDefaultMaxKeyCount > -2) {
                cacheConfigBuilder.withDefaultCacheMaxKeyCount(cacheDefaultMaxKeyCount);
            }
        }

        // default max size in GB
        if (isNotBlank(curValue = env.getProperty(prefix + "cacheDefaults.maxSizeGB"))) {
            final long cacheDefaultMaxSizeGB = NumberUtils.toLong(curValue);

            if (cacheDefaultMaxSizeGB > -2) {
                cacheConfigBuilder.withDefaultCacheMaxSizeGB(cacheDefaultMaxSizeGB);
            }
        }

        // default max size in GB
        if (isNotBlank(curValue = env.getProperty(prefix + "cacheDefaults.keyTimeoutMinutes"))) {
            final long cacheDefaultKeyTimeoutSeconds = NumberUtils.toLong(curValue);

            if (cacheDefaultKeyTimeoutSeconds > -2) {
                cacheConfigBuilder.withDefaultCacheKeyTimeoutMinutes(cacheDefaultKeyTimeoutSeconds);
            }
        }

        // cleanup period
        if (isNotBlank(curValue = env.getProperty(prefix + "cacheDefaults.cleanupPeriodSeconds"))) {
            final long cacheDefaultCleanupPeriodSeconds = NumberUtils.toLong(curValue) ;

            if (cacheDefaultCleanupPeriodSeconds > 0) {
                cacheConfigBuilder.withDefaultCacheCleanupPeriodMillis(cacheDefaultCleanupPeriodSeconds * 1000L);
            }
        }

        final CacheConfig cacheConfig = cacheConfigBuilder.build();

        implInitializeDatabase(cacheConfig);

        return cacheConfig;
    }

    /**
     * @param cacheConfig
     * @return
     */
    @SuppressWarnings("RedundantThrows")
    @Bean
    public DataSource getDataSource(@NonNull final DataSourceProperties props, @NonNull final CacheConfig cacheConfig) {
        final DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();

        dataSourceBuilder.url(props.getUrl()).username(props.getUsername()).password(props.getPassword());

        final DataSource dataSource = dataSourceBuilder.build();

        if (dataSource instanceof final HikariDataSource hikariDataSource) {
            final Properties dbProperties = cacheConfig.getDBProperties();

            for (final Object curDbPropertyName : dbProperties.keySet()) {
                hikariDataSource.addDataSourceProperty(curDbPropertyName.toString(), dbProperties.get(curDbPropertyName));
            }

            hikariDataSource.setAutoCommit(false);
            hikariDataSource.setMaximumPoolSize(cacheConfig.getDBPoolMaxSize());
            hikariDataSource.setConnectionTimeout(cacheConfig.getDBPoolConnectTimeoutMillis());
            hikariDataSource.setMaxLifetime(cacheConfig.getDBPoolMaxLifetimeMillis());
            // hikariDataSource.setIdleTimeout(cacheConfig.getDBPoolIdleTimeoutMillis());
        }

        return dataSource;
    }

    /**
     * @return
     * @throws Exception
     */
    @Bean
    @DependsOn({"cacheconfig", "liquibase"})
    public CreateDatabaseInitializer initializeDatabase(@NonNull final CacheDatabase cacheDatabase) throws Exception {
        return new CreateDatabaseInitializer(cacheDatabase);
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param env
     * @param curStoreStr
     * @param socketFactory
     * @return
     */
    private @Nullable IObjectStore implCreateObjectStoreFromConfig(@NonNull final Environment env, @NonNull final String curStoreStr, @Nullable SocketFactory socketFactory) {

        if (StringUtils.startsWithIgnoreCase(curStoreStr, S3_PREFIX)) {
            try {
                final int s3StoreConfigId = Integer.parseInt(curStoreStr.substring(S3_PREFIX.length()).trim());
                final S3AdapterConfig s3AdapterConfig = implGetS3AdapterConfig(env, "com.openexchange.cacheservice." + "objectstore.s3.", s3StoreConfigId, socketFactory);

                if (null != s3AdapterConfig) {
                    LOG.info("CS Using configured S3 based ObjectStore: [endpoint: {}, bucket: {}]", s3AdapterConfig.getEndpoint(), s3AdapterConfig.getBucketName());

                    return new S3Adapter(s3AdapterConfig, s3StoreConfigId);
                }
            } catch (CacheException e) {
                LOG.error("CS Not able to connect to S3 based ObjectStore for given config id: {} (Reason: {}", curStoreStr, Throwables.getRootCause(e).getMessage());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("CS Not able to parse configured S3 based ObjectStore id: {}", curStoreStr);
            }
        } else if (StringUtils.startsWithIgnoreCase(curStoreStr, SPROXYD_PREFIX)) {
            try {
                final int sproxydStoreConfigId = Integer.parseInt(curStoreStr.substring(SPROXYD_PREFIX.length()).trim());
                final SproxydAdapterConfig sproxydAdapterConfig = implGetSproxydAdapterConfig(env, "com.openexchange.cacheservice." + "objectstore.sproxyd.", sproxydStoreConfigId, socketFactory);

                if (null != sproxydAdapterConfig) {
                    LOG.info("CS Using configured SproxyD based ObjectStore: {}", sproxydAdapterConfig.getBaseURLString());

                    return new SproxydAdapter(sproxydAdapterConfig, sproxydStoreConfigId);
                }
            } catch (CacheException e) {
                LOG.error("CS Not able to connect to SproxyD based ObjectStore for given config id: {} (Reason: {}", curStoreStr, Throwables.getRootCause(e).getMessage());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("CS Not able to parse configured SproxyD based ObjectStore id: {}", curStoreStr);
            }
        } else if (StringUtils.startsWithIgnoreCase(curStoreStr, FILE_PREFIX)) {
            try {
                final int fileConfigId = Integer.parseInt(curStoreStr.substring(FILE_PREFIX.length()).trim());
                final FileAdapterConfig fileAdapterConfig = implGetFileAdapterConfig(env, "com.openexchange.cacheservice." + "objectstore.file.", fileConfigId);

                if (null != fileAdapterConfig) {
                    LOG.info("CS Using configured file based ObjectStore: {}", fileAdapterConfig.getPath().getAbsolutePath());

                    return new FileAdapter(fileAdapterConfig, fileConfigId);
                }
            } catch (CacheException e) {
                LOG.error("CS Not able to connect to file based ObjectStore for given config id: {} (Reason: {}", curStoreStr, Throwables.getRootCause(e).getMessage());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.error("CS Not able to parse configured file based ObjectStore id: {}", curStoreStr);
            }
        }

        LOG.error("CS Not able to create ObjectStore adapter with configuration value: {} => Please check configuration properties for intended ObjectStore to use", curStoreStr);

        return null;
    }

    /**
     * @param env
     * @param s3StoreConfigId
     * @return
     */
    private S3AdapterConfig implGetS3AdapterConfig(@NonNull final Environment env, @NonNull final String s3ConfigPrefix, final int s3StoreConfigId, @Nullable SocketFactory socketFactory) {

        final S3AdapterConfig.Builder s3ConfigBuilder = S3AdapterConfig.builder();
        final String s3StoreConfigPrefix = s3ConfigPrefix + s3StoreConfigId + ".";
        String curValue = null;

        // get S3 object store endpoint
        if (isNotBlank(curValue = env.getProperty(s3StoreConfigPrefix + "endpoint"))) {
            s3ConfigBuilder.withEndpoint(curValue);
        }

        // get S3 object store region
        if (isNotBlank(curValue = env.getProperty(s3StoreConfigPrefix + "region"))) {
            s3ConfigBuilder.withRegion(curValue);
        }

        // get S3 object store bucket name
        if (isNotBlank(curValue = env.getProperty(s3StoreConfigPrefix + "bucketName"))) {
            s3ConfigBuilder.withBucketName(curValue);
        }

        // get S3 object store access key
        if (isNotBlank(curValue = env.getProperty(s3StoreConfigPrefix + "accessKey"))) {
            s3ConfigBuilder.withAccessKey(curValue);
        }

        // get S3 object store secret key
        if (isNotBlank(curValue = env.getProperty(s3StoreConfigPrefix + "secretKey"))) {
            s3ConfigBuilder.withSecretKey(curValue);
        }

        // set S3 object store socket factory, that has been provided
        s3ConfigBuilder.withSocketFactory(socketFactory);

        // create and return S3AdapterConfig if valid
        final S3AdapterConfig s3AdapterConfig = s3ConfigBuilder.build();

        return s3AdapterConfig.isValid() ? s3AdapterConfig : null;
    }

    /**
     * @param env
     * @param socketFactory
     * @param sproxydStoreConfigId
     * @return
     */
    private SproxydAdapterConfig implGetSproxydAdapterConfig(@NonNull final Environment env, @NonNull final String sproxyDConfigPrefix, final int sproxydStoreConfigId, @Nullable SocketFactory socketFactory) {

        final SproxydAdapterConfig.Builder sproxydConfigBuilder = SproxydAdapterConfig.builder();
        final String sproxydStoreConfigPrefix = sproxyDConfigPrefix + sproxydStoreConfigId + ".";
        String curValue = null;

        // get Sproxyd object store endpoint
        if (isNotBlank(curValue = env.getProperty(sproxydStoreConfigPrefix + "endpoint", StringUtils.EMPTY))) {
            curValue = curValue.trim();

            if (curValue.startsWith(HTTP_PREFIX) || curValue.startsWith(HTTPS_PREFIX)) {
                sproxydConfigBuilder.withEndpoint(curValue);
            }
        }

        // get Sproxyd object store path
        if (isNotBlank(curValue = env.getProperty(sproxydStoreConfigPrefix + "path"))) {
            sproxydConfigBuilder.withPath(curValue);
        }

        // get Sproxyd object store max connections
        implApplyLongProperty(env, sproxydStoreConfigPrefix + "maxConnections", 1, 100, (curLong) -> sproxydConfigBuilder.withMaxConnections(curLong.intValue()));

        // get Sproxyd object store connection timeout in milliseconds
        implApplyLongProperty(env, sproxydStoreConfigPrefix + "connectTimeout", 1000, 5000, sproxydConfigBuilder::withConnectTimeoutMillis);

        // get Sproxyd object store connection timeout in milliseconds
        implApplyLongProperty(env, sproxydStoreConfigPrefix + "socketTimeout", 1000, 30000, sproxydConfigBuilder::withSocketTimeoutMillis);

        // get Sproxyd object store socket timeout in milliseconds
        implApplyLongProperty(env, sproxydStoreConfigPrefix + "heartbeatInterval", 1000, 60000, sproxydConfigBuilder::withSocketTimeoutMillis);

        // set Sproxyd object store socket factory, that has been provided
        sproxydConfigBuilder.withSocketFactory(socketFactory);

        // create and return SproxydAdapterConfig if valid
        final SproxydAdapterConfig sproxydAdapterConfig = sproxydConfigBuilder.build();

        return sproxydAdapterConfig.isValid() ? sproxydAdapterConfig : null;
    }

    /**
     * @param env
     * @param fileConfigId
     * @return
     */
    private FileAdapterConfig implGetFileAdapterConfig(@NonNull final Environment env, @NonNull final String fileConfigPrefix, final int fileConfigId) {

        final Builder fileConfigBuilder = FileAdapterConfig.builder();
        final String fileStoreConfigPathKey = fileConfigPrefix + fileConfigId + ".path";
        String curValue = null;

        // get root path for filesystem based store
        if (isNotBlank(curValue = env.getProperty(fileStoreConfigPathKey))) {
            fileConfigBuilder.withPath(new File(curValue));
        }

        // create and return FileAdapterConfig if valid
        final FileAdapterConfig fileAdapterConfig = fileConfigBuilder.build();

        return fileAdapterConfig.isValid() ? fileAdapterConfig : null;
    }

    /**
     * @param env
     * @param configKey
     * @param lowerBoundary
     * @param defaultValue
     * @param longConsumer
     */
    private void implApplyLongProperty(@NonNull final Environment env, @NonNull final String configKey, final long lowerBoundary, final long defaultValue, @NonNull final Consumer<Long> longConsumer) {

        final String configValueStr = env.getProperty(configKey);
        long longValue = defaultValue;

        if (isNotBlank(configValueStr)) {
            if (NumberUtils.isParsable(configValueStr)) {
                longValue = NumberUtils.toLong(configValueStr, Long.MIN_VALUE);
            }

            if (longValue < lowerBoundary) {
                longValue = defaultValue;

                LOG.warn("CS Could not read numeric configuration value (Minimum allowed: {}): {} => using default value: {})", configKey + "=" + configValueStr, lowerBoundary, defaultValue);
            }
        }

        longConsumer.accept(longValue);
    }

    private static void implInitializeDatabase(@NonNull final CacheConfig cacheConfig) throws CacheException {
        // create new database, if it does not exist yet
        final String jdbcUrl = "jdbc:mysql://" + cacheConfig.getDBHost() + ":" + cacheConfig.getDBPort() + "/" + cacheConfig.getDBSchema() + "?serverTimezone=Europe/Berlin";
        final int paramSepPos = jdbcUrl.indexOf("?");
        final String jdbcDatabaseUrl = jdbcUrl.substring(0, (paramSepPos > 0) ? paramSepPos : jdbcUrl.length());
        final int schemaSepPos = jdbcDatabaseUrl.lastIndexOf("/");
        final String databaseSchema = (schemaSepPos < jdbcDatabaseUrl.length() - 1) ? jdbcDatabaseUrl.substring(schemaSepPos + 1) : "cacheservicedb";
        final String jdbcRootPassword = cacheConfig.getDBRootPassword();
        final String jdbcUsername = isNotBlank(jdbcRootPassword) ? "root" : cacheConfig.getDBUser();
        final String jdbcPassword = isNotBlank(jdbcRootPassword) ? jdbcRootPassword : cacheConfig.getDBPassword();

        final StringBuilder jdbcUrlToUseBuilder = new StringBuilder(256).append(jdbcUrl);
        final Properties databaseProperties = cacheConfig.getDBProperties();

        if (databaseProperties.size() > 0) {
            boolean addMore = (paramSepPos > -1);

            for (final Object curObj : databaseProperties.keySet()) {
                final String curKey = curObj.toString();

                if (StringUtils.isNotBlank(curKey) && !"user".equals(curKey) && !"password".equals(curKey)) {
                    final String curValue = databaseProperties.getProperty(curKey);

                    if (StringUtils.isNotBlank(curValue)) {
                        if (addMore) {
                            jdbcUrlToUseBuilder.append('&');
                        } else {
                            jdbcUrlToUseBuilder.append('?');
                            addMore = true;
                        }

                        jdbcUrlToUseBuilder.append(curKey).append('=').append(curValue);
                    }
                }
            }
        }

        final String jdbcUrlToUse = jdbcUrlToUseBuilder.substring(0, schemaSepPos) + ((paramSepPos > -1) ? jdbcUrlToUseBuilder.substring(paramSepPos) : "");
        final long startTimeMillis = System.currentTimeMillis();

        LOG.info("CS Trying to create '{}' database schema if it does not exist", databaseSchema);

        while (true) {
            try (final Connection con = DriverManager.getConnection(jdbcUrlToUse, jdbcUsername, jdbcPassword);
                 final Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

                stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS `" + databaseSchema + "` DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;");

                // leave retry loop as soon as statement was executed successfully
                break;
            } catch (Throwable e) {
                // leave in case the maximum time for retries has been reached
                if ((System.currentTimeMillis() - startTimeMillis) > DB_CREATE_DATABASE_TIMEOUT_MILLIS) {
                    LOG.info("CS No connection to database possible, giving up! Please check your database server or user/password settings...");
                    throw new CacheException(Throwables.getRootCause(e));
                }

                LOG.info("CS Waiting for creation of database schema to continue: {}", Throwables.getRootCause(e).getMessage());
            }

            try {
                // retry after a wait period
                Thread.sleep(DB_CREATE_DATABASE_PERIOD_MILLIS);
            } catch (@SuppressWarnings("unused") InterruptedException i) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
