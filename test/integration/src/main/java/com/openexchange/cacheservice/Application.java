/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.cacheservice;

import org.apache.commons.lang3.StringUtils;
import static io.restassured.RestAssured.when;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.json.JSONObject;
import org.json.JSONException;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

/**
 * {@link Application}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @param <JSONObject>
 * @since v8.0.0
 */
@SuppressWarnings("rawtypes")
@TestInstance(Lifecycle.PER_CLASS)
public class Application {

    private final static long MAX_INITIAL_CONNECTION_TIMEOUT_MILLIS = 60000;

    private static String SERVER_PROTOCOL = "http";

    private static String SERVER_HOST = "localhost";

    private static int SERVER_PORT = 8001;

    public static final String PATH_STATUS = "/cache/status";

    static {
        final String serverProtocolFromEnv = System.getenv("SERVER_PROTOCOL");
        final String serverHostFromEnv = System.getenv("SERVER_HOST");
        final String serverPortFromEnv = System.getenv("SERVER_PORT");

        // overwrite server protocol if given
        if (StringUtils.isNotBlank(serverProtocolFromEnv) && ("http".equals(serverProtocolFromEnv))) {
            SERVER_PROTOCOL = serverProtocolFromEnv;
        }

        // overwrite server host if given
        if (StringUtils.isNotBlank(serverHostFromEnv)) {
            SERVER_HOST = serverHostFromEnv;
        }

        // overwrite server port if given
        if (StringUtils.isNotBlank(serverPortFromEnv) && StringUtils.isNumeric(serverPortFromEnv)) {
            try {
                SERVER_PORT = Integer.parseUnsignedInt(serverPortFromEnv);

                if (SERVER_PORT < 1) {
                    throw new NumberFormatException("'SERVER' env var given must be greater than 0");
                }
            } catch (NumberFormatException e) {
                Assertions.fail("Given SERVER_PORT env var is not a valid Integer value");
            }
        }
    }

    // -------------------------------------------------------------------------

    @BeforeAll
    public void setUp() {
        resetRestAssured();
        waitForServerConnection();
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Check for cache /status reponse code 200")
    public void test_01_Status() {
        var response = when().
            get(PATH_STATUS).
        then().
            assertThat().statusCode(200).extract().response();

        final JSONObject jsonResult = implCheckAndGetJSONResponse(response);
        Assertions.assertEquals("UP", jsonResult.getString("status"));
    }

    public void waitForServerConnection() {
        final long connectionCheckEndTimeMillis = System.currentTimeMillis() + MAX_INITIAL_CONNECTION_TIMEOUT_MILLIS;

        do {
            //noinspection OverlyBroadCatchBlock
            try {
                if (when().get(PATH_STATUS).statusCode() == 200) {
                    return;
                }
            } catch (Exception e) {
                // wait
            }
        } while (System.currentTimeMillis() < connectionCheckEndTimeMillis);

        Assertions.fail("Not able to get initial connection to: " +
                Application.SERVER_PROTOCOL + "://" +
                Application.SERVER_HOST + ":" +
                Application.SERVER_PORT);

    }

    JSONObject implCheckAndGetJSONResponse(final Response response) {
        JSONObject jsonResponse = null;

        try {
            jsonResponse = new JSONObject(response.getBody().asString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        } catch (RuntimeException e) {
            Assertions.fail(e);
        } finally {
            Assertions.assertNotNull(jsonResponse);
        }
        return jsonResponse;
    }

    private void resetRestAssured() {
        RestAssured.reset();
        RestAssured.baseURI = Application.SERVER_PROTOCOL + "://" + Application.SERVER_HOST;
        RestAssured.port = Application.SERVER_PORT;
        System.out.println();
        System.out.println("RestAssured setup for integration-tests:");
        System.out.println("========================================");
        System.out.println("RestAssured.baseURI=" + RestAssured.baseURI);
        System.out.println("RestAssured.port=" + RestAssured.port);
        System.out.println("RestAssured.proxy=" + RestAssured.proxy);
        System.out.println();
    }
}

