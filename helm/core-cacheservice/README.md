# Helm Chart core-cacheservice
This Helm Chart deploys Cache service core in a kubernetes cluster.

## Introduction
This Chart includes the following components:

* Cache service application container to deploy in a kubernetes cluster.

## Requirements
Requires Kubernetes v1.19+

## Dependencies
This section will provide details about specific requirements in addition to this Helm Chart.
## Pushing to registry
From wihtin ${PROJECT_DIR}/helm/core-cacheservice directory:

```shell
helm repo add ox-documents-registry https://registry.open-xchange.com/chartrepo/documents
helm repo update
helm push . ox-documents-registry
```

## Test installation
Run a test against a cluster deployment:

```shell
helm repo add ox-documents-registry https://registry.open-xchange.com/chartrepo/documents
helm repo update
helm install --dry-run --debug --generate-name --version [VERSION] ox-documents-registry/core-cacheservice
```

## Installing the chart
Install the Chart with the release name 'alice':

```shell
helm repo add ox-documents-registry https://registry.open-xchange.com/chartrepo/documents
helm repo update
helm install alice --version [VERSION] ox-documents-registry/core-cacheservice [-f path/to/values_with_credentials.yaml]
```

### Configuration

## Global Configuration
| Parameter                                         | Description                                                                                       | Default                     |
|---------------------------------------------------|---------------------------------------------------------------------------------------------------|-----------------------------|
| `defaultRegistry`                                 | The image registry                                                                                | `registry.open-xchange.com` |
| `image.repository`                                | The image repository                                                                              | `core-cacheservice`         |
| `image.tag`                                       | The image tag                                                                                     | ``                          |
| `image.pullPolicy`                                | The imagePullPolicy for the deployment                                                            | `IfNotPresent`              |
| `imagePullSecrets`                                | List of references to secrets for image registries                                                | `[]`                        |
| `ingress.enabled`                                 | Make ImageConverter service reachable from outside of cluster                                     | `false`                     |
| `ingress.controller`                              | The type of controller to use, possible values are `nginx` and `traefik`                          | `nginx`                     |
| `ingress.hosts`                                   | The list of hosts the service can be reached from. Set to empty to allow from everywhere.         | `[]`                        |
| `ingress.hosts.host`                              | The host the service can be reached from                                                          | `chart-example.local`       |
| `cacheService.cacheDefaults.maxEntries`           | The maximum number of cache key entries. Use -1 for unlimited.                                    | `1000000`                   |
| `cacheService.cacheDefaults.maxSizeMegaBytes`     | The maximum size of all cache key entries combined. Use -1 for unlimited.                         | `-1`                        |
| `cacheService.cacheDefaults.maxLifetimeSeconds`   | The maximum age in seconds of a cache key entry before it gets removed. Use -1 for unlimited.     | `2592000`                   |
| `cacheService.cacheDefaults.cleanupPeriodSeconds` | The period in seconds after which the next cache cleanup will be performed                        | `300`                       |
| `cacheService.mysql.host`                         | The CacheService database connection host                                                         | ``                          |
| `cacheService.mysql.port`                         | The CacheService database connection port                                                         | `3306`                      |
| `cacheService.mysql.database`                     | The CacheService database connection schema                                                       | `cacheservicedb`            |
| `cacheService.mysql.auth.user`                    | The CacheService database connection user                                                         | ``                          |
| `cacheService.mysql.auth.password`                | The CacheService database connection password                                                     | ``                          |
| `cacheService.mysql.auth.rootPassword`            | The CacheService database connection root password to create e.g. a new database                  | ``                          |
| `cacheService.mysql.properties`                   | The optional CacheService database connection properties to pass to the database drivers.         | `[]`                        |
| `cacheService.s3ObjectStores`                     | The list of S3 object stores to use                                                               | `[]`                        |
| `cacheService.s3ObjectStores.id`                  | The numeric id of the current S3 based object store that shouldn't be changed once assigned       | ``                          |
| `cacheService.s3ObjectStores.endpoint`            | The endpoint URL of the current S3 object store                                                   | ``                          |
| `cacheService.s3ObjectStores.region`              | The region of the current S3 object store                                                         | `eu-central-1`              |
| `cacheService.s3ObjectStores.bucketName`          | The bucket name of the current S3 object store                                                    | `cacheservice`              |
| `cacheService.s3ObjectStores.accessKey`           | The access key of the current S3 object store                                                     | ``                          |
| `cacheService.s3ObjectStores.secretKey`           | The secret key of the current S3 object store                                                     | ``                          |
| `cacheService.sproxydObjectStores`                | The list of SproxyD object stores to use                                                          | `[]`                        |
| `cacheService.sproxydObjectStores.id`             | The numeric id of the current SproxyD based object store that shouldn't be changed once assigned  | ``                          |
| `cacheService.sproxydObjectStores.endpoint`       | The endpoint URL of the current SproxyD based object store                                        | ``                          |
| `cacheService.sproxydObjectStores.path`           | The path where to store objects in the current SproxyD based object store                         | `proxyd/cacheservice`       |
| `persistence.enabled`                             | Specifies if cluster volumes are mounted by container. Using emptyDir Volumes when false.         | `false`                     |
| `env`                                             | Configuration properties passed to the service via environment variables                          | `[]`                        |
