{{- define "core-cacheservice.ingressPathMappings" -}}
paths:
  - pathType: Prefix
    path: /cacheservice
    targetPath: /cache/
    targetPort:
      name: http
  - pathType: Exact
    path: /cacheservice-health
    targetPath: /health
    targetPort:
      name: httplive
  - pathType: Exact
    path: /cacheservice-env
    targetPath: /env
    targetPort:
      name: httplive
  - pathType: Exact
    path: /cacheservice-metrics
    targetPath: /metrics
    targetPort:
      name: httplive
{{- end -}}
