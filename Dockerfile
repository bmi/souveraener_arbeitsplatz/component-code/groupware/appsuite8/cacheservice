###################################################################################################
# open-xchange-cacheservice build stage for project and project specific JRE
###################################################################################################
FROM        registry-proxy.k3s.os2.oxui.de/library/eclipse-temurin:17 as builder-stage


# Copy relevant project sources
COPY        build.gradle ./
COPY        settings.gradle ./
COPY        gradlew ./
COPY        gradle ./gradle
COPY        src ./src

# Build project
RUN         ./gradlew --parallel --max-workers 4 runtime

###################################################################################################
# open-xchange-cacheservice service stage, using JRE and project builds from build stage
###################################################################################################
FROM        registry-proxy.k3s.os2.oxui.de/library/debian:bullseye-slim

ENV         PROJECT_HOME /opt/open-xchange/cacheservice

ENV         LANG C.UTF-8
ENV         LANGUAGE C.UTF-8
ENV         LC_ALL C.UTF-8

RUN         apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
                ca-certificates \
                fontconfig \
                locales \
                procps \
                tzdata && \
            echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
            locale-gen en_US.UTF-8 && \
            rm -rf /var/lib/apt/lists/*

COPY        --from=builder-stage build/image/cacheservice/ ${PROJECT_HOME}/

# Copy configuration files
COPY        conf/cacheservice.properties ${PROJECT_HOME}/etc/
COPY        src/main/resources/logback-spring.xml ${PROJECT_HOME}/etc/

ENV         TINI_VERSION v0.19.0
ADD         https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN         chmod +x /tini && \
            groupadd -r -g 1000 open-xchange && \
            useradd -r -g open-xchange -u 987 open-xchange

RUN         chown -R open-xchange:open-xchange /opt/open-xchange
RUN         mkdir -p /var/log/open-xchange && chown -R open-xchange:open-xchange /var/log/open-xchange && \
            mkdir -p /var/spool/open-xchange && chown -R open-xchange:open-xchange /var/spool/open-xchange && \
            mkdir -p /var/opt && chown -R open-xchange:open-xchange /var/opt

USER        open-xchange:open-xchange

WORKDIR     /opt/open-xchange

ENTRYPOINT  [ "/tini", "-g", "--", "/bin/sh", "-c", \
              "JAVA_OPTS=\" \
                -Duser.timezone=UTC \
                -Dliquibase.secureParsing=false \
                -Xms${CS_JVM_HEAP_SIZE_MB:-768}m \
                -Xmx${CS_JVM_HEAP_SIZE_MB:-768}m \
                -XshowSettings:vm\" \
              ${PROJECT_HOME}/bin/cacheservice \
                --spring.profiles.active=prod \
                --spring.config.additional-location=${PROJECT_HOME}/etc/cacheservice.properties \
                --logging.config=${PROJECT_HOME}/etc/logback-spring.xml" \
            ]

EXPOSE      8001
